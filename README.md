# ADV

This repository contains the source code for the ADV server and client
programs and their documentation. It also serves as a compilation environment
suited to build, test and/or install any set of ADV mods.

Remember to clone the repository giving the `--recursive` flag to Git.
If you have an outdated mods set on your repository, you may need to run
`git submodule update --init --recursive`.

## Compilation

To build the package, run inside the package directory:

```bash
./configure
make
```

The `make` command will compile the `adv-server` and `adv`
programs, together with all available mods.

If you only want to build one of these, run `make server`, `make client` or
`make mods` instead.

If you want to build a specific set of mods, you can type something like:

```bash
make mods/foo mods/bar ...
```

To install the package, run `make install` as administrator user.

To see all available compilation and installation options, run:

```bash
./configure --help
```

To clean the working directory, run:

```bash
make clean
```

## Compilation dependencies

- GNU GCC C/C++ Compiler
- C/C++ Standard Library
- GNU Make
- Boost (for the client only)
- OpenSSL

## Installation dependencies

- For both the server and the client
  + OpenSSL
- For the server only
  + Rsync
  + Tar (for the backup tool)
  + Xz (for the backup tool)
  + Jq (for the backup tool)
- For the client only
  + NCurses 5.4 or later
- For the Adv mod
  + PostgreSQL
  + Perl
  + DBD-PG Perl Library
  + List::MoreUtils Perl module
  + Perl-json module

## Translations

Strings used in both the server and the client are defined in JSON files.
They can be found under the [strings directory](etc/strings).
If you want the project to support a new language, simply add the corresponding
`LANG.json` files under these directories.

Keep in mind that many strings have variables. Their syntax must be maintained.
Please use any existing translation as reference.

Always use the corresponding english file as a starting point,
that way you can perform partial translations.

If you want to add some randomness to the client strings you can use the
`@` character in key definitions. For a string having the key `string_foo`,
instead of defining that exact key you can define `string_foo@1`, `string_foo@2` and so on.
The client will pick one of them randomly each time `string_foo` is needed.
**The server does not support string randomness.**

### Translations of Client Commands

Client commands are defined under the [commands directory](etc/strings/commands).
These files are critical for commands to work correctly with each particular language inside the client.

If you want to work with the translation of a commands file, please remember:

- Keep all the original english `forms` on every command, and add all the desired translated forms at the end.
  The last form on the array shall be seen on the client help.
- Replace the forms at every `see-also` field with the last form of each corresponding `forms` array.
- Limit text lines to a maximum of 80 characters long.

If you have any doubt, please use any existing non-english translation as reference.

## Versioning

The ADV Project uses a **change significance scheme** for numbering versions.
Stable releases are of the form X.Y, with X being the Major number
and Y being the Minor number. Unstable versions are of the form X.Y.rN,
where N is the revision number.

## Documentation

You will find the core ADV documentation under the [doc directory](doc/).

- [Server/Client](doc/README.md)
- [Games](doc/Games.md)
- [Mods](doc/Mods.md)
  - [Hooks](doc/Hooks.md)
  - [Mods API](doc/ModsApi.md)
- [Client API](doc/ClientApi.md)
- [Server Hooks](doc/ServerHooks.md)

## License

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

Written by Marcelo López Minnucci <coloconazo@gmail.com>
and Mariano López Minnucci <mlopezviedma@gmail.com>

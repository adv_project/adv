# Client API Calls for getting Server Information

## Server Information

The client can ask the server how many players are connected at the moment
and the server uptime:

```json
{
  "class": "info"
}
```

The server will send a response like this:

```json
{
  "class": "info",
  "connected": <number_of_currently_connected_players>,
  "uptime": <server_uptime_in_seconds>
}
```

## Server Version

The client can ask the server which is the running version:

```json
{
  "class": "version"
}
```

The server will send a response like this:

```json
{
  "class": "version"
  "version": "<running_version>"
}
```

## List existent games

The client can ask the server which are the currently existent games:

```json
{
  "class": "games"
}
```

The server will send a response like this:

```json
{
  "class": "games",
  "game": "<game_the_player_is_currently_in>",
  "games": [
             {
                "name": "<first_game_name>",
                "title": "<pretty_name>",
                "created": <seconds_since_epoch_of_game_creation>,
                "mods": [ "<mod1>", "<mod2>", ... ],
                "description": [
                  "<game_description>",
                  ...
                ]
             },
             {
                "name": "<second_game_name>",
                "title": "<pretty_name>",
                "created": <seconds_since_epoch_of_game_creation>,
                "mods": [ "<mod1>", "<mod2>", ... ],
                "description": [
                  "<game_description>",
                  ...
                ]
             },
             ...
           ]
}
```

If the player is not inside a game, the `game` field will be an empty string.

## List installed mods

The client can ask the server which are the currently installed mods:

```json
{
  "class": "mods",
  "type": "<filter_by_mod_type>"
}
```

The `type` field is optional. If provided, the server will respond with mods of that type only.
Otherwise, the server will respond with all installed mods.

The server will send a response like this:

```json
{
  "class": "mods",
  "type": "<asked_type_if_provided>",
  "mods": [
    {
      "name": "<mod_name_as_referenced>",
      "version": "<mod_version_if_provided>",
      "url": "<mod_url_if_provided>",
      "type": "<mod_type_if_provided>",
      "pretty-name": "<mod_pretty_name_if_provided>",
      "description": [
        "<mod_description_if_provided>",
        ...
      ],
      "depends": [
        "<name_of_mod_dependency_if_provided>",
        ...
      ]
    },
    ...
  ]
}
```

# ADV Documentation

The ADV project is designed to provide an ideal environment for playing
singleplayer and multiplayer text-based games. These games must be created
specifically to work within ADV and can have a highly modular structure.
The project is composed by a few elements:

- A **server** acts as the communication center for all games and players.
- A **client** provides a simple and intuitive interface for players to
  connect to a server and play on it.
- Games are built as **mods**. A game can consist in more than one mod.
  Mod designers can choose to make them work together or independently.
- Games can also have games inside themselves recursively, called *branches*.

The ADV server/client system has a very simple design, and these programs
perform very simple tasks. The reachable level of complexity is given by the
server workflow and mods design.

## The ADV Server

The `adv-server` program forks the process sending it to background and
writing its PID file if the `--start` argument is given; and
reads the PID file sending a terminate signal to the active process
if the `--stop` argument is given.

Before running the server for the first time, the user must run the
`adv-createcert` program to create an SSL key/certificate pair.

Most game mods need a PostgreSQL database to work.
Before running the server for the first time,
run the `adv-initdb` program as root user for this purpose.

A backup tool is also available for backing up the entire server state,
including user accounts and games data.
Run `adv-backup --help` for further details.

#### Working directories

The server sets up two directories:

- **Static directory**: This is where all static data is stored, and it's of
  course persistent after shutdown.
- **Running directory**: This is where mods constantly modify their data.
  It's mounted on RAM, loaded from the static directory content on startup
  and backed up periodically and when the server stops. These two last tasks
  are done using `rsync`.

If the server is run by the administrator user, then the static directory is
`/var/lib/adv` and the running directory is `/run/adv`. Otherwise, the `HOME`
environment variable and the `/run/user/$(id -u)` directory must both be set
for the user; so the static directory is `$HOME/.adv` and the running directory
is `/run/user/$(id -u)/adv`.

#### Configuration

Configuration is read from the file `STATIC_DIRECTORY/config.json`, or
`PREFIX/share/adv/config.json` if the former doesn't exist.

These are some available options:

`log-level` can be one of `emerg`, `alert`, `crit`, `err`, `warn`, `notice`,
`info` or `debug`. Integer numbers can also be specified (0 for emergency,
1 for alert, 2 for critical, 3 for error, 4 for warning, 5 for notice,
6 for info and 7 for debug).

Logs are stored in `RUNNING_DIRECTORY/adv-server.log` and backed up
periodically in `STATIC_DIRECTORY/adv-server.log`.

`port` specifies the TCP port the server will listen to.

`delay` indicates how many microseconds will the server wait when there's
nothing to do.

`sync-period` indicates the synchronization period between
`RUNNING_DIRECTORY` and `STATIC_DIRECTORY`, in seconds.

The server tries to open its TCP socket a configurable number of times.
`socket-period` indicates the period between tries, in seconds.
`socket-tries` indicates the number of tries before quitting.

`dbname`, `dbhost`, `dbport`, `dbuser` and `dbpass` provide the PostgreSQL
database information for every mod that needs it.

`motd` is the server's Message Of The Day. It must be either an empty list or
a list of strings. This list will be sent to players on every successful
login response.

Some of these options and some others can also be changed by connecting to the
server as an administrator user using the `/admin` command.

#### Player connections

The server opens a TCP socket (on port 2224 by default) for players to connect.
This socket is encrypted with SSL, and this way
messages are sent and interpreted as JSON objects separated by the
NUL character. For a detailed explanation on messages syntax,
see [ADV Client API](doc/ClientApi.md).

#### Server Hooks

The server static directory can also contain a `hooks` directory with a series
of executable files named [server hooks](doc/ServerHooks.md).

## The ADV Client

The `adv` program is a simple and configurable NCurses interface that allows
the user to connect to both local and remote ADV servers. In ADV, singleplayer
experience is by no means different from multiplayer experience: if you run
a local server without opening the server port over the network and you connect
to `localhost` using the `adv` client, then you'll be in *singleplayer mode*.

The `adv-server` and `adv` programs share the same versioning system.
This means that using server/client pairs of the same version guarantees full
compatibility. Therefore, the client will be able to help you with all and only
the commands that the server supports. Of course, you'll be able to know the
running version of a remote server through the [Client API](doc/ClientApi.md).

For more information on how the client works, run `adv` on a terminal
and navigate through the help by typing `/help`.

## ADV Games

A player can be connected to at most one **game**. Games are created by players
specifying which [mods](doc/Mods.md) will the game use and following
instructions from each mod at the game creation phase.

For more information on how games work, see [ADV Games](doc/Games.md).

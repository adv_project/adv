# Server Hooks

Server hooks are executed by the server after specific events; they are given
an input JSON object through the `RUNNING_DIRECTORY/input.json` file.
The `RUNNING_DIRECTORY` is given as an only argument for the hook.

Inside the [doc directory](doc/) you can find a `server-hooks` directory
to serve as a starting point for creating server hooks.

Every hook that the server supports is listed below.
If the input JSON is not specified for a given hook, assume no input JSON.

## Server Start and Stop

Every time the server starts and stops, `STATIC_DIRECTORY/hooks/start` and
`STATIC_DIRECTORY/hooks/stop` are executed, respectively.

## Server Synchronization

Every time the server syncs the static directory, `STATIC_DIRECTORY/hooks/sync`
is executed, with the following input JSON:

```json
{
  "origin": "command_line|server_loop|admin|server_stop",
  "arguments": "<sync_arguments>"
}
```

`origin` indicates the reason why the synchronization took place:

- `command_line`: The `adv-server` program was executed giving the `--sync` option.
- `server_loop`: The sync period has passed after the last sync, so the server just performed it.
- `admin`: An administrator user sent the `sync` administration task.
  In this case the administrator can send additional `arguments` to trigger specific actions.
- `server_stop`: The server is stopping, so it just performed the last sync.

This hook also blocks the entire games environment, so it's a secure moment to
perform some critical tasks. The `sync` file under the `server-hooks` directory
serves as a usage example. In this case, the `sync` hook is used to
keep a specific set of mods updated and to backup the server.

## Game creation

When a game is created, `STATIC_DIRECTORY/hooks/create` is executed,
with the following input JSON:

```json
{
  "name": "<game_name>",
  "username": "<creator_username>"
}
```

## Game destruction

When a game is destroyed, `STATIC_DIRECTORY/hooks/destroy` is executed,
with the following input JSON:

```json
{
  "name": "<game_name>"
}
```

## Player enters a game

Every time a player enters a game, `STATIC_DIRECTORY/hooks/enter` is executed,
with the following input JSON:

```json
{
  "name": "<game_name>",
  "username": "<username>"
}
```

## Player leaves a game

Every time a player leaves a game, `STATIC_DIRECTORY/hooks/leave` is executed,
with the following input JSON:

```json
{
  "name": "<game_name>",
  "username": "<username>"
}
```

## Player logs in

Every time a player logs in to the server, `STATIC_DIRECTORY/hooks/login` is executed,
with the following input JSON:

```json
{
  "username": "<username>"
}
```

## Player logs out

Every time a player logs out from the server, `STATIC_DIRECTORY/hooks/logout` is executed,
with the following input JSON:

```json
{
  "username": "<username>"
}
```

## Player sends a chat message

Every time a player sends a chat message, `STATIC_DIRECTORY/hooks/chat` is executed,
with the following input JSON:

```json
{
  "username": "<username>",
  "content": "<message_content>"
}
```

## Player makes a dice roll

Every time a player makes a dice roll, `STATIC_DIRECTORY/hooks/roll` is executed,
with the following input JSON:

```json
{
  "username": "<username>",
  "values": [ <die1>, <die2>, ... ]
}
```

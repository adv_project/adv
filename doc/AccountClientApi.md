# Client API Calls for Accounts Management

## Logging in

To login as a player on a server, the client must send a login message like
the following:

```json
{
  "class": "login",
  "username": "<username>",
  "password": "<password>"
}
```

This won't work if the client is already logged in (i.e. it must be sent from
an idle connection).

The `password` field is optional. When a client logs in without a password,
it'll be considered a *non-registered player*.

If the login was successful, the server will send a response like this:

```json
{
  "class": "login",
  "response": "success",
  "username": "<username>",
  "connected": <number_of_currently_connected_players>,
  "version": "<running_version>",
  "uptime": <server_uptime_in_seconds>,
  "motd": [ "<first_motd_line>", "<second_motd_line>", ... ]
}
```

Where `motd` is the server's Message Of The Day.

Upon failure, the server will send a response like this:

```json
{
  "class": "login",
  "response": "failure",
  "reason": "already_logged_in|unregistered_disabled|username_not_provided|invalid_username|already_registered_username|incorrect_password|kicked|banned",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

If reason is `kicked`, then the additional field `wait` will be given
to indicate how many seconds does the user have to wait to be able to login again.

## Logging out

The logout message is very simple:

```json
{
  "class": "logout"
}
```

The server will send a response like this:

```json
{
  "class": "logout",
  "username": "<username>",
  "reason": "logout"
}
```

A server administrator can kick a player for a certain amount of time.
If the kicked player is connected at that moment, it'll receive a logout response like this:

```json
{
  "class": "logout",
  "username": "<username>",
  "reason": "kicked",
  "wait": <seconds_to_wait>
}
```

and will be immediately disconnected from the server. This means that the player will have to wait
for `<seconds_to_wait>` seconds to be able to login again.

A server administrator can also ban a player forever.
If the kicked player is connected at that moment, it'll receive a logout response like this:

```json
{
  "class": "logout",
  "username": "<username>",
  "reason": "banned"
}
```

## Registration

Registration means persistently saving a username and a password inside a
server. If a username is registered, the only way to login is by providing
the password inside the login message.

```json
{
  "class": "register",
  "username": "<username>",
  "password": "<password>"
}
```

This won't work if the client is already logged in (i.e. it must be sent from
an idle connection). If the registration is successful, the server will login
the player and send a response like this:

```json
{
  "class": "register",
  "response": "success",
  "username": "<username>",
  "connected": <number_of_currently_connected_players>,
  "version": "<running_version>",
  "uptime": <server_uptime_in_seconds>,
  "motd": [ "<first_motd_line>", "<second_motd_line>", ... ]
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "register",
  "response": "failure",
  "reason": "already_logged_in|registration_disabled|username_not_provided|invalid_username|password_not_provided|already_registered_username",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Password change

To change the password for an already logged in account, the client must send:

```json
{
  "class": "password",
  "password": "<password>"
}
```

If the password change is successful, the server will send a response like this:

```json
{
  "class": "password",
  "response": "success",
  "username": "<username>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "password",
  "response": "failure",
  "reason": "not_logged_in|registration_disabled|password_not_provided",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

If the client is logged in as a non-registered user, then the registration
will turn effective.

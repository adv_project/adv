# Client API Calls for Playing

## Enter a game

To enter a game, the client must be logged in and send the following request:

```json
{
  "class": "enter",
  "name": "<game_name>",
  "lang": "<client_language>"
}
```

The `lang` field is optional, and it's the way for the games to know the player's preferred language.

Upon success, the server will send a response like this:

```json
{
  "class": "enter",
  "response": "success",
  "name": "<game_name>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "enter",
  "response": "failure",
  "reason": "not_logged_in|game_name_not_provided|invalid_game_name|already_in_that_game|already_in_another_game|game_doesnt_exist",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Leave a game

To leave a game, the client must send the following request:

```json
{
  "class": "leave"
}
```

Upon success, the server will send a response like this:

```json
{
  "class": "leave",
  "response": "success",
  "name": "<game_name>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "leave",
  "response": "failure",
  "reason": "not_logged_in|not_in_any_game",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Send actions to game

*Actions* are the way players communicate with the game they are in:

```json
{
  "class": "action",
  "action": "[ "<first_argument>", "<second_argument>", ... ]",
  "text": "[ "<first_text_line>", "<second_text_line>", ... ]"
}
```

Upon failure, the server will send a response like this:

```json
{
  "class": "action",
  "response": "failure",
  "reason": "not_logged_in|not_in_any_game|no_action_provided|game_paused",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

If `reason` equals `game_paused`, then the response will also contain a
`pause-message` field:

```json
{
  "class": "action",
  "response": "failure",
  "reason": "game_paused",
  "pause-message": [ "<first_line>", "<second_line>", ... ],
  "content": object
}
```

## Receive actions from game

Games communicate with players with *messages*. Players receive messages this way:

```json
{
  "class": "message",
  "type": "<message_type>",
  "tag": "<message_tag>",
  "message": "<message_sent>"
}
```

The `type` field gives the client a hint about how to print the message.
The standard types that every client should know are `plain`, `help`, `info`,
`notice`, `warning`, `error` and `admin`. If the `type` field is not given,
the server sends the default type `plain`.

The `tag` indicates if an older sent line should be replaced. Every time a client
receives a tagged message, this message should replace the one having the same
tag, if it exists. An empty tag means that the message is not tagged, therefore
it will never be replaced (this is the default).


The `message` field can also be an array of strings, meaning that they should be
printed on different lines:

```json
{
  "class": "message",
  "type": "<message_type>",
  "tag": "<optional_message_tag>",
  "message": "[ "<first_text_line>", "<second_text_line>", ... ]"
}
```

**Messages sent as string arrays cannot be tagged.**

## Receive "write" requests from game

When a game wants a player to send an action with a `text` field, the following
API Call can be received:

```json
{
  "class": "write",
  "action": "[ "<first_argument>", "<second_argument>", ... ]",
  "text": "[ "<first_text_line>", "<second_text_line>", ... ]"
}
```

The client should react on this call by asking the player to send this `action`
together with a text field, optionally giving `text` as a content initialization.

# Client API Calls for Games Management

## Game creation

To create a new game, the client must be logged in and send the following request:

```json
{
  "class": "create",
  "lang": "<client_language>",
  "name": "<game_name>",
  "mods": [ "<first_mod_name>", "<second_mod_name>", ... ],
  "title": "<game_title>"
}
```

The `lang` field is optional, and it's the way for the games to know the player's preferred language.

Remember:
- A game cannot already exist with the same name.
- At least one mod name must be specified.
- The `title` field is optional. If empty or not provided, the server will assign the game name to it.

Upon success, the server will send a response like this:

```json
{
  "class": "create",
  "response": "success",
  "name": "<game_name>",
  "mods": [ "<first_mod_name>", "<second_mod_name>", ... ],
  "title": "<game_title>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "create",
  "response": "failure",
  "reason": "not_logged_in|game_name_not_provided|invalid_game_name|game_name_already_exists|max_games_limit|creation_rate_limit|no_mod_provided",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

If reason is `creation_rate_limit`, then the additional field `wait` will be given
to indicate how many seconds does the user have to wait to be able to create a new game.

## Game management

To manage a game, the client must be logged in as the game owner,
be inside the game to manage and send the following request:

```json
{
  "class": "manage",
  "mod": "<mod_name>",
  "arguments": [ "<first_argument>", "<second_argument>", ... ]
}
```

Depending on which mods the game has, different arguments will be requested.

Upon failure, the server will send a response like this:

```json
{
  "class": "manage",
  "response": "failure",
  "reason": "not_logged_in|not_in_any_game|mod_name_not_provided|not_game_owner",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Game destruction

To destroy a game, the client must be logged in as the game owner
and send the following request:

```json
{
  "class": "destroy",
  "name": "<game_name>"
}
```

Upon success, the server will send a response like this:

```json
{
  "class": "destroy",
  "response": "success",
  "name": "<game_name>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "destroy",
  "response": "failure",
  "reason": "not_logged_in|game_name_not_provided|game_doesnt_exist|not_game_owner",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Transfer a game to another player

To change the ownership of a game, the client must be logged in as the game owner
and send the following request:

```json
{
  "class": "transfer",
  "name": "<game_name>",
  "new-owner": "<name_of_new_owner>"
}
```

Upon success, the server will send a response like this:

```json
{
  "class": "transfer",
  "response": "success",
  "name": "<game_name>",
  "new-owner": "<name_of_new_owner>"
}
```

Otherwise, a failure response will be sent:

```json
{
  "class": "transfer",
  "response": "failure",
  "reason": "not_logged_in|game_name_not_provided|game_doesnt_exist|not_game_owner|new_owner_not_provided|invalid_new_owner",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

# ADV Games

Games are built using installed [mods](doc/Mods.md).
A player can only create games with mods of type `game`.
When a game is created, a directory structure is associated with it
under the root game directory, which contains:

- A `properties.json` file with some necessary information about the game:

```json
{
  "name": "<game_unique_name>",
  "title": "<game_pretty_name>",
  "owner": "<game_owner>",
  "created": <seconds_since_epoch_of_game_creation>,
  "mods": [
    "<mod1>",
    "<mod2>",
    ...
  ],
  "games": [
    "<branch1>",
    "<branch2>",
    ...
  ],
  "dbname": "<database_name>",
  "dbhost": "<database_hostname>",
  "dbport": <database_port>,
  "dbuser": "<database_user>",
  "dbpass": "<database_password>",
  "description": [
    "<optional_description_about_the_game>",
    ...
  ],
  "seconds-per-tick": <number_of_seconds_between_updates>,
  "last-updated": <seconds_since_epoch_of_last_update>,
  "paused": <boolean_indicating_whether_the_game_is_paused_or_not>,
  "pause-message": [
    "<message_sent_to_players_when_attempting_to_play_a_paused_game>",
    ...
  ]
}
```

- a `mods/` directory that holds one symbolic link to each game mod;
- a `games/` directory that holds all child games (or *branches*); and
- a `users` symbolic link that points to a static directory provided by
  the server. This directory can be used to store static information about
  users that is persistent after game destructions.

Mods can add any other files and directories to this structure,
but they should never write `properties.json`, `mods/` or `games/`.

In case mods use the PostgreSQL database, they should create a schema named
exactly like the game and work only inside that schema.

Through the [Client API](doc/ClientApi.md), a player can:

- Create and manage games;
- Enter a game;
- Leave from the game he/she is in;
- List all existent games on the server; and
- Send actions to the game he/she is in.

The game will use these events and some more to run *game hooks*, which are
covered in the [ADV Mods](doc/Mods.md) documentation.

## Branches

A branch is a game that was created by an existent game.
A game can only create branches with mods of types `game` and `branch`.
The `owner` property of a branch is always `..` (dot-dot).

The owner of a branch is its parent game, which means that the parent game
is the only entity that can directly destroy that particular branch.
When a player destroys a game, all game branches are recursively destroyed.

## See Also

- [Mods](doc/Mods.md)
  - [Hooks](doc/Hooks.md)
  - [Mods API](doc/ModsApi.md)

# Client API Calls for Administration Tasks

Only users having the `admin` role can perform all **administration tasks**.
If a server doesn't have any admins, then the first player that connects
to the server is assigned that role automatically. This will be notified
to the player right after he/she logs in, through the following message:

```json
{
  "class": "admin",
  "event": "you_are_the_master",
  "response": "success",
  "username": "<name_of_the_player>"
}
```

Administrators also have permission to destroy and transfer any game.

Users having the `moderator` role can perform some administration tasks,
which are:

- [Send message to a player as anonymous administrator](#send-message-to-a-player-as-anonymous-administrator)
- [Kick a player from the server for a specific amount of time](#kick-a-player-from-the-server-for-a-specific-amount-of-time)
- [Ban a player from the server forever](#ban-a-player-from-the-server-forever)
- [List registered players](#list-registered-players)
- [List connected players](#list-connected-players)
- Other server events
  - [User just logged in](#user-just-logged-in)
  - [User just logged out](#user-just-logged-out)

To perform an administration task, the client must send the following request:

```json
{
  "class": "admin",
  "command": "<administration_command>"
}
```

If the command fails, a failure response will be sent to the sender:

```json
{
  "class": "admin",
  "response": "failure",
  "reason": "not_logged_in|not_a_server_admin|not_a_server_moderator|cannot_ban_a_moderator|bad_admin_command",
  "content": object
}
```

The client can also send an admin task without a `command` field:

```json
{
  "class": "admin"
}
```

The server will send a success response if the user is an administrator
on the server:

```json
{
  "class": "admin",
  "event": "yes_sir",
  "response": "success"
}
```

If the user is not an administrator on the server, then a failure
`not_a_server_admin` response will be sent.

Some commands require a specific set of additional fields.
Possible commands are explained below.

## Setting values for a certain server option

```json
{
  "class": "admin",
  "command": "set",
  "option": "<option_name>",
  "value": "<optional_new_option_value>"
}
```

Possible options are `lang`, `log-level`, `delay`, `sync-period`,
`max-players`, `max-games`, `creation-rate`, `chat-backlog-size` and `motd`
(in this last case, `value` must be a string array).

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "set",
  "response": "success",
  "option": "<option_name>",
  "value": "<new_option_value>"
}
```

If `value` is not provided, the server will send the same response anyway,
with the current option value.

## Assign a role to a player

```json
{
  "class": "admin",
  "command": "assign-role",
  "role": "<role_name>",
  "username": "<name_of_the_player_to_assign_the_role>"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "assign-role",
  "response": "success",
  "role": "<role_name>",
  "username": "<name_of_the_player_to_assign_the_role>"
}
```

If `username` is connected, then the following message
shall be sent to that player:

```json
{
  "class": "admin",
  "event": "assign-role",
  "role": "<role_name>",
  "response": "success"
}
```

## Revoke a role from a player

```json
{
  "class": "admin",
  "command": "revoke-role",
  "role": "<role_name>",
  "username": "<name_of_the_player_to_revoke_the_role>"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "revoke-role",
  "response": "success",
  "role": "<role_name>",
  "username": "<name_of_the_player_to_revoke_the_role>"
}
```

## Enable and disable new accounts registration

```json
{
  "class": "admin",
  "command": "enable-registration|disable-registration"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "enable-registration|disable-registration",
  "response": "success"
}
```

## Enable and disable unregistered account logins

```json
{
  "class": "admin",
  "command": "enable-unregistered|disable-unregistered"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "enable-unregistered|disable-unregistered",
  "response": "success"
}
```

## Send message to a player as anonymous administrator

```json
{
  "class": "admin",
  "command": "send",
  "username": "<name_of_the_player>",
  "message": "<message_content>"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "send",
  "username": "<name_of_the_player>",
  "message": "<message_content>",
  "response": "success"
}
```

and the following response to `username`:

```json
{
  "class": "message",
  "type": "admin",
  "message": "<message_content>"
}
```

## Send message to all connected players as anonymous administrator

```json
{
  "class": "admin",
  "command": "broadcast",
  "message": "<message_content>"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "broadcast",
  "message": "<message_content>",
  "response": "success"
}
```

and the following response to all connected players:

```json
{
  "class": "message",
  "type": "admin",
  "message": "<message_content>"
}
```

## Kick a player from the server for a specific amount of time

```json
{
  "class": "admin",
  "command": "kick",
  "username": "<name_of_the_player>",
  "seconds": <seconds_to_kick>
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "kick",
  "username": "<name_of_the_player>",
  "seconds": <seconds_to_kick>,
  "response": "success"
}
```

and the corresponding `logout` response to the kicked player:

```json
{
  "class": "logout",
  "username": "<username>",
  "reason": "kicked",
  "wait": <seconds_to_wait>
}
```

Admins or moderators cannot be kicked.

## Ban a player from the server forever

```json
{
  "class": "admin",
  "command": "ban",
  "username": "<name_of_the_player>"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "ban",
  "username": "<name_of_the_player>",
  "response": "success"
}
```

and the corresponding `logout` response to the kicked player:

```json
{
  "class": "logout",
  "username": "<username>",
  "reason": "banned"
}
```

To let the player login again, an admin must `kick` the player to setup a new login time.

Admins or moderators cannot be banned.

## List registered players

```json
{
  "class": "admin",
  "command": "registered"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "registered",
  "registered": [
    {
      "username": "<username>",
      "roles": [ "<role_1>", "<role_2>", ... ],
      "banned": <until>
    },
    ...
  ],
  "response": "success"
}
```

Kicked players will have `until` larger than zero, showing the expiration moment.
Banned players will have `until` equal to zero.
Otherwise, the `banned` field won't be given.

## List connected players

```json
{
  "class": "admin",
  "command": "players"
}
```

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "players",
  "players": [ "<username_1>", "<username_1>", ... ],
  "response": "success"
}
```

## Perform the working directory synchronization now

```json
{
  "class": "admin",
  "command": "sync",
  "arguments": "<sync_arguments>"
}
```

The `arguments` field is optional. This way the server can trigger additional
actions through the `sync` server hook.

Upon success, the server will send the following response to the sender:

```json
{
  "class": "admin",
  "event": "sync",
  "response": "success"
}
```

## Other server events

The server also notifies all connected admins when certain events occur.

### User just logged in

```json
{
  "class": "admin",
  "event": "login",
  "username": "<name_of_the_player_who_logged_in>",
  "response": "success"
}
```

### User just logged out

```json
{
  "class": "admin",
  "event": "logout",
  "username": "<name_of_the_player_who_logged_out>",
  "response": "success"
}
```

### User just created a new game

```json
{
  "class": "admin",
  "event": "create",
  "username": "<name_of_the_player_who_created_the_game>",
  "name": "<name_of_the_new_game>",
  "response": "success"
}
```

### Working directory synchronization was just performed

```json
{
  "class": "admin",
  "event": "just_synced",
  "response": "success"
}
```

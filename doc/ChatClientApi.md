# Client API Calls for Chatting

## Chatting with other connected players

The server provides a minimal chat system. If a logged in player sends a
chat message, every connected player will receive it.

To send a chat message, the client must send the following request:


```json
{
  "class": "chat",
  "content": "<chat_message_content>"
}
```

Upon success, the server will send the following response to every logged in
player:

```json
{
  "class": "chat",
  "username": "<name_of_sender>",
  "content": "<chat_message_content>"
}
```

Otherwise, a failure response will be sent to the sender:

```json
{
  "class": "chat",
  "response": "failure",
  "reason": "not_logged_in",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Rolling dice

The server provides a minimal dice system.
If a logged in player makes a dice roll,
every connected player will receive the result.

To make a dice roll, the client must send the following request:

```json
{
  "class": "roll",
  "ndice": <number_of_dice_to_roll>,
  "nfaces": <number_of_each_die_faces>
}
```

Both `ndice` and `nfaces` fields are optional.
By default, the server will roll one 6-faces die.

Upon success, the server will send the following response to every logged in
player:

```json
{
  "class": "roll",
  "username": "<name_of_sender>",
  "ndice": <number_of_rolled_dice>,
  "nfaces": <number_of_each_die_faces>,
  "values": [ <die1>, <die2>, ... ]
}
```

Otherwise, a failure response will be sent to the sender:

```json
{
  "class": "roll",
  "response": "failure",
  "reason": "not_logged_in",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

## Getting the last chat and roll messages stored on the server

If so configured, the server stores the last chat and roll messages
with their respective timestamps.
To retrieve them, the client must send the following request:

```json
{
  "class": "chat-backlog"
}
```

Upon success, the server will send a response to the sender similar to this:

```json
{
  "class": "chat-backlog",
  "size": <amount_of_messages_stored>,
  "content": [
    {
      "class": "chat",
      "timestamp": <seconds_since_epoch_of_sent_message>,
      "username": "<name_of_sender>",
      "content": "<chat_message_content>"
    },
    {
      "class": "roll",
      "timestamp": <seconds_since_epoch_of_sent_message>,
      "username": "<name_of_sender>",
      "ndice": <number_of_rolled_dice>,
      "nfaces": <number_of_each_die_faces>,
      "values": [ <die1>, <die2>, ... ]
    },
    ...
  ]
}
```

Otherwise, a failure response will be sent to the sender:

```json
{
  "class": "chat-backlog",
  "response": "failure",
  "reason": "not_logged_in",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

The elements inside the `content` array will be sent in chronological order.
Here we see one `chat` message and one `roll` message just as an example
to show each class structure.

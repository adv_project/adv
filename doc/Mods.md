# ADV Mods

An ADV Mod is a directory structure that allows the creation and
improvement of ADV Games. Installed mods are located inside the
`PREFIX/share/adv/mods` directory.

Every mod should have a file called `properties.json` inside its root directory
with the following fields:

```json
{
  "name": "<mod_pretty_name>",
  "version": "<optional_mod_version>",
  "url": "<optional_mod_url>",
  "type": "<optional_mod_type>",
  "description": [
    "Brief description of the mod, so that the server can send",
    "this information to players when they look at the mods list."
  ],
  "depends": [
    "<mod_1>",
    "<mod_2>",
    ...
  ],
  "libs": [
    "<mod_1>",
    "<mod_2>",
    ...
  ]
}
```

The `depends` field indicates mods that shall be added,
including their dependencies, recursively.

If a mod would like to make use of other mod's features but
**without running their hooks**, the `libs` optional field suits this purpose.
The server will check that those mods exist, same way as with `depends`,
but their dependencies won't be neither checked nor added.

A mod can also contain a `hooks` directory with a series of executable files
named **game hooks** and/or any other files and directories that the mod can
use or share to other mods.

You can find some mod examples [here](doc/mods).

## See also

- [Hooks](doc/Hooks.md)
  - [Server Start](doc/Hooks.md#server-start)
  - [Server Stop](doc/Hooks.md#server-stop)
  - [Game initialization](doc/Hooks.md#game-initialization)
  - [Management request from owner](doc/Hooks.md#management-request-from-owner)
  - [Game was transfered to a new owner](doc/Hooks.md#game-was-transfered-to-a-new-owner)
  - [Game destruction](doc/Hooks.md#game-destruction)
  - [Update](doc/Hooks.md#update)
  - [Action request from player](doc/Hooks.md#action-request-from-player)
  - [Player enters a game](doc/Hooks.md#player-enters-a-game)
  - [Player leaves a game](doc/Hooks.md#player-leaves-a-game)
  - [Player was kicked from a branch](doc/Hooks.md#player-was-kicked-from-a-branch)
  - [Message from parent game](doc/Hooks.md#message-from-parent-game)
  - [Message from a branch](doc/Hooks.md#message-from-a-branch)
  - [Offline hook](doc/Hooks.md##offline-hook)
- [Mods API](doc/ModsApi.md)
  - **Message API Calls**
    - [Send a message to a player](doc/ModsApi.md#send-a-message-to-a-player)
    - [Broadcast a message to all players in the game](doc/ModsApi.md#broadcast-a-message-to-all-players-in-the-game)
    - [Request a "write" action from a player](doc/ModsApi.md#request-a-write-action-from-a-player)
    - [Log a message on the server](doc/ModsApi.md#log-a-message-on-the-server)
  - **API Calls for Games Workflow**
    - [Run a specific script](doc/ModsApi.md#run-a-specific-script)
    - [Abort hook](doc/ModsApi.md#abort-hook)
    - [Kick player from the game](doc/ModsApi.md#kick-player-from-the-game)
    - [Run update hook](doc/ModsApi.md#run-update-hook)
  - **API Calls for Games Properties**
    - [Pause game](doc/ModsApi.md#pause-game)
    - [Resume game](doc/ModsApi.md#resume-game)
    - [Set Seconds Per Tick](doc/ModsApi.md#set-seconds-per-tick)
    - [Set Game Description](doc/ModsApi.md#set-game-description)
  - **API Calls for Branches**
    - [Create new branch](doc/ModsApi.md#create-new-branch)
    - [Send "manage" command to branch](doc/ModsApi.md#send-manage-command-to-branch)
    - [Teleport a player from one game to another](doc/ModsApi.md#teleport-a-player-from-one-game-to-another)
    - [Send a message to a branch](doc/ModsApi.md#send-a-message-to-a-branch)
    - [Send a message to the parent game](doc/ModsApi.md#send-a-message-to-the-parent-game)
    - [Destroy branch](doc/ModsApi.md#destroy-branch)

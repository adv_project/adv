# API Calls

The output file of a hook is grabbed by the server and read like this:

```json
[
  { <First API Call> },
  { <Second API Call> },
  ...
]
```

Every API Call that the server supports is listed below.

## Send a message to a player

```json
{
  "class": "send",
  "player": "<name_of_the_player_that_will_receive_the_message>",
  "type": "<optional_message_type>",
  "tag": "<optional_message_tag>",
  "message": "<message_to_send>"
}
```

The `message` field can also be an array of strings. This allows the mod to
tell the client not to break the whole message with other kinds of information
and also how to separate the message into different lines of text:

```json
{
  "class": "send",
  "player": "<name_of_the_player_that_will_receive_the_message>",
  "type": "<optional_message_type>",
  "message": [ "<first_message_line>", "<second_message_line>", ... ]
}
```

The `type` field gives the client a hint about how to print the message.
The standard types that every client should know are `plain`, `help`, `info`,
`notice`, `warning`, `error` and `admin`. If the `type` field is not given,
the server sends the default type `plain`.

The `tag` field allows the mod to replace older lines. Every time a client
receives a tagged message, this message should replace the one having the same
tag, if it exists. An empty tag means that the message is not tagged, therefore
it will never be replaced (this is the default).
**Messages sent as string arrays cannot be tagged.**

## Broadcast a message to all players in the game

```json
{
  "class": "broadcast",
  "skip": "<optional_player_to_skip>",
  "type": "<optional_message_type>",
  "tag": "<optional_message_tag>",
  "message": "<message_to_send>"
}
```

If the `skip` field is given, the broadcast message will skip this player (the
message is sent to all players in the game **but** him). The `message`, `type`
and `tag` fields behave the same way as in the `send` API Call.

## Request a "write" action from a player

When a game wants a player to send an action with a `text` field, the following
API Call can be sent:

```json
{
  "class": "write",
  "player": "<name_of_the_player_that_will_receive_the_message>",
  "action": "[ "<first_argument>", "<second_argument>", ... ]",
  "text": "[ "<first_text_line>", "<second_text_line>", ... ]"
}
```

The client should react on this call by asking the player to send this `action`
together with a text field, optionally giving `text` as a content initialization.

## Log a message on the server

```json
{
  "class": "log",
  "log-level": <message_log_level>,
  "message": "<message_to_log>"
}
```

The `log-level` field is read as an integer number where 0 means `emerg`,
1 means `alert`, 2 means `crit`, 3 means `err`, 4 means `warn`,
5 means `notice`, 6 means `info` and 7 means `debug`.
This field is optional and its default value is 5 (`notice`).

## Run a specific script

```json
{
  "class": "run",
  "path": "<absolute_path_of_the_script>",
  "input": object
}
```

This will run the specified script sending the `input` JSON object through the
`GAME_DIRECTORY/input.json` file and `GAME_DIRECTORY` as an only argument
for the script, just like the hooks case.
An output JSON object is grabbed from the `GAME_DIRECTORY/output.json` file,
which is interpreted in turn as a list of JSON API Calls.

## Abort hook

```json
{
  "class": "abort"
}
```

This will abort the hook execution. This means that the hook won't run for
the rest of the mods this time around.

## Kick player from the game

```json
{
  "class": "kick",
  "player": "<name_of_the_player_to_kick>"
}
```

This will force a **leave** on the player from the game. The server will run
the **leave** hook on this game and, if this game is a branch,
the server will run the **kick** hook on the parent game.

## Run update hook

```json
{
  "class": "update"
}
```

This will force the game **update hook** to run now. The update time is also
refreshed, so the next update will run `seconds-per-tick` seconds after this.

## Pause game

```json
{
  "class": "pause",
  "pause-message": [ "<first_line>", "<second_line>", ... ]
}
```

This will pause the game and set an optional *pause message* for players to
know the reason for pausing.
Players are not able to send actions to paused games.

## Resume game

```json
{
  "class": "resume"
}
```

This will resume the game and therefore let players send actions again.

## Set Seconds Per Tick

```json
{
  "class": "seconds-per-tick",
  "seconds-per-tick": <new_seconds_per_tick>
}
```

This will set a new value for `seconds-per-tick`. It must be larger or equal
than zero. If zero, then the update hook is disabled.

## Set Game Description

```json
{
  "class": "description",
  "description": [ "<first_line>", "<second_line>", ... ]
}
```

This will set a new description for the game. It must be an array of strings,
indicating how should lines be separated when showing this description to users.

If `description` is not given or it doesn't have the correct format,
then the game description results empty.
When a user creates a game, the description is empty aswell.

## Create new branch

```json
{
  "class": "create",
  "name": "<branch_name>",
  "mods": [ "<first_mod_name>", "<second_mod_name>", ... ],
  "libs": [ "<mod_1>", "<mod_2>", ... ]
}
```

This will create a new child game, a.k.a. *branch*.
The game will reference this branch with the name `<branch_name>`,
which must be unique only among siblings.
The absolute branch name, which will appear on its `properties` file,
will be `<parent_name>_<branch_name>`.

At least one mod name must be specified, and a game can only create branches
with mods of types `game` and `branch`.
The `owner` property of a branch is always `..` (dot-dot).

If a game would like to make use of other mod's features but
**without running their hooks**, the `libs` optional field suits this purpose.
The server will check that those mods exist,
but their dependencies won't be neither checked nor added.

## Send "manage" command to branch

```json
{
  "class": "manage",
  "branch": "<branch_name>",
  "mod": "<mod_name>",
  "arguments": [ "<first_argument>", "<second_argument>", ... ]
}
```

This will make a child game to run its `manage` hook.

## Teleport a player from one game to another

```json
{
  "class": "teleport",
  "player": "<name_of_the_player>",
  "lang": "<optional_player_lang>",
  "destiny": "<destination_game_name>"
}
```

This call includes three possible cases:

- If the player is inside the caller game, then `destiny` must be a valid
  child game. The player will **enter** the child game running the
  corresponding **enter** hook, *without calling the caller leave hook*.
- If the player is inside a child game and `destiny` is a valid child game,
  the player will **leave** the child game running the corresponding **leave**
  hook, and then will **enter** the specified child game,
  running the corresponding **enter** hook.
- If the player is inside a child game and `destiny` is `.` (dot), the player
  will **leave** the child game running the corresponding **leave** hook,
  and then will enter the caller game, *without calling the caller enter hook*.

In the first two cases, if `lang` is provided then it shall be given in the
`enter` input. Otherwise, the server current lang shall be given.

If the player is not directly inside a child game (e.g. inside a branch of
a branch), then all corresponding **leave** hooks shall run recursively.

## Send a message to a branch

```json
{
  "class": "child",
  "branch": "<branch_name>",
  "content": object
}
```

This will execute the `parent` hook on the specified child game
giving `object` as input.

## Send a message to the parent game

```json
{
  "class": "parent",
  "content": object
}
```

This will execute the `child` hook on the parent game giving
the following object as input:

```json
{
  "branch": "<sender_branch_name>",
  "content": object
}
```

## Destroy branch

```json
{
  "class": "destroy",
  "name": "<branch_name>"
}
```

This will destroy an existing child game, a.k.a. *branch*.
All branches that the child may have will be recursively destroyed.

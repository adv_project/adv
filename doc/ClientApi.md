# ADV Client API

ADV Server/Client communication is done by sending and receiving messages
through a TCP socket (on port 2224 by default). This socket is encrypted with
SSL, and this way messages are sent and interpreted as JSON objects separated
by the NUL character.

When a player connects to a server, the connection remains idle until the user
*logs in* effectively. When logged in, the player can *logout* and the
connection will go back to idle mode again.

A *logged in* player has a unique *username*. There cannot be more than one
connection using the same username. As soon as a player logs in with an
already logged in username, the server forces a logout in the old connection.

The server waits 3 seconds for idle connections to login or register.
If the user didn't login or register after this time, the server closes the
connection and the user must perform a new connection to try again.

## Valid names for users and games

A valid name is a non-empty string of 30 or fewer characters.
A *game name* must start with a lowercase letter,
the rest of the string can only contain lowercase letters and digits.
*Usernames* are less restrictive. In this case, the only invalid characters
are spaces, control characters and basic symbols (excluding `_`).

## Client API Calls

Every JSON object that the server receives is a **Client API Call**.
They all have a `class` field and normally other arguments,
specific to each message class.

If a player sends an API Call that the server couldn't recognize or process,
the server will send a failure response like this:

```json
{
  "class": "error",
  "error": "invalid_api_call",
  "content": object
}
```

Where `object` is an exact copy of the message sent by the client.

- [Client API Calls for Accounts Management](doc/AccountClientApi.md)
  - [Logging in](doc/AccountClientApi.md#logging-in)
  - [Logging out](doc/AccountClientApi.md#logging-out)
  - [Registration](doc/AccountClientApi.md#registration)
  - [Password change](doc/AccountClientApi.md#password-change)
- [Client API Calls for getting Server Information](doc/InfoClientApi.md)
  - [Server Information](doc/InfoClientApi.md#server-information)
  - [Server Version](doc/InfoClientApi.md#server-version)
  - [List existent games](doc/InfoClientApi.md#list-existent-games)
  - [List installed mods](doc/InfoClientApi.md#list-installed-mods)
- [Client API Calls for Games Management](doc/GameManagementClientApi.md)
  - [Game creation](doc/GameManagementClientApi.md#game-creation)
  - [Game management](doc/GameManagementClientApi.md#game-management)
  - [Game destruction](doc/GameManagementClientApi.md#game-destruction)
  - [Transfer a game to another player](doc/GameManagementClientApi.md#transfer-a-game-to-another-player)
- [Client API Calls for Playing](doc/PlayClientApi.md)
  - [Enter a game](doc/PlayClientApi.md#enter-a-game)
  - [Leave a game](doc/PlayClientApi.md#leave-a-game)
  - [Send actions to game](doc/PlayClientApi.md#Send-actions-to-game)
  - [Receive actions from game](doc/PlayClientApi.md#receive-actions-from-game)
  - [Receive "write" requests from game](doc/PlayClientApi.md#receive-write-requests-from-game)
- [Client API Calls for Chatting](doc/ChatClientApi.md)
  - [Chatting with other connected players](doc/ChatClientApi.md#chatting-with-other-connected-players)
  - [Rolling dice](doc/ChatClientApi.md#rolling-dice)
  - [Getting the last chat and roll messages stored on the server](doc/ChatClientApi.md#getting-the-last-chat-and-roll-messages-stored-on-the-server)
- [Client API Calls for Administration Tasks](doc/AdminClientApi.md)
  - [Setting values for a certain server option](doc/AdminClientApi.md#setting-values-for-a-certain-server-option)
  - [Assign a role to a player](doc/AdminClientApi.md#assign-a-role-to-a-player)
  - [Revoke a role from a player](doc/AdminClientApi.md#revoke-a-role-from-a-player)
  - [Enable and disable new accounts registration](doc/AdminClientApi.md#enable-and-disable-new-accounts-registration)
  - [Enable and disable unregistered account logins](doc/AdminClientApi.md#enable-and-disable-unregistered-account-logins)
  - [Send message to a player as anonymous administrator](doc/AdminClientApi.md#send-message-to-a-player-as-anonymous-administrator)
  - [Send message to all connected players as anonymous administrator](doc/AdminClientApi.md#send-message-to-all-connected-players-as-anonymous-administrator)
  - [Kick a player from the server for a specific amount of time](doc/AdminClientApi.md#kick-a-player-from-the-server-for-a-specific-amount-of-time)
  - [Ban a player from the server forever](doc/AdminClientApi.md#ban-a-player-from-the-server-forever)
  - [List registered players](doc/AdminClientApi.md#list-registered-players)
  - [List connected players](doc/AdminClientApi.md#list-connected-players)
  - [Perform the working directory synchronization now](doc/AdminClientApi.md#perform-the-working-directory-synchronization-now)
  - [Other server events](doc/AdminClientApi.md#other-server-events)

# Game Hooks

Hooks are executed by the server after specific events; they are given
an input JSON object through the `GAME_DIRECTORY/input.json` file; and a JSON
object is grabbed from the `GAME_DIRECTORY/output.json` file,
which is interpreted as a list of JSON API Calls.
The `GAME_DIRECTORY` is given as an only argument for the hook.

Every hook that the server supports is listed below.
If the input JSON is not specified for a given hook, assume no input JSON.

## Server Start

Every time the server starts, `hooks/start` is executed.

## Server Stop

Every time the server stops, `hooks/stop` is executed.

## Game initialization

When a game is created, `hooks/init` is executed,
with the following input JSON:

```json
{
  "lang": "<server_language>"
}
```

## Management request from owner

Every time a *management request* is sent from the player who owns the game,
`hooks/manage` is executed, with the following input JSON:

```json
{
  "arguments": [ "<first_argument_sent>", "<second_argument_sent>", ... ]
}
```

This is **the only hook** that runs solely in one specific mod.

## Game was transfered to a new owner

If a game ownership is transfered,
`hooks/transfer` is executed, with the following input JSON:

```json
{
  "owner": "<new_owner>"
}
```

## Game destruction

When a game is destroyed, `hooks/destroy` is executed.
**This hook doesn't accept API Calls from `GAME_DIRECTORY/output.json` file.**

## Update

Every time `seconds-per-tick` seconds has passed after the last game update,
`hooks/update` is executed. If `seconds-per-tick` is 0, this hook is disabled.

## Action request from player

Every time an *action* is sent from a player that is inside the game,
`hooks/action` is executed, with the following input JSON:

```json
{
  "player": "<name_of_the_player_who_sent_the_action_request>",
  "action": [ "<first_argument_sent>", "<second_argument_sent>", ... ],
  "text": [ "<additional_lines_of_text_sent>", ... ]
}
```

This hook handles an additional file inside the game directory: `error.json`.
The format of this file is identical to that of `output.json` (it is read as a
list of JSON API Calls). Its purpose is that players always receive a useful
error message when many mods are working independently on the same game.

Before running this hook, the server writes the following object on
`error.json`:

```json
[
  {
    "class": "server"
  },
  {
    "class": "send",
    "player": "<name_of_the_player_who_sent_the_action_request>",
    "type": "error",
    "message": "The game couldn't handle this action: <action arguments>"
  }
]
```

If no mod changes this file, the `send` API call shall be sent to the player
who sent the action request.

The first object `{ "class": "server" }` is an indication that the file content
comes from the server. For players to receive consistent error messages,
mods should follow the instructions below:

- If the `error.json` file is empty or doesn't exist, **do nothing with it**.
In this case, a previous mod already handled the request successfully.
- If the `error.json` file has API calls in it (but **doesn't have** the
initial `{ "class": "server" }` object), this means none of the previous mods
knew how to handle the request. In this case there are two possibilities:
  1. The mod knows how to handle the request, in which case it should empty the
     error file; and
  2. The mod **doesn't** know how to handle the request,
     in which case it may or may not overwrite the error file the way it wants.

Right after running this hook (i.e. every mod in the game has finished its
tasks), the server will run all API Calls present in the resulting
`error.json` file.

## Player enters a game

Every time a player enters a game, `hooks/enter` is executed,
with the following input JSON:

```json
{
  "player": "<name_of_the_player_who_entered_the_game>",
  "lang": "<client_language>"
}
```

## Player leaves a game

Every time a player leaves a game, `hooks/leave` is executed,
with the following input JSON:

```json
{
  "player": "<name_of_the_player_who_left_the_game>"
}
```

If the player is inside a **branch** and sends a leave command to the server,
then all `leave` hooks are executed recursively,
from child to parent, up to the main game.

If the player is inside a **branch** and a game **kicks** the player,
then all `leave` hooks are executed recursively,
from child to parent, up to the game that executed the kick.
Then the `kick` hook will be executed on the parent of the game
that executed the kick, if exists.

Keep in mind that `leave` indicates that *the player may have left voluntarily*.
Although you can send messages to a player that left the game recently,
that's not the best idea because you'll never know if the player received them.
It's possible that the player closed the client right after leaving the game,
and it's also possible that the reason for leaving is because
the player just lost connection with the server.

## Player was kicked from a branch

Every time a branch kicks a player, `hooks/kick` is executed,
with the following input JSON:

```json
{
  "player": "<name_of_the_kicked_player>",
  "branch": "<branch_name>"
}
```

This means that the branch `leave` hook was executed for that player,
and now `player` is inside this game.

## Message from parent game

Every time the parent game sends a message to a game, `hooks/parent`
is executed on the child, with the following input JSON:

```json
{
  object
}
```

## Message from a branch

Every time one of the child games sends a message to the parent game,
`hooks/child` is executed on the parent, with the following input JSON:

```json
{
  "branch": "<sender_branch_name>",
  "content": object
}
```

## Offline hook

If a game has a process that is detached from the server, it can run API calls
through an **offline hook**. If the `GAME_DIRECTORY/hook.lock` file doesn't
exist, the server will read the `GAME_DIRECTORY/hook.json` file as API calls
and remove it after executing them. So, to run detached API commands, a mod
should:

- Check that neither `hook.lock` nor `hook.json` files exist (if any of them
exist, then probably another mod is already running an offline hook).
- Create the `hook.lock` file (if the lock file is not created, the server
will grab the `hook.json` file as soon as it's created, so it may grab a
broken file).
- Write all API calls on `hook.json`.
- Remove `hook.lock`.
- Wait until `hook.json` is removed. This means the server already executed
all the calls.


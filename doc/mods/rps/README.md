# Rock-paper-scissors for Adv

This mod is an implementation of the classic chance game; its main
purpose is to test Adv capabilities in managing multimod games, and
especially games into games.

The mod is perhaps as minimal as it could be; it understands the
following commands:

- 'p' or 'paper',
- 'r' or 'rock',
- 's' or 'scissors'

which are the only game actions, and

- 'players'

to see who is playing.

When the mod receives an action, it is buffered; when a second
action comes, both are compared, the result shown to all players,
and the buffer is flushed.

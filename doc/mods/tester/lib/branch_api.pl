#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub callCreate {
  my $name = shift;
  my $mods = shift;
  my $object = {
    "class"   => "create",
    "name"  => $name,
    "mods"  => $mods
  };
  return $object;
}

sub callManage {
  my $branch = shift;
  my $mod = shift;
  my $arguments = shift;
  my $object = {
    "class"   => "manage",
    "branch"  => $branch,
    "mod"  => $mod,
    "arguments"  => $arguments
  };
  return $object;
}

sub callDestroy {
  my $name = shift;
  my $object = {
    "class" => "destroy",
    "name" => $name
  };
  return $object;
}

sub callTeleport {
  my $player = shift;
  my $destiny = shift;
  my $lang = shift;
  my $object = {
    "class" => "teleport",
    "player" => $player,
    "destiny" => $destiny
  };
  if ($lang) { $object->{'lang'} = $lang; }
  return $object;
}

sub callChild {
  my $branch = shift;
  my $content = shift;
  my $object = {
    "class" => "child",
    "branch" => $branch,
    "content" => $content
  };
  return $object;
}

sub callParent {
  my $content = shift;
  my $object = {
    "class" => "parent",
    "content" => $content
  };
  return $object;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub callKick {
  my $player = shift;
  my $object = {
    "class"   => "kick",
    "player"  => $player
  };
  return $object;
}

sub callRun {
  my $path = shift;
  my $input = shift;
  $input = {} unless ($input);
  my $object = {
    "class" => "run",
    "path"  => $path,
    "input" => $input
  };
  return $object;
}

sub callAbort {
  my $object = { "class" => "abort" };
  return $object;
}

sub callUpdate {
  my $object = { "class" => "update" };
  return $object;
}

1;

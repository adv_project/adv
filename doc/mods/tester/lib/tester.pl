#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

no warnings;

require $moddir . "/lib/io.pl";
require $moddir . "/lib/message_api.pl";
require $moddir . "/lib/properties_api.pl";
require $moddir . "/lib/workflow_api.pl";
require $moddir . "/lib/branch_api.pl";

$stringsfile = $moddir . "/strings.json";
$statefile = $wdir . "/tester_state.json";

sub readState {
  local $/;
  open my $fh, '<', $statefile;
  my $json = <$fh>;
  close $fh;
  $state = JSON::decode_json($json);
}

sub writeState {
  my $json = JSON->new->pretty(1)->encode($state);
  open my $fh, '>', $statefile;
  print $fh $json;
  close $fh;
}

sub loadStrings {
  local $/;
  open my $fh, '<', $stringsfile;
  my $json = <$fh>;
  close $fh;
  $strings = JSON::decode_json($json);
}

sub help {
  my $arg = shift;
  my @lines = ();
  if (!$arg) { return @lines; }
  if (exists $strings->{$arg}) {
    push(@lines, $BOLD . $BLUE . $strings->{$arg}->{'usage'});
    foreach my $line (@{$strings->{$arg}->{'brief'}}) {
      push(@lines, "  " . $line);
    }
  } else {
    push(@lines, $RED . "No help available for " . $BOLD . $arg);
  }
  return @lines;
}

sub info {
  my $arg = shift;
  my @lines = ();
  if (!$arg) { return @lines; }
  if (exists $strings->{$arg}) {
    push(@lines, $BOLD . $GREEN . $strings->{$arg}->{'title'});
    foreach my $line (@{$strings->{$arg}->{'long'}}) {
      push(@lines, "  " . $line);
    }
  } else {
    push(@lines, $RED . "No info available for " . $BOLD . $arg);
  }
  return @lines;
}

sub commands {
  my @lines = ();
  push(@lines, "");
  foreach my $line (@{$strings->{'commands'}->{'commands'}}) {
    push(@lines, $GREEN . $line);
  }
  push(@lines, "");
  return @lines;
}

1;

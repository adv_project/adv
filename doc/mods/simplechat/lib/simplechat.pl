#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

no warnings;

require $moddir . "/lib/io.pl";
require $moddir . "/lib/message_api.pl";
require $moddir . "/lib/properties_api.pl";

$modconfigfile = $wdir . "/simplechat_config.json";

# Read $modconfigfile and store its content inside the $config global variable.
sub readConfig {
  local $/;
  open my $fh, '<', $modconfigfile;
  my $json = <$fh>;
  close $fh;
  $config = JSON::decode_json($json);
}

# Write $config global variable on $modconfigfile.
sub writeConfig {
  my $json = JSON->new->pretty(1)->encode($config);
  open my $fh, '>', $modconfigfile;
  print $fh $json;
  close $fh;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub callPause {
  my $pause_message = shift;
  my $object = {
    "class"         => "pause",
    "pause-message" => $pause_message
  };
  return $object;
}

sub callResume {
  my $object = { "class" => "resume" };
  return $object;
}

sub callSecondsPerTick {
  my $secspertick = shift;
  my $object = {
    "class"            => "seconds-per-tick",
    "seconds-per-tick" => $secspertick
  };
  return $object;
}

sub callDescription {
  my $description = shift;
  my $object = {
    "class"       => "description",
    "description" => $description
  };
  return $object;
}

1;

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVINPUT_H
#define ADVINPUT_H

#define CTRL(c) ((c) & 037)

#include <string>
#include <vector>
#include <wchar.h>

#ifndef NCURSESWDIR
#include <cursesw.h>
#else
#include <ncursesw/cursesw.h>
#endif

class AdvInput
{
  public:
    AdvInput(int cols);
    wchar_t getChar();
    bool handleCompletion();
    void setCols(int cols);
    std::wstring cmdline, cmdline_backup, prompt, completion;
    std::vector<std::wstring> history;
    int cursor, offset, cols, history_position;
    unsigned long last_completion; // Inverted index of last completed command in history
    bool insert;
};

#endif // ADVINPUT_H

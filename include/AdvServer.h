/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVSERVER_H
#define ADVSERVER_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local
#define DEFAULT_PORT 2224 // efi-mg (Easy Flexible Internet/Multiplayer Games)

#include <signal.h>
#include <string>
#include <errno.h>
#include <sys/stat.h>
#include <fts.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <list>
#include <iostream>
#include <fstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <pthread.h>
#include "sigslot.h"
#include "json.hpp"
#include "Logger.h"
#include "md5wrapper.h"
#include "AdvPlayerConnection.h"
#include "AdvGame.h"

struct game_birth_t {
  std::string full_name;
  std::string owner;
  nlohmann::json mods;
  std::string title;
  nlohmann::json libs;
};

class AdvServer : public sigslot::has_slots<>
{
  public:
    AdvServer();
    ~AdvServer() { if (logger) delete logger; }
    int exec(int argc, char **argv);
    int start();
    void stop();
    static void* callConnectionsThread(void *This);
    void runConnectionsThread();
    void log(int log_level, std::string str);
    void initial_sync();
    void sync(std::string origin = "command_line");
    bool isAdmin(std::string username);
    bool isModerator(std::string username);
    bool isRole(std::string role);
    bool checkGameMods(nlohmann::json mods, bool branch = false, bool are_libs = false);
    nlohmann::json resolveModDependencies(nlohmann::json mod_deps);
    nlohmann::json resolveModLibs(nlohmann::json mods, nlohmann::json branch_libs = nlohmann::json::parse("[]"));
    std::string gameDirectoryFromName(std::string full_name);
    static bool checkNameSanity(std::string name);
    static bool checkUserNameSanity(std::string name);
    static std::string advGetText(std::string label);
    static std::string replaceAllInstances(std::string string,
                                           std::string grab, std::string replace);
    static int mkdirrecursively(mode_t mode, std::string root_path, std::string path);
    static int rmdirrecursively(std::string path);
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }
    static bool checkBoolean(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) && (*j)[p].type() == nlohmann::json::value_t::boolean); }
    static bool checkString(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) && (*j)[p].type() == nlohmann::json::value_t::string); }
    static bool checkInteger(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) &&
        ((*j)[p].type() == nlohmann::json::value_t::number_integer ||
        (*j)[p].type() == nlohmann::json::value_t::number_unsigned)); }
    static bool checkNumber(nlohmann::json *j, std::string p)
        { return (hasParameter(j, p) &&
          ((*j)[p].type() == nlohmann::json::value_t::number_integer ||
          ((*j)[p].type() == nlohmann::json::value_t::number_unsigned ||
          (*j)[p].type() == nlohmann::json::value_t::number_float))); }
    static bool checkArray(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) && (*j)[p].type() == nlohmann::json::value_t::array &&
        (*j)[p].size() > 0 && (*j)[p][0].type() == nlohmann::json::value_t::string); }
    static nlohmann::json strings; // Server text strings in JSON format

  protected:
    std::string working_directory; // Directory under /run where data will be constantly changing
    std::string static_directory; // Directory where data will be statically stored and synced
    std::string lang; // Server language
    std::string dbname; // Database name
    std::string dbhost; // Database host
    int dbport; // Database port
    std::string dbuser; // Database user
    std::string dbpass; // Database password
    std::string rsync_excludes;
    std::string sync_arguments;
    nlohmann::json motd; // Message of the day
    nlohmann::json admins; // Server administrator users
    nlohmann::json moderators; // Server moderators
    nlohmann::json cache; // Mods and hooks cache
    nlohmann::json chat_backlog;
    unsigned long creation_rate; // Minimum time between new games creations
    unsigned long chat_backlog_size;
    unsigned long max_players;
    unsigned int max_games;
    bool enable_registration;
    bool enable_unregistered;
    bool do_exit;
    time_t uptime;
    time_t last_created;
    time_t last_sync;
    int port; // Server port
    int server_fd; // Server socket file descriptor
    // How many microseconds will the server thread wait when there's nothing to do
    unsigned long delay;
    unsigned long sync_period; // Static directory synchronization period, in seconds
    unsigned long socket_period; // Seconds between socket creation tries
    unsigned long socket_tries; // Socket creation tries
    Logger *logger;
    pthread_t thread;
    std::list<AdvPlayerConnection*> connections; // List of current player connections
    nlohmann::json players; // List of registered players
    std::list<AdvGame> games; // All existing games
    pthread_mutex_t log_mutex;
    pthread_mutex_t connections_mutex;
    int connections_readers;
    pthread_mutex_t handles_mutex;
    int handles_readers;
    pthread_mutex_t ingames_mutex;
    int ingames_readers;
    pthread_mutex_t accounts_mutex;
    pthread_mutex_t games_mutex;
    int games_readers;
    std::list<game_birth_t> scheduled_creations;
    pthread_mutex_t creation_mutex;
    std::list<std::list<AdvGame>::iterator> scheduled_destructions;
    pthread_mutex_t destruction_mutex;
    pthread_mutex_t config_mutex;
    pthread_mutex_t sync_mutex;
    pthread_mutex_t server_hooks_mutex;
    nlohmann::json client_api; // Client API Calls grouped by category

    void updateCache();
    void readLang();
    void readConfig();
    void writeConfig();
    void readChatBacklog();
    void writeChatBacklog();
    void loadStrings();
    void readPlayersFile();
    void writePlayersFile();
    void loadGames();
    void destroyGame(std::list<AdvGame>::iterator game);
    void leaveFromGame(std::list<AdvPlayerConnection*>::iterator conn, std::string game = "");
    void logoutFromServer(std::list<AdvPlayerConnection*>::iterator conn, bool banned = false, time_t wait = 0);
    void banPlayer(std::string handle, time_t wait);
    void runHook(std::string hook, std::string input);

    void startReadingConnections();
    void stopReadingConnections();
    void writeConnections();
    void startReadingGames();
    void stopReadingGames();
    void writeGames();
    void startReadingHandles();
    void stopReadingHandles();
    void writeHandles();
    void startReadingIngames();
    void stopReadingIngames();
    void writeIngames();

    void onSendMessage(nlohmann::json message);
    void onKick(AdvGame *game, std::string player);
    void onRequestReceived(AdvPlayerConnection* advPlayerConnection, int fd, std::string request);
    void onModApiCall(AdvGame *game, nlohmann::json call);
    void sendFailureResponse(std::list<AdvPlayerConnection*>::iterator it,
                             nlohmann::json response, std::string reason, nlohmann::json content);
    void handleAccountRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void handleInfoRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void handleGameManagementRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void handlePlayRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void handleChatRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void handleAdminRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message);
    void onConnectionClosed(AdvPlayerConnection* advPlayerConnection, int fd, std::string err);
};

#endif // ADVSERVER_H

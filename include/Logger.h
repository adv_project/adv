/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <ctime>
#include <string>

class Logger
{
  public:
    Logger(int log_level, std::string default_log_file, std::string log_directory, std::string store_directory);
    void setLogLevel(int log_level);
    void setDefaultLogFile(std::string default_log_file);
    void setLogDirectory(std::string log_directory);
    void setStoreDirectory(std::string store_directory);
    void log(int log_level, std::string log_file, std::string log_message);
    static std::string logLevel(int log_level, bool shortened);
    int getLogLevel() const { return log_level; }
    std::string getDefaultLogFile() const { return default_log_file; }
    std::string getLogDirectory() const { return log_directory; }

  protected:
    int log_level;
    std::string default_log_file;
    std::string log_directory;
    std::string store_directory;
    char last_log_date[16];
};

#endif // LOGGER_H

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVGAME_H
#define ADVGAME_H

#include <signal.h>
#include <sys/wait.h>
#include <string>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fstream>
#include <pthread.h>
#include "json.hpp"
#include "sigslot.h"

class AdvGame : public sigslot::has_slots<>
{
  public:
    AdvGame(std::string name, std::string game_directory, std::string owner = "",
            unsigned long seconds_per_tick = 0, nlohmann::json mods = nlohmann::json::parse("[]"),
            std::string title = "", std::string dbname = "adv", std::string dbhost = "localhost",
            int dbport = 5432, std::string dbuser = "adv", std::string dbpass = "",
            nlohmann::json libs = nlohmann::json::parse("[]"));
    void serverStarted();
    void serverStopped();
    void init(std::string lang = "en");
    void manage(std::string mod, nlohmann::json arguments);
    void destroy();
    void update();
    void action(std::string player, nlohmann::json action);
    void playerEntered(std::string player, std::string lang = "en");
    void playerLeft(std::string player);
    void playerKicked(std::string player, std::string branch_name);
    void messageFromParent(nlohmann::json input);
    void messageFromChild(std::string branch_name, nlohmann::json input);
    void setOwner(std::string new_owner);
    void pause(nlohmann::json pause_message);
    void resume();
    void lock() { pthread_mutex_lock(&game_mutex); }
    void unlock() { pthread_mutex_unlock(&game_mutex); }
    void updateDatabaseConfig(std::string dbname, std::string dbhost, int dbport, std::string dbuser, std::string dbpass);
    void pushBranch(std::string branch_name);
    void popBranch(std::string branch_name);
    bool isPaused() const { return properties["paused"]; }
    std::string getName() const { return properties["name"].get<std::string>(); }
    std::string getOwner() const { return properties["owner"].get<std::string>(); }
    time_t getCreated() const { return (time_t) properties["created"]; }
    nlohmann::json getMods() const { return properties["mods"]; }
    std::string getTitle() const { return properties["title"].get<std::string>(); }
    nlohmann::json getDescription() const { return properties["description"]; }
    nlohmann::json getPauseMessage() const { return properties["pause-message"]; }
    static void loadModApi(nlohmann::json j) { mod_api = j; }
    static void updateModsCache(nlohmann::json j) { mods_cache = j; }
    static void loadStaticDirectory(std::string d) { static_directory = d; }
    static nlohmann::json getModApi() { return mod_api; }
    static nlohmann::json getModsCache() { return mods_cache; }
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }

    sigslot::signal2<int, std::string> log;
    sigslot::signal2<AdvGame*, nlohmann::json> sendCall;
    sigslot::signal1<nlohmann::json> sendMessage;
    sigslot::signal2<AdvGame*, std::string> kick;

  protected:
    std::string game_directory; // Game directory inside /run structure
    nlohmann::json properties; // Game properties stored in game_directory/properties.json
    pthread_mutex_t game_mutex; // Game modifications must be thread-safe
    bool abort_hook; // Flag used to abort running a hook on subsequent mods
    bool destroyed; // Flag used to prevent running hooks in case the game is being destroyed
    static nlohmann::json mod_api; // Mod API Calls grouped by category
    static nlohmann::json mods_cache;
    static std::string static_directory; // Server static directory

    void runHook(std::string hook, std::string input);
    std::string runScript(std::string hook_path, std::string input);
    void readProperties();
    void writeProperties();
    void runAPICommands(std::string commands);
    bool runMessageApiCommand(nlohmann::json &call);
    bool runGameWorkflowApiCommand(nlohmann::json &call);
    bool runGamePropertiesApiCommand(nlohmann::json &call);
};

#endif // ADVGAME_H

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVPLAYERCONNECTION_H
#define ADVPLAYERCONNECTION_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local

#include <string>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <pthread.h>
#include "sigslot.h"

class AdvPlayerConnection : public sigslot::has_slots<>
{
  public:
    std::string handle, game;

    AdvPlayerConnection(int fd, SSL_CTX* ssl_ctx);
    static void* callThread(void *This);
    void runThread();
    ssize_t sendEvent(std::string event);
    int getFd() const { return fd; }
    void exit() { do_exit = true; }
    static void setDelay(long new_delay) { if (new_delay > 0) delay = new_delay; }
    static void setTimeout(time_t new_timeout) { timeout = new_timeout; }

    sigslot::signal3<AdvPlayerConnection*, int, std::string> requestReceived;
    sigslot::signal3<AdvPlayerConnection*, int, std::string> connectionClosed;

  protected:
    static unsigned long delay; // How many microseconds will the connection thread
                                // wait when there are no incoming characters
    static time_t timeout; // Maximum allowed time for idle connections, in seconds
    bool do_exit;
    int fd; // Connection socket file descriptor
    SSL_CTX* ssl_ctx; // SSL Context
    SSL* ssl; // Connection SSL structure
    time_t uptime;
    pthread_t thread;
    pthread_mutex_t events_mutex;
};

#endif // ADVPLAYERCONNECTION_H

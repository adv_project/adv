/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVCLIENT_H
#define ADVCLIENT_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local
#define DEFAULT_PORT 2224 // efi-mg (Easy Flexible Internet/Multiplayer Games)

#include <signal.h>
#include <string>
#include <locale>
#include <boost/locale/encoding_utf.hpp>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <pthread.h>
#include "json.hpp"
#include "sigslot.h"
#include "AdvInput.h"
#include "Window.h"

class AdvClient : public sigslot::has_slots<>
{
  public:
    AdvClient();
    ~AdvClient();
    int exec(int argc, char **argv);
    static void* callThread(void *This);
    void runThread();
    ssize_t sendRequest(std::string request);
    void loadStrings();
    void loadCommands();
    void loadConfig();
    void saveConfig();
    void startUI();
    void stopUI();
    void updateView();
    void exit() { do_exit = true; }
    void eventReceived(std::string cmd);
    void handleInput(wchar_t key);
    void handleBoundKey(wchar_t key);
    void parseCommand(std::string cmd);
    void performAction(std::string action, std::string arguments);
    nlohmann::json getTextFromEditor(nlohmann::json ret = nlohmann::json::parse("[]"));
    void printText(std::string type, std::string text, std::string tag = "");
    void showDetailedHelp(std::string arg);
    void windowResized(int SIG);
    std::string advGetText(std::string label);
    std::string advGetError(std::string label)
      { return advGetText(std::string("server_error_") + label); }
    void pickRandomString(std::string label);
    void releaseRandomString() { random_string = 0; }
    std::string getElapsedTime(unsigned long diff);
    static std::string getTime();
    static std::string getDateAndTime(time_t moment = time(0));
    static time_t parseElapsedTime(std::string str);
    static std::string replaceAllInstances(std::string string,
                                           std::string grab, std::string replace);
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }
    static bool checkString(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) && (*j)[p].type() == nlohmann::json::value_t::string); }
    static bool checkInteger(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) &&
        ((*j)[p].type() == nlohmann::json::value_t::number_integer ||
        (*j)[p].type() == nlohmann::json::value_t::number_unsigned)); }
    static bool checkNumber(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) &&
        ((*j)[p].type() == nlohmann::json::value_t::number_integer ||
        ((*j)[p].type() == nlohmann::json::value_t::number_unsigned ||
        (*j)[p].type() == nlohmann::json::value_t::number_float))); }
    static bool checkArray(nlohmann::json *j, std::string p)
      { return (hasParameter(j, p) && (*j)[p].type() == nlohmann::json::value_t::array &&
        (*j)[p].size() > 0 && (*j)[p][0].type() == nlohmann::json::value_t::string); }
    static void nextArg(std::string &string, std::string &arg);
    static in_addr getIPAddress(std::string hostname);
    void loadSupportedLanguages();
    static bool isBoundable(std::string key_name);

  protected:
    bool do_exit, ui, connected, new_login, new_lines_on_view, play_mode, tab_mode, asked_for_motd;
    int client_fd; // Client socket file descriptor
    unsigned int random_string;
    SSL_CTX* ssl_ctx; // SSL Context
    SSL* ssl; // Connection SSL structure
    pthread_t thread;
    pthread_mutex_t view_mutex; // updateView() must be thread-safe
    pthread_mutex_t current_server_mutex; // current_server must be thread-safe
    pthread_mutex_t editor_mutex; // text editor access must be thread-safe
    nlohmann::json strings; // Client text strings in JSON format
    nlohmann::json commands; // Client commands list in JSON format
    nlohmann::json config; // Complete client configuration in JSON format
    nlohmann::json langs; // Languages Matrix
    WINDOW *inputWindow;
    Window *outputWindow;
    AdvInput *input; // Keyboard input handler
    nlohmann::json current_server; // Currently active server
};

#endif // ADVCLIENT_H

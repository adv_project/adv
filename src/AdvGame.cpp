/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvServer.h"
#include "../include/AdvGame.h"
#include "AdvGameApi.cpp"

nlohmann::json AdvGame::mod_api;
nlohmann::json AdvGame::mods_cache;
std::string AdvGame::static_directory = "/";

AdvGame::AdvGame(std::string name, std::string game_directory, std::string owner,
                 unsigned long seconds_per_tick, nlohmann::json mods, std::string title,
                 std::string dbname, std::string dbhost, int dbport, std::string dbuser, std::string dbpass, nlohmann::json libs)
{
  game_mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_mutex_lock(&game_mutex);
  this->game_directory = game_directory;
  destroyed = false;
  struct stat st;
  // Check if properties file exists
  if (stat (std::string(game_directory + "/properties.json").c_str(), &st)) { // New game
    AdvServer::mkdirrecursively(0700, "/", game_directory);
    properties["name"] = name;
    properties["owner"] = owner;
    properties["created"] = time(0);
    properties["seconds-per-tick"] = seconds_per_tick;
    properties["mods"] = nlohmann::json::parse("[]");
    properties["games"] = nlohmann::json::parse("[]");
    if (title.empty()) properties["title"] = name; else properties["title"] = title;
    properties["description"] = nlohmann::json::parse("[]");
    properties["paused"] = true;
    std::string pause_message = std::string("[\"") + AdvServer::advGetText("game_not_initialized_yet") + "\"]";
    properties["pause-message"] = nlohmann::json::parse(pause_message);
    properties["dbname"] = dbname;
    properties["dbhost"] = dbhost;
    properties["dbport"] = dbport;
    properties["dbuser"] = dbuser;
    properties["dbpass"] = dbpass;
    AdvServer::mkdirrecursively(0700, game_directory, "mods");
    AdvServer::mkdirrecursively(0700, game_directory, "games");
    symlink(std::string(static_directory + "/users").c_str(), std::string(game_directory + "/users").c_str());
    unsigned int l;
    for (unsigned int i = 0; i < mods.size(); ++i) {
      symlink(std::string(std::string(PREFIX) + "/share/adv/mods/" + mods[i].get<std::string>()).c_str(),
              std::string(game_directory + "/mods/" + mods[i].get<std::string>()).c_str());
      for (l = 0; l < libs.size(); ++l)
        if (libs[l] == mods[i]) break;
      if (l == libs.size()) properties["mods"].push_back(mods[i]);
    }
    for (unsigned int i = 0; i < libs.size(); ++i)
      symlink(std::string(std::string(PREFIX) + "/share/adv/mods/" + libs[i].get<std::string>()).c_str(),
              std::string(game_directory + "/mods/" + libs[i].get<std::string>()).c_str());
  }
  else readProperties(); // Existent game
  properties["last-updated"] = time(0);
  writeProperties();
  pthread_mutex_unlock(&game_mutex);
}

// Run start hook
void AdvGame::serverStarted()
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "start");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  runHook("start", "{}");
  pthread_mutex_unlock(&game_mutex);
}

// Run stop hook
void AdvGame::serverStopped()
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "stop");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  runHook("stop", "{}");
  pthread_mutex_unlock(&game_mutex);
}

// Run init hook
void AdvGame::init(std::string lang)
{
  pthread_mutex_lock(&game_mutex);
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "init");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["lang"] = lang;
  runHook("init", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

// Run manage hook
void AdvGame::manage(std::string mod, nlohmann::json arguments)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_for_mod_on_game"),
    "${HOOK}", "manage");
  str = AdvServer::replaceAllInstances(str, "${MOD}", mod);
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  struct stat st;
  str = game_directory + "/mods/" + mod + "/hooks/manage";
  if ( (stat(str.c_str(), &st) < 0) || ((st.st_mode & S_IEXEC) == 0) ) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  nlohmann::json input;
  input["arguments"] = arguments;
  runAPICommands(runScript(str, input.dump()));
  pthread_mutex_unlock(&game_mutex);
}

// Run destroy hook
void AdvGame::destroy()
{
  pthread_mutex_lock(&game_mutex);
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "destroy");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  runHook("destroy", "{}");
  destroyed = true;
  pthread_mutex_unlock(&game_mutex);
}

// Run update hook
void AdvGame::update()
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  time_t now = time(0);
  struct stat st;
  std::ifstream hook_file;
  std::string commands;
  std::string str = game_directory + "/hook.lock";
  if (stat(str.c_str(), &st) < 0) { // Hook lock doesn't exist, so we should read hook.json
    if (stat(std::string(game_directory + "/hook.json").c_str(), &st) == 0) {
      hook_file.open(std::string(game_directory + "/hook.json").c_str());
      if (hook_file.good()) {
        while (std::getline(hook_file, str)) commands += str;
        hook_file.close();
      }
      str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
        "${HOOK}", "offline");
      str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
      log.emit(7, str);
      runAPICommands(commands);
      unlink(std::string(game_directory + "/hook.json").c_str());
    }
  }
  if ( properties["paused"] || ((unsigned long) properties["seconds-per-tick"] == 0) ||
    (now - (time_t) properties["last-updated"] < properties["seconds-per-tick"]) ) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "update");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  runHook("update", "{}");
  properties["last-updated"] = now;
  writeProperties();
  pthread_mutex_unlock(&game_mutex);
}

// Run action hook
void AdvGame::action(std::string player, nlohmann::json action)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "action");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input, error, error_api_call;
  std::string commands;
  std::ofstream error_write_file;
  std::ifstream error_read_file;
  input["player"] = player;
  input["action"] = action["action"];
  input["text"] = action["text"];
  error = nlohmann::json::parse("[]");
  error_api_call["class"] = "server";
  error.push_back(error_api_call);
  error_api_call["class"] = "send";
  error_api_call["player"] = player;
  error_api_call["type"] = "error";
  str.clear();
  for (int i = 0; i < action["action"].size(); ++i) str += action["action"][i].get<std::string>() + " ";
  if (!str.empty() && std::isspace(str.back())) str.pop_back();
  error_api_call["message"] = AdvServer::replaceAllInstances(AdvServer::advGetText("game_couldnt_handle_action"),
    "${ACTION}", str);
  error.push_back(error_api_call);
  error_write_file.open(std::string(game_directory + "/error.json").c_str());
  if (error_write_file.good()) {
    error_write_file << std::setw(2) << error;
    error_write_file.close();
  }
  runHook("action", input.dump());
  error_read_file.open(std::string(game_directory + "/error.json").c_str());
  if (error_read_file.good()) {
    while (std::getline(error_read_file, str)) commands += str;
    error_read_file.close();
  }
  runAPICommands(commands);
  unlink(std::string(game_directory + "/error.json").c_str());
  pthread_mutex_unlock(&game_mutex);
}

// Run enter hook
void AdvGame::playerEntered(std::string player, std::string lang)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "enter");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["player"] = player;
  input["lang"] = lang;
  runHook("enter", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

// Run leave hook
void AdvGame::playerLeft(std::string player)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "leave");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["player"] = player;
  runHook("leave", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::playerKicked(std::string player, std::string branch_name)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "kick");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["player"] = player;
  input["branch"] = branch_name;
  runHook("kick", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::messageFromParent(nlohmann::json input)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "parent");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  runHook("parent", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::messageFromChild(std::string branch_name, nlohmann::json message)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "child");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["branch"] = branch_name;
  input["content"] = message;
  runHook("child", input.dump());
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::setOwner(std::string new_owner)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"),
    "${HOOK}", "transfer");
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  nlohmann::json input;
  input["owner"] = new_owner;
  runHook("transfer", input.dump());
  properties["owner"] = new_owner;
  writeProperties();
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::pause(nlohmann::json pause_message)
{
  properties["paused"] = true;
  properties["pause-message"] = pause_message;
  writeProperties();
}

void AdvGame::resume()
{
  properties["paused"] = false;
  properties["pause-message"] = nlohmann::json::parse("[]");
  writeProperties();
}

void AdvGame::pushBranch(std::string branch_name)
{
  pthread_mutex_lock(&game_mutex);
  if (destroyed) {
    pthread_mutex_unlock(&game_mutex);
    return;
  }
  int branch;
  for (branch = 0; branch < properties["games"].size(); ++branch)
    if (properties["games"][branch] == branch_name) break;
  if (branch >= properties["games"].size()) {
    properties["games"].push_back(branch_name);
    writeProperties();
  }
  pthread_mutex_unlock(&game_mutex);
}

void AdvGame::popBranch(std::string branch_name)
{
  for (int branch = 0; branch < properties["games"].size(); ++branch)
    if (properties["games"][branch] == branch_name) {
      properties["games"].erase(branch);
      break;
    }
    writeProperties();
}

void AdvGame::updateDatabaseConfig(std::string dbname, std::string dbhost, int dbport, std::string dbuser, std::string dbpass)
{
  properties["dbname"] = dbname;
  properties["dbhost"] = dbhost;
  properties["dbport"] = dbport;
  properties["dbuser"] = dbuser;
  properties["dbpass"] = dbpass;
  writeProperties();
}

void AdvGame::runHook(std::string hook, std::string input)
{
  abort_hook = false;
  std::string str;
  for (int mod = 0; mod < properties["mods"].size() && !abort_hook; ++mod) {
    if (!hasParameter(&mods_cache, properties["mods"][mod].get<std::string>()) ||
      !hasParameter(&mods_cache[properties["mods"][mod].get<std::string>()]["hooks"], hook))
      continue;
    str = game_directory + "/mods/" + properties["mods"][mod].get<std::string>() + "/hooks/" + hook;
    if (hook == "destroy")
      runScript(str, input);
    else
      runAPICommands(runScript(str, input));
  }
}

std::string AdvGame::runScript(std::string path, std::string input)
{
  std::string str = AdvServer::replaceAllInstances(AdvServer::advGetText("running_script_on_game"),
    "${SCRIPT}", path);
  str = AdvServer::replaceAllInstances(str, "${GAME}", properties["name"].get<std::string>());
  log.emit(7, str);
  std::string output;
  std::ofstream input_file;
  input_file.open(std::string(game_directory + "/input.json").c_str());
  if (input_file.good()) {
    input_file << std::setw(2) << input;
    input_file.close();
  }
  path += " " + game_directory;
  system(path.c_str());
  unlink(std::string(game_directory + "/input.json").c_str());
  std::ifstream output_file;
  output_file.open(std::string(game_directory + "/output.json").c_str());
  if (output_file.good()) {
    while (std::getline(output_file, str)) output += str;
    output_file.close();
  }
  unlink(std::string(game_directory + "/output.json").c_str());
  return output;
}

void AdvGame::readProperties()
{
  std::ifstream properties_file;
  properties_file.open(std::string(game_directory + "/properties.json").c_str());
  if (properties_file.good()) {
    properties_file >> properties;
    properties_file.close();
  }
}

void AdvGame::writeProperties()
{
  std::ofstream properties_file;
  properties_file.open(std::string(game_directory + "/properties.json").c_str());
  if (properties_file.good()) {
    properties_file << std::setw(2) << properties;
    properties_file.close();
  }
}

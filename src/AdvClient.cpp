/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvClient.h"
#include "AdvClientActions.cpp"

// Initialize client default values
AdvClient::AdvClient()
{
  loadSupportedLanguages();
  loadConfig();
  loadStrings();
  srand(time(0));
  setlocale(LC_ALL, "");
  do_exit = false;
  ui = false;
  connected = false;
  new_login = true;
  new_lines_on_view = false;
  play_mode = true;
  tab_mode = false;
  asked_for_motd = false;
  input = nullptr;
  outputWindow = nullptr;
  view_mutex = PTHREAD_MUTEX_INITIALIZER;
  current_server_mutex = PTHREAD_MUTEX_INITIALIZER;
  editor_mutex = PTHREAD_MUTEX_INITIALIZER;
  client_fd = -1;
  random_string = 0;
}

AdvClient::~AdvClient()
{
  if (input != nullptr) delete input;
  if (outputWindow != nullptr) delete outputWindow;
}

// Read argc and argv as command line arguments and perform the requested action
int AdvClient::exec(int argc, char **argv)
{
  std::string usage = std::string(CLIENTPROGRAM) + " " + advGetText("command_line_usage");
  for (int i = 1; i < argc; ++i) {
    if ( (strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0) ) {
      std::cout << usage << std::endl;
      int line = 1;
      while (advGetText("command_line_help_" + std::to_string(line)) !=
             std::string("command_line_help_" + std::to_string(line))) {
        std::cout << advGetText("command_line_help_" + std::to_string(line)) << std::endl;
        ++line;
      }
      return 0;
    }
    else if ( (strcmp(argv[i], "-u") == 0) || (strcmp(argv[i], "--usage") == 0) ) {
      std::cout << replaceAllInstances(advGetText("command_line_full_usage"), "${USAGE}", usage) << std::endl;
      return 0;
    }
    else if ( (strcmp(argv[i], "-v") == 0) || (strcmp(argv[i], "--version") == 0) ) {
      std::string version = advGetText("version");
      version = replaceAllInstances(version, "${PROGRAM}", CLIENTPROGRAM);
      version = replaceAllInstances(version, "${VERSION}", PROGRAMVERSION);
      std::cout << version << std::endl;
      std::cout << replaceAllInstances(advGetText("license"), "${LICENSE}", LICENSE) << std::endl;
      std::cout << replaceAllInstances(advGetText("written_by"), "${AUTHOR}", AUTHOR) << std::endl;
      return 0;
    }
    else {
      current_server["name"] = argv[i];
      break;
    }
  }
  loadCommands();
  input = new AdvInput(0);
  outputWindow = new Window(0, 0, 0, 0, 0, config["buffer-size"]);
  startUI();
  if (hasParameter(&config, "history")) {
    std::string str;
    for (int i = 0; i < config["history"].size(); ++i) {
      str = config["history"][i];
      input->history.push_back(boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size()));
    }
  }
  if (hasParameter(&config, "buffer") && config["buffer"].size() > 0) {
    std::string separator = "--------------------------------------------------------------------------------";
    for (int i = 0; i < config["buffer"].size(); ++i)
      outputWindow->addLine(config["buffer"][i]);
    if (config["buffer"][config["buffer"].size() - 1] != separator) outputWindow->addLine(separator);
  }
  if (config["welcome-message"].get<std::string>() == "yes") {
    int line = 1;
    pickRandomString("welcome_message_1");
    while (advGetText("welcome_message_" + std::to_string(line)) !=
           std::string("welcome_message_" + std::to_string(line))) {
      printText("server_info", advGetText("welcome_message_" + std::to_string(line)));
      ++line;
    }
    releaseRandomString();
    showDetailedHelp("tldr");
  }
  updateView();
  if (hasParameter(&current_server, "name") && current_server["name"] != "")
    performAction("login", current_server["name"]);
  pthread_create(&thread, nullptr, &callThread, this);
  wchar_t key;
  unsigned long ping_period = 0;
  while (!do_exit) {
    if (connected && !ping_period) // Check the connection by sending a dummy message (ping) to the server
      if (sendRequest("") != 1) connected = false;
    if (ui) {
      pthread_mutex_lock(&view_mutex);
      key = input->getChar();
      pthread_mutex_unlock(&view_mutex);
      if (key != ERR) {
        handleInput(key);
        updateView();
      }
    }
    if (ping_period >= 3000000/(1 + (int) config["delay"])) ping_period = 0; else ++ping_period;
    usleep((unsigned int) config["delay"]);
  }
  saveConfig();
  stopUI();
  return 0;
}

// The connection thread cannot be a static method,
// so it is executed through this static method
void* AdvClient::callThread(void *This)
{
  int oldstate;
  pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS, &oldstate);
  // Read "This" as a pointer to a AdvClient
  AdvClient *me = static_cast<AdvClient *>(This);
  me->runThread(); // Run the real connection thread
  pthread_exit(nullptr);
}

// Maintain a connection to the current server whenever possible
// and notify when a message is received from the server
void AdvClient::runThread()
{
  sockaddr_in client;
  char c;
  int port, ret;
  X509 *ssl_crt;
  X509_NAME* ssl_crt_name;
  static const char hexbytes[] = "0123456789ABCDEF";
  unsigned int md_size;
  unsigned char md[EVP_MAX_MD_SIZE];
  time_t handshake_begin;
  std::string event, str, active_server;
  unsigned int tries = 0;
  bool connection_error;
  SSL_load_error_strings();
  SSL_library_init();
  OpenSSL_add_all_algorithms();
  ssl_ctx = SSL_CTX_new(SSLv23_client_method());
  SSL_CTX_set_mode(ssl_ctx, SSL_MODE_AUTO_RETRY);
  SSL_CTX_set_verify(ssl_ctx, SSL_VERIFY_NONE, NULL);
  while (!do_exit) {
    // Check if there's a new server defined
    while ( !hasParameter(&current_server, "name") || !hasParameter(&current_server, "hostname") )
      usleep((unsigned int) config["delay"]);
    if (!new_login) continue;
    memset(&client, 0, sizeof(client));
    client.sin_family = AF_INET;
    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    pthread_mutex_lock(&current_server_mutex);
    // Get current server parameters
    active_server = current_server["hostname"];
    if (!hasParameter(&current_server, "port")) current_server["port"] = DEFAULT_PORT;
    port = current_server["port"];
    pthread_mutex_unlock(&current_server_mutex);
    client.sin_addr = getIPAddress(active_server);
    client.sin_port = htons(port);
    ssl = SSL_new(ssl_ctx);
    SSL_set_fd(ssl, client_fd);
    SSL_set_connect_state(ssl);
    str = replaceAllInstances(advGetText("connecting_to_server_port"), "${SERVER}", active_server);
    str = replaceAllInstances(str, "${PORT}", std::to_string(port));
    printText("server_info", AdvClient::getTime() + " " + str);
    updateView();
    connection_error = false;
    if (connect(client_fd, (struct sockaddr*)&client, sizeof(client)) < 0) connection_error = true;
    if (!connection_error) {
      handshake_begin = time(0);
      while (true) {
        if (time(0) - handshake_begin > 10) {
          printText("server_error", replaceAllInstances(advGetText("ssl_error"), "${ERROR}", "SSL_ERROR_TIMEOUT"));
          connection_error = true;
          break;
        }
        usleep((unsigned int) config["delay"]);
        ERR_clear_error();
        ret = SSL_connect(ssl);
        if (ret == 1)
          break;
        else {
          ret = SSL_get_error(ssl, ret);
          if (ret == SSL_ERROR_WANT_READ || ret == SSL_ERROR_WANT_WRITE)
            continue;
          else {
            str = "SSL_ERROR_UNKNOWN";
            switch (ret) {
              case SSL_ERROR_NONE: str = "SSL_ERROR_NONE"; break;
              case SSL_ERROR_ZERO_RETURN: str = "SSL_ERROR_ZERO_RETURN"; break;
              case SSL_ERROR_WANT_CONNECT: str = "SSL_ERROR_WANT_CONNECT"; break;
              case SSL_ERROR_WANT_ACCEPT: str = "SSL_ERROR_WANT_ACCEPT"; break;
              case SSL_ERROR_WANT_X509_LOOKUP: str = "SSL_ERROR_WANT_X509_LOOKUP"; break;
              case SSL_ERROR_WANT_ASYNC: str = "SSL_ERROR_WANT_ASYNC"; break;
              case SSL_ERROR_WANT_ASYNC_JOB: str = "SSL_ERROR_WANT_ASYNC_JOB"; break;
              case SSL_ERROR_WANT_CLIENT_HELLO_CB: str = "SSL_ERROR_WANT_CLIENT_HELLO_CB"; break;
              case SSL_ERROR_SYSCALL: str = "SSL_ERROR_SYSCALL"; break;
              case SSL_ERROR_SSL: str = "SSL_ERROR_SSL"; break;
            }
            printText("server_error", replaceAllInstances(advGetText("ssl_error"), "${ERROR}", str));
            connection_error = true;
            break;
          }
        }
      }
    }
    if (connection_error) {
      printText("server_error", AdvClient::getTime() + " " +
        replaceAllInstances(advGetText("connection_to_server_failed"), "${SERVER}", active_server));
      updateView();
      close(client_fd);
      ++tries;
      new_login = false;
      if (tries >= config["connection-tries"]) {
        str = replaceAllInstances(advGetText("tried_to_connect_n_times_resign"), "${SERVER}", active_server);
        str = AdvClient::getTime() + " " + replaceAllInstances(str, "${TRIES}", std::to_string(tries));
        printText("server_error", str);
        updateView();
        new_login = false;
        tries = 0;
      }
      else {
        for (int t = 0; t < (1000000*(unsigned int) config["connection-period"]) / (unsigned int) config["delay"]; ++t) {
          if (new_login) break;
          usleep((unsigned int) config["delay"]);
        }
        new_login = true;
      }
      SSL_free(ssl);
      continue;
    }
    connected = true;
    printText("server_info", AdvClient::getTime() + " " + replaceAllInstances(advGetText("connected_to_server"),
      "${SERVER}", current_server["hostname"]));
    printText("server_info", replaceAllInstances(advGetText("connected_with_encryption"),
      "${ENCRYPTION}", SSL_get_cipher(ssl)));
    ssl_crt = SSL_get_peer_certificate(ssl);
    X509_digest(ssl_crt, EVP_get_digestbyname("sha1"), md, &md_size);
    X509_free(ssl_crt);
    str.clear();
    for(int i = 0; i < md_size; ++i) {
      str += hexbytes[(md[i]&0xf0)>>4];
      str += hexbytes[(md[i]&0x0f)>>0];
      str += ":";
    }
    str.pop_back();
    printText("server_info", advGetText("server_sha1_fingerprint"));
    printText("server_info", str);
    updateView();
    tries = 0;
    if (hasParameter(&current_server, "username")) {
      nlohmann::json message;
      if (hasParameter(&current_server, "register"))
        message["class"] = "register";
      else
        message["class"] = "login";
      if (hasParameter(&current_server, "username"))
        message["username"] = current_server["username"];
      else
        message["username"] = "";
      if (hasParameter(&current_server, "password")) message["password"] = current_server["password"];
      pthread_mutex_unlock(&current_server_mutex);
      sendRequest(message.dump());
    }
    while ((ret = SSL_read(ssl, &c, 1)) >= 0) {
      if ( !connected || !hasParameter(&current_server, "name") || (active_server != current_server["hostname"]) )
        break;
      if (ret == 1) {
        if (c == '\0') {
          if (event.length() > 0) {
            eventReceived(event);
            event = "";
          }
        }
        else event = event + c;
      }
    }
    connected = false;
    input->prompt = L"> ";
    close(client_fd);
    SSL_free(ssl);
    pthread_mutex_lock(&current_server_mutex);
    printText("server_info", AdvClient::getTime() + " " + replaceAllInstances(advGetText("disconnected_from_server"),
      "${SERVER}", active_server));
    pthread_mutex_unlock(&current_server_mutex);
    updateView();
  }
  SSL_CTX_free(ssl_ctx);
}

// Send request to server
ssize_t AdvClient::sendRequest(std::string request)
{
  request += "\0";
  return SSL_write(ssl, request.c_str(), request.length() + 1);
}

// Support IPv4 only for the time being
in_addr AdvClient::getIPAddress(std::string hostname)
{
  struct addrinfo hints, *response;
  char addrstr[100];
  in_addr ret;
  memset(&ret, 0, sizeof(ret));
  ret.s_addr = INADDR_ANY;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;
  if ( !getaddrinfo(hostname.c_str(), NULL, &hints, &response) && response ) {
    inet_ntop(response->ai_family, response->ai_addr->sa_data, addrstr, 100);
    switch (response->ai_family) {
      case AF_INET:
        ret = ((struct sockaddr_in *) response->ai_addr)->sin_addr;
        break;
    }
  }
  return ret;
}

void AdvClient::loadSupportedLanguages()
{
  DIR *dir;
  struct dirent *entry;
  std::ifstream strings_file;
  nlohmann::json strings_json;
  std::string l;
  if ((dir = opendir(std::string(std::string(PREFIX) + "/share/adv/strings/commands").c_str())) != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
      l = entry->d_name;
      if (l.length() > 5 && l.substr(l.length() - 5) == ".json")
        langs[l.substr(0, l.length() - 5)] = nlohmann::json::parse("{}");
    }
    closedir(dir);
  }
  for (auto& x: nlohmann::json::iterator_wrapper(langs)) {
    strings_file.open(std::string(PREFIX) + "/share/adv/strings/client/" + x.key() + ".json");
    if (strings_file.good()) {
      strings_json.clear();
      try { strings_file >> strings_json; } catch (...) {}
      strings_file.close();
    }
    for (auto& y: nlohmann::json::iterator_wrapper(langs))
      if (hasParameter(&strings_json, std::string("lang_") + y.key()))
        langs[y.key()][x.key()] = strings_json[std::string("lang_") + y.key()];
      else
        langs[y.key()][x.key()] = y.key();
  }
}

void AdvClient::loadStrings()
{
  std::ifstream strings_file;
  strings_file.open(std::string(PREFIX) + "/share/adv/strings/client/" + config["lang"].get<std::string>() + ".json");
  if (strings_file.good()) {
    try { strings_file >> strings; }
    catch (...) {}
    strings_file.close();
  }
}

void AdvClient::loadCommands()
{
  std::ifstream commands_file;
  commands_file.open(std::string(PREFIX) + "/share/adv/strings/commands/" + config["lang"].get<std::string>() + ".json");
  if (commands_file.good()) {
    try {
      commands_file >> commands;
    }
    catch (...) {}
    commands_file.close();
  }
}

void AdvClient::loadConfig()
{
  char* home_directory = getenv("HOME");
  if (home_directory != NULL) {
    std::ifstream config_file;
    config_file.open(std::string(std::string(home_directory) + "/.advrc").c_str());
    if (config_file.good()) {
      try {
        config_file >> config;
      }
      catch (...) {}
      config_file.close();
    }
  }
  if ( hasParameter(&config, "default-server") && !hasParameter(&current_server, "name") )
    for (int i = 0; i < config["servers"].size(); ++i)
      if (config["servers"][i]["name"] == config["default-server"]) {
        current_server = config["servers"][i];
        break;
      }
  if (!checkString(&config, "lang") || !hasParameter(&langs, config["lang"].get<std::string>())) config["lang"] = "en";
  if (!checkInteger(&config, "delay")) config["delay"] = 50000;
  if (!checkInteger(&config, "buffer-size")) config["buffer-size"] = 1000;
  if (!checkInteger(&config, "history-size")) config["history-size"] = 500;
  if (!checkInteger(&config, "connection-period")) config["connection-period"] = 10;
  if (!checkInteger(&config, "connection-tries")) config["connection-tries"] = 10;
  if (!checkInteger(&config, "fast-scroll-percentage")) config["fast-scroll-percentage"] = 90;
  if (!checkInteger(&config, "slow-scroll-lines")) config["slow-scroll-lines"] = 1;
  if (!hasParameter(&config, "aliases") ||
    config["aliases"].type() != nlohmann::json::value_t::array) config["aliases"] = nlohmann::json::parse("[]");
  if (!hasParameter(&config, "bindings") ||
    config["bindings"].type() != nlohmann::json::value_t::array) config["bindings"] = nlohmann::json::parse("[]");
  for (int i = 0; i < config["bindings"].size(); ++i)
    if (!isBoundable(config["bindings"][i]["key"].get<std::string>())) {
      config["bindings"].erase(i);
      --i;
    }
  if (!checkString(&config, "welcome-message")) config["welcome-message"] = "yes";
  if (!checkString(&config, "store-buffer")) config["store-buffer"] = "yes";
  if (!checkString(&config, "editor") || config["editor"].get<std::string>().empty()) {
    char* editor = getenv("EDITOR");
    if (editor != NULL) config["editor"] = std::string(editor); else config["editor"] = "";
  }
}

void AdvClient::saveConfig()
{
  char* home_directory = getenv("HOME");
  if (home_directory == NULL) return;
  std::ofstream config_file;
  config_file.open(std::string(std::string(home_directory) + "/.advrc").c_str());
  if (config_file.good()) {
    config["history"] = nlohmann::json::parse("[]");
    config["buffer"] = nlohmann::json::parse("[]");
    for (int i = 0; i < input->history.size(); ++i)
      config["history"].push_back(boost::locale::conv::utf_to_utf<char>(
        input->history.at(i).c_str(), input->history.at(i).c_str() + input->history.at(i).size()));
    std::list<std::string> buffer = outputWindow->getContent();
    if (config["store-buffer"] == "yes")
      for (std::list<std::string>::iterator it = buffer.begin(); it != buffer.end(); ++it)
        config["buffer"].push_back(*it);
    config_file << std::setw(2) << config << std::endl;
    config_file.close();
  }
}

void AdvClient::startUI()
{
  pthread_mutex_lock(&view_mutex);
  initscr();
  if ( (LINES < 8) || (COLS < 24) ) {
    endwin();
    std::cerr << std::endl << advGetText("terminal_too_small_quitting") << std::endl;
    do_exit = true;
    pthread_mutex_unlock(&view_mutex);
    return;
  }
  raw();
  cbreak();
  noecho();
  timeout(0);
  start_color();
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_YELLOW, COLOR_BLACK);
  init_pair(4, COLOR_BLUE, COLOR_BLACK);
  init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(6, COLOR_CYAN, COLOR_BLACK);
  init_pair(7, COLOR_WHITE, COLOR_BLACK);
  keypad(stdscr, TRUE);
  move(LINES-2, 0);
  hline(0, COLS);
  refresh();
  input->setCols(COLS-1);
  inputWindow = newwin(1, COLS-1, LINES-1, 0);
  outputWindow->refreshSize(LINES-2, COLS-1, 0, 0);
  ui = true;
  pthread_mutex_unlock(&view_mutex);
  updateView();
}

void AdvClient::stopUI()
{
  pthread_mutex_lock(&view_mutex);
  ui = false;
  endwin();
  pthread_mutex_unlock(&view_mutex);
}

void AdvClient::updateView()
{
  if (!ui) return;
  pthread_mutex_lock(&view_mutex);
  outputWindow->showWin();
  move(LINES-3, COLS-1);
  if (outputWindow->getScroll() > 0) {
    if (new_lines_on_view) wattron(stdscr, COLOR_PAIR(1));
    waddnwstr(stdscr, L"🠳", -1);
    wattrset(stdscr, 0);
  }
  else {
    waddnwstr(stdscr, L" ", -1);
    new_lines_on_view = false;
  }
  move(0, COLS-1);
  if (outputWindow->getScroll() < outputWindow->getMaxScroll())
    waddnwstr(stdscr, L"🠱", -1);
  else
    waddnwstr(stdscr, L" ", -1);
  wrefresh(stdscr);
  wmove(inputWindow, 0, 0);
  wclrtoeol(inputWindow);
  if (!input->insert) wattron(inputWindow, A_BOLD);
  if (play_mode) wattroff(inputWindow, COLOR_PAIR(6)); else wattron(inputWindow, COLOR_PAIR(6));
  wmove(inputWindow, 0, 0);
  waddnwstr(inputWindow, input->prompt.c_str(), -1);
  wattroff(inputWindow, A_BOLD);
  if (tab_mode) wattron(inputWindow, COLOR_PAIR(2));
  waddnwstr(inputWindow, input->cmdline.substr(input->offset, input->cmdline.length()-input->offset).c_str(), -1);
  wmove(inputWindow, 0, input->prompt.length() + input->cursor - 1);
  wrefresh(inputWindow);
  pthread_mutex_unlock(&view_mutex);
}

void AdvClient::eventReceived(std::string cmd)
{
  nlohmann::json message;
  try {
    message = nlohmann::json::parse(cmd);
  }
  catch (...) {
    printText("unknown", cmd);
    updateView();
    return;
  }
  if (!checkString(&message, "class")) {
    printText("unknown", cmd);
  }
  else if (message["class"] == "register") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("registered_as_user"),
        "${USER}", message["username"]));
    else {
      printText("server_error", replaceAllInstances(advGetText("registration_failed"),
        "${REASON}", advGetError(message["reason"])));
      pthread_mutex_lock(&current_server_mutex);
      current_server.clear();
      pthread_mutex_unlock(&current_server_mutex);
    }
  }
  else if (message["class"] == "login") {
    if (message["response"] == "success") {
      printText("server_info", replaceAllInstances(advGetText("logged_in_as_user"),
        "${USER}", message["username"]));
    }
    else {
      if (message["reason"] == "banned") {
        printText("error", advGetError("banned"));
      }
      else if (message["reason"] == "kicked") {
        printText("error", advGetError("banned"));
        printText("error", replaceAllInstances(advGetText("please_wait_and_try_again"),
          "${WAIT}", getElapsedTime((unsigned long) message["wait"])));
      }
      else {
        printText("server_error", replaceAllInstances(advGetText("login_failed"),
          "${REASON}", advGetError(message["reason"])));
      }
      pthread_mutex_lock(&current_server_mutex);
      current_server.clear();
      pthread_mutex_unlock(&current_server_mutex);
    }
  }
  else if (message["class"] == "password") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("password_changed_successfully"),
        "${USER}", message["username"]));
    else {
      printText("server_error", replaceAllInstances(advGetText("password_change_failed"),
        "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "logout") {
    if (message["reason"] == "banned") {
      printText("error", advGetError("banned"));
    }
    else if (message["reason"] == "kicked") {
      printText("error", advGetError("banned"));
      printText("error", replaceAllInstances(advGetText("please_wait_and_try_again"),
                                                    "${WAIT}", getElapsedTime((unsigned long) message["wait"])));
    }
    printText("server_info", advGetText("logged_out_from_server"));
    performAction("logout", "");
  }
  else if (message["class"] == "create") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("game_created_successfully"),
        "${GAME}", message["name"]));
    else {
      if (message["reason"] == "creation_rate_limit") {
        printText("server_error", advGetError("creation_rate_limit"));
        printText("server_error", replaceAllInstances(advGetText("please_wait_and_try_again"),
          "${WAIT}", getElapsedTime((unsigned long) message["wait"])));
      }
      else
        printText("server_error", replaceAllInstances(advGetText("game_creation_failed"),
          "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "manage") {
    if (message["response"] == "failure")
      printText("server_error",
        replaceAllInstances(advGetText("cannot_manage_game"), "${REASON}", advGetError(message["reason"])));
  }
  else if (message["class"] == "transfer") {
    if (message["response"] == "success") {
      std::string str = replaceAllInstances(advGetText("game_transfered_to_player"),
        "${GAME}", message["name"]);
      str = replaceAllInstances(str, "${NEW_OWNER}", message["new-owner"]);
      printText("server_info", str);
    }
    else {
      printText("server_error", replaceAllInstances(advGetText("game_transference_failed"),
        "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "destroy") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("game_destroyed_successfully"),
        "${GAME}", message["name"]));
    else {
      printText("server_error", replaceAllInstances(advGetText("game_destruction_failed"),
        "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "enter") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("you_entered_game"),
        "${GAME}", message["name"]));
    else {
      printText("server_error", replaceAllInstances(advGetText("failed_entering_game"),
        "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "leave") {
    if (message["response"] == "success")
      printText("server_info", replaceAllInstances(advGetText("you_left_from_game"),
        "${GAME}", message["name"]));
    else {
      printText("server_error", replaceAllInstances(advGetText("failed_leaving_game"),
        "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "games") {
    if (message["games"].size() == 0)
      printText("server_info", advGetText("there_are_currently_no_games"));
    else {
      std::string str;
      printText("server_info", std::string(Window::UNDERLINE + advGetText("games_on_the_server")));
      for (int i = 0; i < message["games"].size(); ++i) {
        str = Window::BOLD + message["games"][i]["title"].get<std::string>() + Window::NOTBOLD + " (" +
          replaceAllInstances(advGetText("referenced_as"), "${REF}", message["games"][i]["name"].get<std::string>()) + ")";
        printText("info", str);
        str = std::string("  ") + advGetText("mods_on_games_list");
        for (int j = 0; j < message["games"][i]["mods"].size(); ++j)
          str += " " + message["games"][i]["mods"][j].get<std::string>();
        str += "; " + advGetText("created_on_games_list") + " " + AdvClient::getDateAndTime(message["games"][i]["created"]);
        printText("server_info", str);
        if (message["games"][i]["description"].size()) for (int j = 0; j < message["games"][i]["description"].size(); ++j)
          printText("server_info", std::string("  ") + message["games"][i]["description"][j].get<std::string>());
      }
    }
    if (!message["game"].get<std::string>().empty())
      printText("server_info", replaceAllInstances(advGetText("you_are_in_game"),
        "${GAME}", message["game"]));
  }
  else if (message["class"] == "mods") {
    std::string str, highlight;
    if (message["mods"].size() == 0) {
      if (hasParameter(&message, "type"))
        printText("server_info", replaceAllInstances(advGetText("there_are_currently_no_mods_with_type"),
          "${TYPE}", message["type"]));
      else
        printText("server_info", advGetText("there_are_currently_no_mods"));
    }
    else {
      if (hasParameter(&message, "type"))
        printText("server_info", replaceAllInstances(advGetText("mods_installed_on_the_server_with_type"),
          "${TYPE}", message["type"]));
        else
          printText("server_info", advGetText("mods_installed_on_the_server"));
      for (int i = 0; i < message["mods"].size(); ++i) {
        if (!hasParameter(&message, "type") && hasParameter(&message["mods"][i], "type") && message["mods"][i]["type"] == "game") {
          highlight = Window::BOLD;
          message["mods"][i]["type"] = Window::BLUE + "game";
        }
        else {
          highlight.clear();
        }
        str = ":: ";
        if (hasParameter(&message["mods"][i], "pretty-name")) {
          str += message["mods"][i]["pretty-name"].get<std::string>();
          str += std::string(" (") + replaceAllInstances(advGetText("referenced_as"),
            "${REF}", message["mods"][i]["name"].get<std::string>()) + ")";
        }
        else
          str += message["mods"][i]["name"].get<std::string>();
        printText("info", highlight + str);
        if (hasParameter(&message["mods"][i], "version"))
          str = message["mods"][i]["version"].get<std::string>();
        else
          str = advGetText("version_unknown");
        str = replaceAllInstances(advGetText("mod_version"), "${VERSION}", str);
        if (!hasParameter(&message, "type") && hasParameter(&message["mods"][i], "type"))
          str += " ; " + replaceAllInstances(advGetText("mod_type"), "${TYPE}", message["mods"][i]["type"].get<std::string>());
        printText("server_info", highlight + "  " + str);
        if (hasParameter(&message["mods"][i], "depends")) {
          str = advGetText("depends_on");
          for (int j = 0; j < message["mods"][i]["depends"].size(); ++j)
            str += std::string(" ") + message["mods"][i]["depends"][j].get<std::string>();
          printText("server_info", highlight + "  " + str);
        }
        if (hasParameter(&message["mods"][i], "description")) {
          for (int j = 0; j < message["mods"][i]["description"].size(); ++j)
            printText("server_info", highlight + "  " + message["mods"][i]["description"][j].get<std::string>());
        }
        if (hasParameter(&message["mods"][i], "url"))
          str = message["mods"][i]["url"].get<std::string>();
        else
          str = advGetText("url_unknown");
        str = replaceAllInstances(advGetText("mod_url"), "${URL}", str);
        printText("server_info", highlight + "  " + str);
      }
    }
  }
  else if (message["class"] == "version") {
    std::string str = replaceAllInstances(advGetText("running_server_version"),
      "${SERVER}", current_server["hostname"]);
    str = replaceAllInstances(str, "${VERSION}", message["version"]);
    printText("server_info", str);
  }
  else if (message["class"] == "info") {
    std::string str = advGetText("server_uptime") + " " + getElapsedTime((unsigned long) message["uptime"]);
    printText("notice", Window::BOLD + Window::ITALIC + str);
    if ((unsigned long) message["connected"] == 0)
      str = advGetText("no_players_connected_right_now");
    else if ((unsigned long) message["connected"] == 1)
      str = advGetText("one_player_connected_right_now");
    else
      str = replaceAllInstances(advGetText("n_players_connected_right_now"), "${AMOUNT}",
                                std::to_string((unsigned long) message["connected"]));
      printText("notice", Window::BOLD + Window::ITALIC + str);
  }
  else if (message["class"] == "chat") {
    std::string str;
    if (message["content"].get<std::string>().substr(0, 4) == "/me ")
      str = Window::ITALIC + AdvClient::getTime() + " " + message["username"].get<std::string>() + " " +
        message["content"].get<std::string>().substr(4);
    else
      str = AdvClient::getTime() + " " + Window::BOLD + message["username"].get<std::string>() +
        Window::NOTBOLD + ": " + message["content"].get<std::string>();
    printText("chat", str);
  }
  else if (message["class"] == "roll") {
    unsigned int ndice, nfaces;
    std::string str = replaceAllInstances(advGetText("player_rolls_dice"), "${PLAYER}", message["username"]);
    std::string values;
    if (hasParameter(&message, "ndice") && hasParameter(&message, "nfaces")) {
      ndice = (unsigned int) message["ndice"];
      nfaces = (unsigned int) message["nfaces"];
      str = replaceAllInstances(str, "${ROLL}", std::to_string(ndice) + "d" + std::to_string(nfaces));
    }
    else
      str = replaceAllInstances(str, "${ROLL}", "?");
    for (int i = 0; i < message["values"].size(); ++i)
      values += std::to_string((unsigned int) message["values"][i]) + " ";
    str = Window::ITALIC + replaceAllInstances(str, "${DICE}", values);
    printText("chat", str);
  }
  else if (message["class"] == "action") {
    if (hasParameter(&message, "response") && (message["response"] == "failure")) {
      if (message["reason"] == "game_paused") {
        printText("server_info", advGetText("cannot_send_anything_to_paused_game"));
        if (checkArray(&message, "pause-message"))
          for (int i = 0; i < message["pause-message"].size(); ++i)
            printText("server_info", message["pause-message"][i].get<std::string>());
      }
      else printText("server_error",
        replaceAllInstances(advGetText("failed_sending_action"), "${REASON}", advGetError(message["reason"])));
    }
  }
  else if (message["class"] == "chat-backlog") {
    std::string str;
    unsigned int ndice, nfaces;
    std::string values;
    for (int i = 0; i < (int) message["size"]; ++i) {
      if (message["content"][i]["class"] == "chat") {
        if (message["content"][i]["content"].get<std::string>().substr(0, 4) == "/me ")
          str = Window::ITALIC + "[" + AdvClient::getDateAndTime(message["content"][i]["timestamp"]) + "] " + message["content"][i]["username"].get<std::string>() + " " +
          message["content"][i]["content"].get<std::string>().substr(4);
        else
          str = std::string("[") + AdvClient::getDateAndTime(message["content"][i]["timestamp"]) + "] " + Window::BOLD + message["content"][i]["username"].get<std::string>() +
          Window::NOTBOLD + ": " + message["content"][i]["content"].get<std::string>();
      }
      else if (message["content"][i]["class"] == "roll") {
        std::string str = replaceAllInstances(advGetText("player_rolls_dice"), "${PLAYER}", message["content"][i]["username"]);
        if (hasParameter(&message["content"][i], "ndice") && hasParameter(&message["content"][i], "nfaces")) {
          ndice = (unsigned int) message["content"][i]["ndice"];
          nfaces = (unsigned int) message["content"][i]["nfaces"];
          str = replaceAllInstances(str, "${ROLL}", std::to_string(ndice) + "d" + std::to_string(nfaces));
        }
        else
          str = replaceAllInstances(str, "${ROLL}", "?");
        values.clear();
        for (int i = 0; i < message["content"][i]["values"].size(); ++i)
          values += std::to_string((unsigned int) message["content"][i]["values"][i]) + " ";
        str = Window::ITALIC + "[" + AdvClient::getDateAndTime(message["content"][i]["timestamp"]) + "] " + replaceAllInstances(str, "${DICE}", values);
      }
      printText("chat", str);
    }
  }
  else if (message["class"] == "message") {
    std::string type, tag;
    if (checkString(&message, "type"))
      type = message["type"].get<std::string>();
    else
      type = "plain";
    if (checkString(&message, "tag"))
      tag = message["tag"].get<std::string>();
    else
      tag = "";
    if (checkString(&message, "message"))
      printText(type, message["message"].get<std::string>(), tag);
    else
      for (int i = 0; i < message["message"].size(); ++i)
        printText(type, message["message"][i].get<std::string>());
  }
  else if (message["class"] == "write") {
    message["class"] = "action";
    if (!hasParameter(&message, "text")) message["text"] = nlohmann::json::parse("[]");
    message["text"] = AdvClient::getTextFromEditor(message["text"]);
    sendRequest(message.dump());
  }
  else if (message["class"] == "admin") {
    if (message["response"] == "success") {
      if (message["event"] == "set") {
        std::string str = replaceAllInstances(advGetText("server_option_set"), "${OPTION}", message["option"]);
        if (checkString(&message, "value")) {
          if (message["option"] == "log-level")
            message["value"] = advGetText(std::string("log_level_") + message["value"].get<std::string>());
          else if (message["option"] == "lang")
            message["value"] = advGetText(std::string("lang_") + message["value"].get<std::string>());
          str = replaceAllInstances(str, "${VALUE}", message["value"].get<std::string>());
        }
        else if (checkNumber(&message, "value"))
          str = replaceAllInstances(str, "${VALUE}", std::to_string((int) message["value"]));
        else if (checkArray(&message, "value")) {
          if (message["option"] == "motd" && asked_for_motd) {
            asked_for_motd = false;
            nlohmann::json j;
            j["class"] = "admin";
            j["command"] = "set";
            j["option"] = "motd";
            j["value"] = AdvClient::getTextFromEditor(message["value"]);
            sendRequest(j.dump());
            return;
          }
          else {
            str = replaceAllInstances(str, "${VALUE}", "");
            printText("notice", str);
            for (int i = 0; i < message["value"].size(); ++i)
              printText("plain", Window::BOLD + Window::ITALIC + message["value"][i].get<std::string>());
            str.clear();
          }
        }
        if (!str.empty()) printText("notice", str);
      }
      else if (message["event"] == "yes_sir") {
        printText("notice", advGetText("yes_sir"));
      }
      else if (message["event"] == "you_are_the_master") {
        unsigned int line = 1;
        pickRandomString("you_are_the_master_1");
        while (advGetText("you_are_the_master_" + std::to_string(line)) !=
          std::string("you_are_the_master_" + std::to_string(line))) {
          printText("notice", advGetText("you_are_the_master_" + std::to_string(line)));
          ++line;
        }
        releaseRandomString();
      }
      else if (message["event"] == "assign-role") {
        std::string str;
        if (!hasParameter(&message, "username")) {
          unsigned int line = 1;
          pickRandomString("assigned_role_1");
          while (advGetText("assigned_role_" + std::to_string(line)) !=
            std::string("assigned_role_" + std::to_string(line))) {
            printText("notice", replaceAllInstances(advGetText("assigned_role_" + std::to_string(line)), "${ROLE}", message["role"]));
            ++line;
          }
          releaseRandomString();
        }
        else {
          str = replaceAllInstances(advGetText("role_assigned"), "${ROLE}", message["role"]);
          printText("notice", replaceAllInstances(str, "${USERNAME}", message["username"]));
        }
      }
      else if (message["event"] == "revoke-role") {
        std::string str = replaceAllInstances(advGetText("role_revoked"), "${ROLE}", message["role"]);
        printText("notice", replaceAllInstances(str, "${USERNAME}", message["username"]));
      }
      else if (message["event"] == "enable-registration") {
        printText("notice", advGetText("registration_is_enabled"));
      }
      else if (message["event"] == "disable-registration") {
        printText("notice", advGetText("registration_is_disabled"));
      }
      else if (message["event"] == "enable-unregistered") {
        printText("notice", advGetText("unregistered_is_enabled"));
      }
      else if (message["event"] == "disable-unregistered") {
        printText("notice", advGetText("unregistered_is_disabled"));
      }
      else if (message["event"] == "send") {
        std::string str = replaceAllInstances(advGetText("message_sent_to_player"), "${USERNAME}", message["username"]);
        printText("notice", replaceAllInstances(str, "${MESSAGE}", message["message"]));
      }
      else if (message["event"] == "broadcast") {
        printText("notice", replaceAllInstances(advGetText("message_broadcasted"), "${MESSAGE}", message["message"]));
      }
      else if (message["event"] == "players") {
        printText("notice", advGetText("connected_players"));
        for (unsigned int i = 0; i < message["players"].size(); ++i)
          printText("notice", std::string("- ") + message["players"][i].get<std::string>());
      }
      else if (message["event"] == "registered") {
        printText("notice", advGetText("registered_players"));
        std::string str;
        int i, j;
        for (unsigned int i = 0; i < message["registered"].size(); ++i) {
          str = std::string("- ") + message["registered"][i]["username"].get<std::string>();
          if (message["registered"][i]["roles"].size() > 0) {
            str += " (";
            for (j = 0; j < message["registered"][j]["roles"].size() - 1; ++j)
              str += message["registered"][i]["roles"][j].get<std::string>() + ", ";
            str += message["registered"][i]["roles"][j].get<std::string>() + ")";
          }
          if (hasParameter(&message["registered"][i], "banned")) {
            if ((time_t) message["registered"][i]["banned"] == 0)
              str += " (" + advGetText("banned") + ")";
            else
              str += " (" + replaceAllInstances(advGetText("kicked"), "${KICK_TIME}", getElapsedTime((unsigned long) (message["registered"][i]["banned"]) - time(0))) + ")";
          }
          printText("notice", str);
        }
      }
      else if (message["event"] == "sync") {
        printText("notice", advGetText("scheduled_working_directory_sync"));
      }
      else if (message["event"] == "just_synced") {
        printText("notice", advGetText("working_directory_synced"));
      }
      else if (message["event"] == "create") {
        std::string str = replaceAllInstances(advGetText("user_created_new_game"), "${USERNAME}", message["username"]);
        printText("notice", replaceAllInstances(str, "${NAME}", message["name"]));
      }
      else if (message["event"] == "login") {
        printText("notice", replaceAllInstances(advGetText("user_logged_in"), "${USERNAME}", message["username"]));
      }
      else if (message["event"] == "logout") {
        printText("notice", replaceAllInstances(advGetText("user_logged_out"), "${USERNAME}", message["username"]));
      }
      else if (message["event"] == "kick") {
        std::string str = replaceAllInstances(advGetText("user_kicked"), "${USERNAME}", message["username"]);
        printText("notice", replaceAllInstances(str, "${KICK_TIME}", getElapsedTime(message["seconds"])));
      }
      else if (message["event"] == "ban") {
        printText("notice", replaceAllInstances(advGetText("user_banned"), "${USERNAME}", message["username"]));
      }
    }
    else {
      printText("server_error", advGetText(message["reason"]));
    }
  }
  else if (message["class"] == "error") {
    printText("server_error",
      replaceAllInstances(advGetText("server_error"), "${ERROR}", message["error"]));
  }
  else printText("unknown", cmd);
  if ((message["class"] == "login" || message["class"] == "register") && message["response"] == "success") {
    std::string str = message["username"].get<std::string>() + "@" +
    current_server["hostname"].get<std::string>() + "> ";
    input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
    str = replaceAllInstances(advGetText("running_server_version"),
      "${SERVER}", current_server["hostname"]);
    str = replaceAllInstances(str, "${VERSION}", message["version"]);
    printText("notice", Window::BOLD + Window::ITALIC + str);
    str = advGetText("server_uptime") + " " + getElapsedTime((unsigned long) message["uptime"]);
    printText("notice", Window::BOLD + Window::ITALIC + str);
    if ((unsigned long) message["connected"] == 0)
      str = advGetText("no_players_connected_right_now");
    else if ((unsigned long) message["connected"] == 1)
      str = advGetText("one_player_connected_right_now");
    else
      str = replaceAllInstances(advGetText("n_players_connected_right_now"), "${AMOUNT}",
                                std::to_string((unsigned long) message["connected"]));
      printText("notice", Window::BOLD + Window::ITALIC + str);
    if (message["motd"].size() > 0) for (int i = 0; i < message["motd"].size(); ++i)
      printText("plain", Window::BOLD + Window::ITALIC + message["motd"][i].get<std::string>());
    if (checkString(&config, "default-game") && !config["default-game"].get<std::string>().empty()) {
      message.clear();
      message["class"] = "enter";
      message["name"] = config["default-game"].get<std::string>();
      message["lang"] = config["lang"].get<std::string>();
      sendRequest(message.dump());
    }
  }
  updateView();
}

void AdvClient::handleInput(wchar_t key)
{
  std::string str;
  switch (key) {
    case 9: // Tab
      break;
    default:
      tab_mode = false;
      input->last_completion = 0;
      input->completion = input->cmdline;
      break;
  }
  switch (key) {
    case KEY_RESIZE:
      windowResized(SIGWINCH);
      break;
    case KEY_PPAGE:
      if (outputWindow->getLines() > 10)
        outputWindow->scrollUp(1 + outputWindow->getLines()* (double) config["fast-scroll-percentage"]/100);
      else
        outputWindow->scrollUp((int) config["slow-scroll-lines"]);
      break;
    case CTRL('b'):
      outputWindow->scrollUp((int) config["slow-scroll-lines"]);
      break;
    case KEY_NPAGE:
        if (outputWindow->getLines() > 10)
          outputWindow->scrollUp(-1 - outputWindow->getLines()* (double) config["fast-scroll-percentage"]/100);
        else
          outputWindow->scrollUp(- (int) config["slow-scroll-lines"]);
      break;
    case CTRL('n'):
      outputWindow->scrollUp(- (int) config["slow-scroll-lines"]);
      break;
    case CTRL('l'):
      outputWindow->clear();
      break;
    case CTRL('e'):
      outputWindow->scrollUp(-outputWindow->getScroll());
      break;
    case 9: // Tab
      if ( play_mode && !input->cmdline.empty() && (input->cmdline.length() < (input->cursor + input->offset)) )
        tab_mode = input->handleCompletion();
      break;
    case '\n':
      while (!input->cmdline.empty() && std::isspace(input->cmdline.back())) input->cmdline.pop_back();
      if (input->cmdline.length() > 0) {
        str = boost::locale::conv::utf_to_utf<char>(
          input->cmdline.c_str(), input->cmdline.c_str() + input->cmdline.size());
        parseCommand(str);
        if (play_mode) {
          if ( (input->history.size() == 0) || (input->history.at(input->history.size()-1) != input->cmdline) )
            input->history.push_back(input->cmdline);
          for (int i = 0; i < input->history.size() - 1; ++i)
            if (input->history[i] == input->cmdline) {
              input->history.erase(input->history.begin() + i);
              break;
            }
          while (input->history.size() > config["history-size"]) input->history.erase(input->history.begin());
        }
        input->cmdline.clear();
        input->cursor = 1;
        input->offset = 0;
        input->history_position = 0;
      }
      else {
        input->cursor = 1;
        input->offset = 0;
        input->history_position = 0;
      }
      break;
    default: handleBoundKey(key);
  }
}

void AdvClient::handleBoundKey(wchar_t key)
{
  std::string key_name;
  switch (key) {
    case KEY_F(1):
      play_mode = !play_mode;
      break;
    case KEY_F(2): key_name = "F2"; break;
    case KEY_F(3): key_name = "F3"; break;
    case KEY_F(4): key_name = "F4"; break;
    case KEY_F(5): key_name = "F5"; break;
    case KEY_F(6): key_name = "F6"; break;
    case KEY_F(7): key_name = "F7"; break;
    case KEY_F(8): key_name = "F8"; break;
    case KEY_F(9): key_name = "F9"; break;
    case KEY_F(10): key_name = "F10"; break;
    case KEY_F(11): key_name = "F11"; break;
    case KEY_F(12): key_name = "F12"; break;
    case CTRL('q'): key_name = "CTRL-Q"; break;
    case CTRL('w'): key_name = "CTRL-W"; break;
    case CTRL('r'): key_name = "CTRL-R"; break;
    case CTRL('t'): key_name = "CTRL-T"; break;
    case CTRL('y'): key_name = "CTRL-Y"; break;
    case CTRL('u'): key_name = "CTRL-U"; break;
    case CTRL('o'): key_name = "CTRL-O"; break;
    case CTRL('p'): key_name = "CTRL-P"; break;
    case CTRL('a'): key_name = "CTRL-A"; break;
    case CTRL('s'): key_name = "CTRL-S"; break;
    case CTRL('d'): key_name = "CTRL-D"; break;
    case CTRL('f'): key_name = "CTRL-F"; break;
    case CTRL('g'): key_name = "CTRL-G"; break;
    case CTRL('k'): key_name = "CTRL-K"; break;
    case CTRL('x'): key_name = "CTRL-X"; break;
    case CTRL('v'): key_name = "CTRL-V"; break;
    case CTRL(' '): key_name = "CTRL-SPACE"; break;
  }
  if (!key_name.empty())
    for (int i = 0; i < config["bindings"].size(); ++i)
      if (key_name == config["bindings"][i]["key"].get<std::string>()) {
        parseCommand(config["bindings"][i]["command"].get<std::string>());
        break;
      }
}

bool AdvClient::isBoundable(std::string key_name)
{
  if ( (key_name == "F2") || (key_name == "F3") || (key_name == "F4") ||
    (key_name == "F5") || (key_name == "F6") || (key_name == "F7") || (key_name == "F8") ||
    (key_name == "F9") || (key_name == "F10") || (key_name == "F11") || (key_name == "F12") ||
    (key_name == "CTRL-Q") || (key_name == "CTRL-W") || (key_name == "CTRL-R") || (key_name == "CTRL-T") ||
    (key_name == "CTRL-Y") || (key_name == "CTRL-U") || (key_name == "CTRL-O") || (key_name == "CTRL-P") ||
    (key_name == "CTRL-A") || (key_name == "CTRL-S") || (key_name == "CTRL-D") || (key_name == "CTRL-F") ||
    (key_name == "CTRL-G") || (key_name == "CTRL-K") || (key_name == "CTRL-X") || (key_name == "CTRL-V") ||
    (key_name == "CTRL-SPACE") ) return true;
  return false;
}

void AdvClient::parseCommand(std::string cmd)
{
  std::string command;
  std::string arguments = cmd;
  nextArg(arguments, command);
  bool action_performed = false;
  for (int i = 0; i < config["aliases"].size(); ++i)
    if (command == config["aliases"][i]["alias"].get<std::string>()) {
      command = config["aliases"][i]["replacement"];
      std::string str;
      nextArg(command, str);
      arguments = command + arguments;
      command = str;
      cmd = command + " " + arguments;
      break;
    }
  while (!cmd.empty() && std::isspace(cmd.back())) cmd.pop_back();
  for (int i = 0; i < commands.size(); ++i) if (commands[i]["class"].get<std::string>() == "command")
    for (int j = 0; j < commands[i]["forms"].size(); ++j) {
      if (command == commands[i]["forms"][j]) {
        performAction(commands[i]["action"], arguments);
        action_performed = true;
        break;
      }
    }
  if (!action_performed) {
    if (connected) {
      nlohmann::json message;
      if (play_mode) {
        std::string arg;
        message["class"] = "action";
        message["action"] = nlohmann::json::parse("[]");
        while (!cmd.empty()) {
          nextArg(cmd, arg);
          message["action"].push_back(arg);
        }
      }
      else {
        message["class"] = "chat";
        message["content"] = cmd;
      }
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
}

nlohmann::json AdvClient::getTextFromEditor(nlohmann::json ret)
{
  if (ret.type() != nlohmann::json::value_t::array || ret.size() == 0 || ret[0].type() != nlohmann::json::value_t::string)
    ret = nlohmann::json::parse("[]");
  if (config["editor"].get<std::string>().empty()) return ret;
  pthread_mutex_lock(&editor_mutex);
  if (ui) stopUI();
  std::string temp_file = "/tmp/adv-editor";
  std::fstream file;
  std::string line;
  if (ret.size()) {
    file.open(temp_file.c_str(), std::ios::app);
    if (!file.good()) {
      startUI();
      pthread_mutex_unlock(&editor_mutex);
      return ret;
    }
    for (unsigned long i = 0; i < ret.size(); ++i) file << ret[i].get<std::string>() << std::endl;
    file.close();
  }
  system(std::string(config["editor"].get<std::string>() + " " + temp_file).c_str());
  file.open(temp_file.c_str());
  if (!file.good()) {
    startUI();
    pthread_mutex_unlock(&editor_mutex);
    return ret;
  }
  ret = nlohmann::json::parse("[]");
  while (std::getline(file, line)) ret.push_back(line);
  file.close();
  unlink(temp_file.c_str());
  startUI();
  pthread_mutex_unlock(&editor_mutex);
  return ret;
}

void AdvClient::printText(std::string type, std::string text, std::string tag)
{
  if (type == "admin")
    text = Window::RED + Window::BOLD + replaceAllInstances(advGetText("message_from_admin"), "${MESSAGE}", text);
  else if (type == "server_info") text = Window::ITALIC + Window::MAGENTA + text;
  else if (type == "server_error") text = Window::ITALIC + Window::MAGENTA + Window::BOLD + text;
  else if (type == "chat") text = Window::CYAN + text;
  else if (type == "help") text = Window::MAGENTA + text;
  else if (type == "info") text = Window::BLUE + text;
  else if (type == "notice") text = Window::GREEN + text;
  else if (type == "warning") text = Window::YELLOW + text;
  else if (type == "error") text = Window::RED + text;
  text += Window::RESET;
  pthread_mutex_lock(&view_mutex);
  outputWindow->addLine(text, tag);
  new_lines_on_view = true;
  pthread_mutex_unlock(&view_mutex);
}

void AdvClient::showDetailedHelp(std::string arg)
{
  bool found = false;
  bool found_form;
  std::string name, line;
  int form;
  for (int i = 0; i < commands.size(); ++i) {
    found_form = false;
    if (commands[i]["class"].get<std::string>() == "command") {
      line = Window::UNDERLINE + advGetText("forms_in_detailed_help") + Window::NOTUNDERLINE + " ";
      for (form = 0; form < commands[i]["forms"].size(); ++form) {
        line += commands[i]["forms"][form].get<std::string>() + std::string(" ");
        if (!found_form) {
          name = commands[i]["forms"][form].get<std::string>();
          if ( (name != arg) && (name[0] == '/') ) name.erase(0, 1);
          if (name == arg) found_form = true;
        }
      }
      if (found_form) {
        found = true;
        printText("help", "");
        printText("help", std::string("  ") + commands[i]["forms"][form-1].get<std::string>());
        printText("help", line);
        line = Window::UNDERLINE + advGetText("usage_in_detailed_help") + Window::NOTUNDERLINE + " ";
        printText("help", line + commands[i]["forms"][form-1].get<std::string>() +
          std::string(" ") + commands[i]["usage"].get<std::string>());
        for (int j = 0; j < commands[i]["description"].size(); ++j)
          printText("help", std::string("  ") + commands[i]["description"][j].get<std::string>());
        if (hasParameter(&commands[i], "see-also") && commands[i]["see-also"].size() > 0) {
          line = Window::UNDERLINE + advGetText("see_also_in_detailed_help") + Window::NOTUNDERLINE + " ";
          for (int j = 0; j < commands[i]["see-also"].size(); ++j)
            line += commands[i]["see-also"][j].get<std::string>() + std::string(" ");
          printText("help", line);
        }
      }
    }
    else if (commands[i]["class"].get<std::string>() == "topic") {
      name = commands[i]["name"].get<std::string>();
      if (name != arg) continue;
      found = true;
      printText("help", "");
      printText("help", std::string("  ") + Window::UNDERLINE + commands[i]["title"].get<std::string>());
      for (int j = 0; j < commands[i]["description"].size(); ++j)
        printText("help", commands[i]["description"][j].get<std::string>());
      if (hasParameter(&commands[i], "see-also") && commands[i]["see-also"].size() > 0) {
        printText("help", std::string("  ") + Window::UNDERLINE + advGetText("related_commands_in_detailed_help"));
        for (int j = 0; j < commands[i]["see-also"].size(); ++j) {
          for (int k = 0; k < commands.size(); ++k) {
            form = commands[k]["forms"].size();
            if (form > 0) {
              name = commands[k]["forms"][form-1].get<std::string>();
              if (name == commands[i]["see-also"][j].get<std::string>()) {
                printText("help", commands[k]["forms"][form-1].get<std::string>() + std::string(" ") +
                  commands[k]["usage"].get<std::string>());
                printText("help", std::string("  ") + commands[k]["short"].get<std::string>());
              }
            }
          }
        }
      }
    }
  }
  if (found)
    printText("help", "");
  else
    printText("error", replaceAllInstances(advGetText("help_for_item_not_found"), "${ITEM}", arg));
}

void AdvClient::windowResized(int SIG)
{
  stopUI();
  startUI();
}

std::string AdvClient::advGetText(std::string label)
{
  std::string ret = label;
  if (checkString(&strings, label)) return strings[label].get<std::string>();
  if (!random_string) {
    pickRandomString(label);
    if (checkString(&strings, std::string(label + "@" + std::to_string(random_string))))
      ret = strings[std::string(label + "@" + std::to_string(random_string))].get<std::string>();
    random_string = 0;
  }
  else
    if (checkString(&strings, std::string(label + "@" + std::to_string(random_string))))
      ret = strings[std::string(label + "@" + std::to_string(random_string))].get<std::string>();
  return ret;
}

void AdvClient::pickRandomString(std::string label)
{
  unsigned int index = 1;
  while (++index) {
    if (checkString(&strings, std::string(label + "@" + std::to_string(index))))
      continue;
    else
      break;
  }
  if (index > 2) random_string = (rand() % (index-1)) + 1; else random_string = 1;
}

std::string AdvClient::getTime()
{
  std::string ret = "[";
  time_t now = time(0);
  tm t = *localtime(&now);
  if (t.tm_hour < 10) ret += "0";
  ret += (std::to_string(t.tm_hour) + ":");
  if (t.tm_min < 10) ret += "0";
  ret += (std::to_string(t.tm_min) + "]");
  return ret;
}

std::string AdvClient::getDateAndTime(time_t moment)
{
  std::string ret;
  tm t = *localtime(&moment);
  ret = std::to_string(t.tm_year + 1900) + "-";
  if (t.tm_mon < 10) ret += "0";
  ret += std::to_string(t.tm_mon + 1) + "-";
  if (t.tm_mday < 10) ret += "0";
  ret += std::to_string(t.tm_mday) + " ";
  if (t.tm_hour < 10) ret += "0";
  ret += (std::to_string(t.tm_hour) + ":");
  if (t.tm_min < 10) ret += "0";
  ret += std::to_string(t.tm_min);
  return ret;
}

std::string AdvClient::getElapsedTime(unsigned long diff)
{
  std::string ret1, ret2;
  unsigned short seconds = diff % 60;
  diff /= 60;
  unsigned short minutes = diff % 60;
  diff /= 60;
  unsigned short hours = diff % 24;
  diff /= 24;
  unsigned short days = diff % 365;
  diff /= 365;
  if (diff) {
    if (diff == 1) ret1 = advGetText("time_one_year");
    else ret1 = replaceAllInstances(advGetText("time_years"), "${YEARS}", std::to_string(diff));
    if (days == 1) ret2 = advGetText("time_one_day");
    else if (days >= 1) ret2 = replaceAllInstances(advGetText("time_days"), "${DAYS}", std::to_string(days));
  }
  else if (days) {
    if (days == 1) ret1 = advGetText("time_one_day");
    else ret1 = replaceAllInstances(advGetText("time_days"), "${DAYS}", std::to_string(days));
    if (hours == 1) ret2 = advGetText("time_one_hour");
    else if (hours >= 1) ret2 = replaceAllInstances(advGetText("time_hours"), "${HOURS}", std::to_string(hours));
  }
  else if (hours) {
    if (hours == 1) ret1 = advGetText("time_one_hour");
    else ret1 = replaceAllInstances(advGetText("time_hours"), "${HOURS}", std::to_string(hours));
    if (minutes == 1) ret2 = advGetText("time_one_minute");
    else if (minutes >= 1) ret2 = replaceAllInstances(advGetText("time_minutes"), "${MINUTES}", std::to_string(minutes));
  }
  else if (minutes) {
    if (minutes == 1) ret1 = advGetText("time_one_minute");
    else ret1 = replaceAllInstances(advGetText("time_minutes"), "${MINUTES}", std::to_string(minutes));
    if (seconds == 1) ret2 = advGetText("time_one_second");
    else if (seconds >= 1) ret2 = replaceAllInstances(advGetText("time_seconds"), "${SECONDS}", std::to_string(seconds));
  }
  else {
    if (seconds == 1) ret1 = advGetText("time_one_second");
    else ret1 = replaceAllInstances(advGetText("time_seconds"), "${SECONDS}", std::to_string(seconds));
  }
  if (!ret2.empty()) ret1 += " " + ret2;
  return ret1;
}

time_t AdvClient::parseElapsedTime(std::string str)
{
  char c;
  std::string n;
  unsigned long seconds, minutes, hours, days;
  seconds = minutes = hours = days = 0;
  while (!str.empty()) {
    c = str[0];
    str.erase(0, 1);
    switch (c) {
      case 'd':
        try {
          days += std::stoi(n);
          n.clear();
        } catch (...) { return 0; }
        break;
      case 'h':
        try {
          hours += std::stoi(n);
          n.clear();
        } catch (...) { return 0; }
        break;
      case 'm':
        try {
          minutes += std::stoi(n);
          n.clear();
        } catch (...) { return 0; }
        break;
      case 's':
        try {
          seconds += std::stoi(n);
          n.clear();
        } catch (...) { return 0; }
        break;
      default:
        n += c;
        break;
    }
  }
  if (!n.empty())
    try {
      seconds += std::stoi(n);
    } catch (...) { return 0; }
  return (time_t) ((days*24 + hours)*60 + minutes)*60 + seconds;
}

std::string AdvClient::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

void AdvClient::nextArg(std::string &string, std::string &arg)
{
  arg = "";
  while (std::isspace(string[0])) string.erase(0, 1);
  while (string.length() > 0) {
    if (std::isspace(string[0])) {
      string.erase(0, 1);
      break;
    }
    else arg += string[0];
    string.erase(0, 1);
  }
}

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This slot is executed when a game sends most API calls
void AdvServer::onModApiCall(AdvGame *game, nlohmann::json call)
{
  std::string log_message;
  startReadingGames();
  if (!checkString(&call, "class")) {
    log_message = replaceAllInstances(advGetText("unknown_api_call_on_game"), "${GAME}", game->getName());
    stopReadingGames();
    log_message = replaceAllInstances(log_message, "${CALL}", call.dump());
    log(6, log_message);
    return;
  }
  else if (call["class"] == "create") {
    if (!checkString(&call, "name")) {
      stopReadingGames();
      log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", game->getName() + "_"));
      return;
    }
    call["full-name"] = game->getName() + "_" + call["name"].get<std::string>();
    if (!checkArray(&call, "mods")) {
      stopReadingGames();
      log_message = replaceAllInstances(advGetText("invalid_child_game_mods"), "${GAME}", call["full-name"].get<std::string>());
      log_message = replaceAllInstances(log_message, "${MODS}", "[]");
      return;
    }
    if (!checkNameSanity(call["name"].get<std::string>())) {
      stopReadingGames();
      log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", call["full-name"].get<std::string>()));
      return;
    }
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == call["full-name"].get<std::string>()) {
        stopReadingGames();
        log(5, replaceAllInstances(advGetText("child_game_already_exists"), "${GAME}", call["full-name"].get<std::string>()));
        return;
      }
    stopReadingGames();
    if (!checkGameMods(call["mods"], true)) {
      log_message = replaceAllInstances(advGetText("invalid_child_game_mods"), "${GAME}", call["full-name"].get<std::string>());
      log_message = replaceAllInstances(log_message, "${MODS}", call["mods"].dump());
      log(5, log_message);
      return;
    }
    if (checkArray(&call, "libs") && !checkGameMods(call["libs"], true, true)) {
      log_message = replaceAllInstances(advGetText("invalid_child_game_mods"), "${GAME}", call["full-name"].get<std::string>());
      log_message = replaceAllInstances(log_message, "${MODS}", call["libs"].dump());
      log(5, log_message);
      return;
    }
    else
      call["libs"] = nlohmann::json::parse("[]");
    call["mods"] = resolveModDependencies(call["mods"]);
    call["libs"] = resolveModLibs(call["mods"], call["libs"]);
    log(7, replaceAllInstances(AdvServer::advGetText("scheduling_child_game_creation"), "${GAME}", call["full-name"].get<std::string>()));
    pthread_mutex_lock(&creation_mutex);
    scheduled_creations.push_back({call["full-name"].get<std::string>(), "..", call["mods"], call["name"].get<std::string>(), call["libs"]});
    pthread_mutex_unlock(&creation_mutex);
  }
  else if (call["class"] == "manage") {
    if (!checkString(&call, "branch")) {
      stopReadingGames();
      return;
    }
    call["full-name"] = game->getName() + "_" + call["branch"].get<std::string>();
    if (!checkNameSanity(call["branch"].get<std::string>())) {
      stopReadingGames();
      log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", call["full-name"].get<std::string>()));
      return;
    }
    if (!checkString(&call, "mod")) {
      stopReadingGames();
      log_message = advGetText("mod_name_not_provided_on_manage_call");
      log_message = replaceAllInstances(log_message, "${GAME}", call["full-name"].get<std::string>());
      log(5, log_message);
      return;
    }
    if (!checkArray(&call, "arguments")) call["arguments"] = nlohmann::json::parse("[]");
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == call["full-name"].get<std::string>()) {
        g->manage(call["mod"].get<std::string>(), call["arguments"]);
        stopReadingGames();
        return;
      }
    stopReadingGames();
    log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
  }
  else if (call["class"] == "destroy") {
    if (!checkString(&call, "name")) {
      stopReadingGames();
      return;
    }
    call["full-name"] = game->getName() + "_" + call["name"].get<std::string>();
    if (!checkNameSanity(call["name"].get<std::string>())) {
      stopReadingGames();
      log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", call["full-name"].get<std::string>()));
      return;
    }
    game->popBranch(call["name"]);
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == call["full-name"].get<std::string>()) {
        log(7, replaceAllInstances(advGetText("scheduling_child_game_destruction"), "${GAME}", call["full-name"].get<std::string>()));
        pthread_mutex_lock(&destruction_mutex);
        scheduled_destructions.push_back(g);
        pthread_mutex_unlock(&destruction_mutex);
        stopReadingGames();
        return;
      }
    stopReadingGames();
    log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
  }
  else if (call["class"] == "teleport") {
    if (!checkString(&call, "player")) {
      stopReadingGames();
      return;
    }
    if (!checkString(&call, "destiny")) {
      stopReadingGames();
      return;
    }
    std::list<AdvPlayerConnection*>::iterator it;
    startReadingConnections();
    startReadingHandles();
    for (it = connections.begin(); it != connections.end(); ++it)
      if ((*it)->handle == call["player"].get<std::string>()) break;
    if (it == connections.end()) {
      stopReadingConnections();
      stopReadingHandles();
      stopReadingGames();
      return;
    }
    if (call["destiny"] == ".")
      call["full-name"] = game->getName();
    else {
      call["full-name"] = game->getName() + "_" + call["destiny"].get<std::string>();
      if (!checkNameSanity(call["destiny"].get<std::string>())) {
        stopReadingConnections();
        stopReadingHandles();
        stopReadingGames();
        log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", call["full-name"].get<std::string>()));
        return;
      }
    }
    startReadingIngames();
    if ((*it)->game == call["full-name"]) {
      stopReadingGames();
      stopReadingIngames();
      log_message = advGetText("player_already_in_destiny");
      log_message = replaceAllInstances(log_message, "${GAME}", call["full-name"].get<std::string>());
      log_message = replaceAllInstances(log_message, "${PLAYER}", (*it)->handle);
      stopReadingConnections();
      stopReadingHandles();
      log(5, log_message);
      return;
    }
    std::list<AdvGame>::iterator g;
    if ((*it)->game == game->getName()) { // Player is inside caller game; move it to one of its branches
      for (g = games.begin(); g != games.end(); ++g)
        if (g->getName() == call["full-name"].get<std::string>()) break;
      if (g == games.end()) {
        stopReadingConnections();
        stopReadingHandles();
        stopReadingIngames();
        stopReadingGames();
        log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
        return;
      }
      (*it)->game = call["full-name"];
      g->playerEntered((*it)->handle);
    }
    else if ((*it)->game.substr(0, game->getName().length() + 1) == std::string(game->getName() + "_")) {
      // Player must be inside a child game
      for (g = games.begin(); g != games.end(); ++g)
        if (g->getName() == call["full-name"].get<std::string>()) break;
      if (g == games.end()) {
        stopReadingConnections();
        stopReadingHandles();
        stopReadingIngames();
        stopReadingGames();
        log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
        return;
      }
      std::list<AdvGame>::reverse_iterator h = games.rbegin();
      nlohmann::json j;
      size_t n;
      j["username"] = (*it)->handle;
      while (h != games.rend() && (*it)->game != game->getName()) {
        for (; h != games.rend(); ++h)
          if (h->getName() == (*it)->game) break;
        if (h != games.rend()) {
          j["name"] = h->getName();
          runHook("leave", j.dump());
          h->playerLeft((*it)->handle);
          ++h;
          if ((n = (*it)->game.rfind("_")) != std::string::npos)
            (*it)->game = (*it)->game.substr(0, n);
          else
            (*it)->game.clear();
        }
      }
      (*it)->game = call["full-name"];
      if ((*it)->game != game->getName())
        if (checkString(&call, "lang"))
          g->playerEntered((*it)->handle, call["lang"]);
        else
          g->playerEntered((*it)->handle, lang);
    }
    else
      log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
    stopReadingConnections();
    stopReadingHandles();
    stopReadingIngames();
    stopReadingGames();
  }
  else if (call["class"] == "child") {
    if (!checkString(&call, "branch")) {
      stopReadingGames();
      return;
    }
    call["full-name"] = game->getName() + "_" + call["branch"].get<std::string>();
    if (!checkNameSanity(call["branch"].get<std::string>())) {
      stopReadingGames();
      log(5, replaceAllInstances(advGetText("invalid_child_game_name"), "${GAME}", call["full-name"].get<std::string>()));
      return;
    }
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == call["full-name"].get<std::string>()) {
        if (!hasParameter(&call, "content")) call["content"] = nlohmann::json::parse("{}");
        g->messageFromParent(call["content"]);
        stopReadingGames();
        return;
      }
    stopReadingGames();
    log(5, replaceAllInstances(advGetText("not_a_child_game"), "${GAME}", call["full-name"].get<std::string>()));
  }
  else if (call["class"] == "parent") {
    size_t n;
    if ((n = game->getName().rfind("_")) == std::string::npos) {
      log(5, replaceAllInstances(advGetText("game_is_not_a_branch"), "${GAME}", game->getName()));
      stopReadingGames();
      return;
    }
    call["parent"] = game->getName().substr(0, n);
    call["child"] = game->getName().substr(n + 1);
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == call["parent"]) {
        if (!hasParameter(&call, "content")) call["content"] = nlohmann::json::parse("{}");
        g->messageFromChild(call["child"], call["content"]);
        break;
      }
    stopReadingGames();
  }
  else {
    log_message = replaceAllInstances(advGetText("unknown_api_call_on_game"), "${GAME}", game->getName());
    stopReadingGames();
    log_message = replaceAllInstances(log_message, "${CALL}", call.dump());
    log(6, log_message);
  }
}

// This slot is executed every time a game wants to send a message to a player
void AdvServer::onSendMessage(nlohmann::json message)
{
  if (checkString(&message, "game")) { // Broadcast
    std::string skip;
    if (checkString(&message, "skip")) skip = message["skip"];
    if (!checkString(&message, "message") && !checkArray(&message, "message")) return;
    if (!checkString(&message, "type")) message["type"] = "plain";
    if (!checkString(&message, "tag")) message["tag"] = "";
    std::list<AdvPlayerConnection*>::iterator it;
    nlohmann::json response;
    response["class"] = "message";
    response["message"] = message["message"];
    response["type"] = message["type"];
    response["tag"] = message["tag"];
    startReadingConnections();
    startReadingHandles();
    startReadingIngames();
    for (it = connections.begin(); it != connections.end(); ++it)
      if ((*it)->game == message["game"].get<std::string>() && (*it)->handle != skip) (*it)->sendEvent(response.dump());
    stopReadingConnections();
    stopReadingHandles();
    stopReadingIngames();
  }
  else if (checkArray(&message, "action")) { // Write
    if (!checkString(&message, "player")) return;
    if (!checkArray(&message, "text")) message["text"] = nlohmann::json::parse("[]");
    std::list<AdvPlayerConnection*>::iterator it;
    startReadingConnections();
    startReadingHandles();
    for (it = connections.begin(); it != connections.end(); ++it)
      if ((*it)->handle == message["player"].get<std::string>()) break;
    stopReadingHandles();
    if (it == connections.end()) {
      stopReadingConnections();
      return;
    }
    nlohmann::json response;
    response["class"] = "write";
    response["action"] = message["action"];
    response["text"] = message["text"];
    (*it)->sendEvent(response.dump());
    stopReadingConnections();
  }
  else { // Message
    if (!checkString(&message, "player")) return;
    if (!checkString(&message, "message") && !checkArray(&message, "message")) return;
    std::list<AdvPlayerConnection*>::iterator it;
    startReadingConnections();
    startReadingHandles();
    for (it = connections.begin(); it != connections.end(); ++it)
      if ((*it)->handle == message["player"].get<std::string>()) break;
    stopReadingHandles();
    if (it == connections.end()) {
      stopReadingConnections();
      return;
    }
    if (!checkString(&message, "type")) message["type"] = "plain";
    if (!checkString(&message, "tag")) message["tag"] = "";
    nlohmann::json response;
    response["class"] = "message";
    response["message"] = message["message"];
    response["type"] = message["type"];
    response["tag"] = message["tag"];
    (*it)->sendEvent(response.dump());
    stopReadingConnections();
  }
}

// This slot is executed every time a game kicks a player from the game
void AdvServer::onKick(AdvGame *game, std::string player)
{
  std::list<AdvPlayerConnection*>::iterator it;
  startReadingConnections();
  startReadingHandles();
  startReadingIngames();
  startReadingGames();
  for (it = connections.begin(); it != connections.end(); ++it)
    if ((*it)->handle == player) break;
  // Check player existence and game permission
  if ( it == connections.end() || ( ((*it)->game != game->getName()) &&
    ((*it)->game.substr(0, game->getName().length() + 1) != std::string(game->getName() + "_")) ) ) {
    stopReadingConnections();
    stopReadingHandles();
    stopReadingIngames();
    stopReadingGames();
    return;
  }
  stopReadingHandles();
  stopReadingIngames();
  if (game->getName().rfind("_") == std::string::npos) { // The game is not a branch
    leaveFromGame(it);
  }
  else {
    leaveFromGame(it, game->getName());
    for (std::list<AdvGame>::iterator g = games.begin(); g != games.end(); ++g)
      if (g->getName() == (*it)->game) {
        g->playerKicked(player, game->getName().substr(game->getName().rfind("_")+1));
        break;
      }
  }
  stopReadingConnections();
  stopReadingGames();
}

// This slot is executed every time a client sends a request to the server through its AdvPlayerConnection
void AdvServer::onRequestReceived(AdvPlayerConnection* advPlayerConnection, int fd, std::string request)
{
  std::string log_message;
  std::list<AdvPlayerConnection*>::iterator it;
  nlohmann::json message, response;
  startReadingConnections();
  for (it = connections.begin(); it != connections.end(); ++it)
    if (*it == advPlayerConnection) break;
  if (it == connections.end()) {
    stopReadingConnections();
    log(3, AdvServer::advGetText("request_received_from_connection_not_in_the_list"));
    return;
  }
  startReadingHandles();
  if ((*it)->handle.empty())
    log_message = AdvServer::advGetText("request_received_from_non_identified");
  else {
    log_message = AdvServer::advGetText("request_received_from_player");
    log_message = AdvServer::replaceAllInstances(log_message, "${PLAYER}", (*it)->handle);
  }
  stopReadingHandles();
  try {
    message = nlohmann::json::parse(request);
  }
  catch (...) {
    stopReadingConnections();
    log_message = AdvServer::advGetText("invalid_json_on_request");
    log_message = AdvServer::replaceAllInstances(log_message, "${REQUEST}", request);
    log(7, log_message);
    return;
  }
  // Check request class
  if (!checkString(&message, "class") || message["class"].get<std::string>().empty() ||
    !hasParameter(&client_api, message["class"].get<std::string>())) {
    response["class"] = "error";
    response["error"] = "invalid_api_call";
    response["content"] = message;
    advPlayerConnection->sendEvent(response.dump());
    stopReadingConnections();
    return;
  }
  if (client_api[message["class"].get<std::string>()] == "Account") {
    nlohmann::json j = message;
    if (checkString(&j, "password")) j["password"] = advGetText("password_hidden");
    log(7, AdvServer::replaceAllInstances(log_message, "${REQUEST}", j.dump()));
    handleAccountRequest(it, message);
  }
  else
    log(7, AdvServer::replaceAllInstances(log_message, "${REQUEST}", request));
  if (client_api[message["class"].get<std::string>()] == "Info")
    handleInfoRequest(it, message);
  else if (client_api[message["class"].get<std::string>()] == "GameManagement")
    handleGameManagementRequest(it, message);
  else if (client_api[message["class"].get<std::string>()] == "Play")
    handlePlayRequest(it, message);
  else if (client_api[message["class"].get<std::string>()] == "Chat")
    handleChatRequest(it, message);
  else if (message["class"] == "admin")
    handleAdminRequest(it, message);
  stopReadingConnections();
}

// This slot is executed every time a client connection is closed
void AdvServer::onConnectionClosed(AdvPlayerConnection* advPlayerConnection, int fd, std::string err)
{
  std::list<AdvPlayerConnection*>::iterator it;
  startReadingConnections();
  for (it = connections.begin(); it != connections.end(); ++it)
    if (*it == advPlayerConnection) break;
  stopReadingConnections();
  if (it != connections.end()) {
    logoutFromServer(it);
    writeConnections();
    connections.erase(it);
    pthread_mutex_unlock(&connections_mutex);
  }
  std::string log_message = AdvServer::advGetText("connection_on_fd_closed");
  log_message = replaceAllInstances(log_message, "${FD}", std::to_string(fd));
  log_message = replaceAllInstances(log_message, "${REASON}", err);
  log(7, log_message);
}

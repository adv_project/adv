/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _X_OPEN_SOURCE_EXTENDED // Needed for extended characters to work

#include "../include/AdvClient.h"

AdvClient client;
void exitClient(int signal) { client.exit(); }

int main(int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8"); // Force UTF8 to allow extended characters
  // Exit the client as soon as an interrupt, quit or terminate signal is received.
  signal(SIGINT, exitClient);
  signal(SIGQUIT, exitClient);
  signal(SIGTERM, exitClient);
  signal(SIGPIPE, SIG_IGN); // Ignore broken pipe signals
  return client.exec(argc, argv);
}

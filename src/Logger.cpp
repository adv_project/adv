/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvServer.h"
#include "../include/Logger.h"

Logger::Logger(int log_level, std::string default_log_file, std::string log_directory, std::string store_directory)
{
  setLogLevel(log_level);
  setLogDirectory(log_directory);
  setStoreDirectory(store_directory);
  setDefaultLogFile(default_log_file);
  time_t now = time(0);
  tm *t = localtime(&now);
  strftime(&last_log_date[0], 16, "%Y-%m", t);
}

void Logger::setLogLevel(int log_level)
{
  if (log_level < 0) log_level = 5;
  if (log_level > 7) log_level = 7;
  this->log_level = log_level;
}

void Logger::setDefaultLogFile(std::string default_log_file)
{
  this->default_log_file = default_log_file;
}

void Logger::setLogDirectory(std::string log_directory)
{
  if (log_directory.length() == 0) log_directory = "./";
  if (log_directory[log_directory.length()-1] != '/') log_directory.append("/");
  this->log_directory = log_directory;
}

void Logger::setStoreDirectory(std::string store_directory)
{
  if (store_directory.length() == 0) store_directory = "./";
  if (store_directory[store_directory.length()-1] != '/') store_directory.append("/");
  this->store_directory = store_directory;
}

void Logger::log(int log_level, std::string log_file, std::string log_message)
{
  if (log_level <= this->log_level) {
    char date[32];
    time_t now = time(0);
    tm *t = localtime(&now);
    strftime(&date[0], 16, "%Y-%m", t);
    if (log_file == "") log_file = default_log_file;
    std::string filename = log_directory + log_file;
    if (strcmp(last_log_date, date) != 0) {
      std::ifstream src(filename.c_str(), std::ios::binary);
      std::ofstream dest(std::string(store_directory + last_log_date + ".log").c_str(), std::ios::binary);
      dest << src.rdbuf();
      src.close();
      dest.close();
      unlink(filename.c_str());
      strncpy(last_log_date, date, 16);
    }
    std::ofstream file(filename, std::ios::out | std::ios::app);
    strftime(&date[0], 32, "%F %H:%M:%S", t);
    file << "[" << date << "] " << logLevel(log_level, false) << ": " << log_message << std::endl;
    file.close();
  }
}

std::string Logger::logLevel(int log_level, bool shortened)
{
  switch (log_level) {
    case 0:  if (shortened) return "emerg";  else return AdvServer::advGetText("log_level_emergency");
    case 1:  if (shortened) return "alert";  else return AdvServer::advGetText("log_level_alert");
    case 2:  if (shortened) return "crit";   else return AdvServer::advGetText("log_level_critical");
    case 3:  if (shortened) return "err";    else return AdvServer::advGetText("log_level_error");
    case 4:  if (shortened) return "warn";   else return AdvServer::advGetText("log_level_warning");
    case 5:  if (shortened) return "notice"; else return AdvServer::advGetText("log_level_notice");
    case 6:  if (shortened) return "info";   else return AdvServer::advGetText("log_level_info");
    default: if (shortened) return "debug";  else return AdvServer::advGetText("log_level_debug");
  }
}

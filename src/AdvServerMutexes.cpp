/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvServer::startReadingConnections()
{
  pthread_mutex_lock(&connections_mutex);
  ++connections_readers;
  pthread_mutex_unlock(&connections_mutex);
}

void AdvServer::stopReadingConnections()
{
  pthread_mutex_lock(&connections_mutex);
  --connections_readers;
  pthread_mutex_unlock(&connections_mutex);
}

// connections_mutex is locked after returning
void AdvServer::writeConnections()
{
  while (true) {
    pthread_mutex_lock(&connections_mutex);
    if (connections_readers < 0) {
      log(3, replaceAllInstances(advGetText("mutex_out_of_range"), "${MUTEX}", "connections_readers"));
      do_exit = true;
      return;
    }
    if (!connections_readers) {
      return;
    }
    else {
      pthread_mutex_unlock(&connections_mutex);
      usleep(delay);
    }
  }
}

void AdvServer::startReadingGames()
{
  pthread_mutex_lock(&games_mutex);
  ++games_readers;
  pthread_mutex_unlock(&games_mutex);
}

void AdvServer::stopReadingGames()
{
  pthread_mutex_lock(&games_mutex);
  --games_readers;
  pthread_mutex_unlock(&games_mutex);
}

// games_mutex is locked after returning
void AdvServer::writeGames()
{
  while (true) {
    pthread_mutex_lock(&games_mutex);
    if (games_readers < 0) {
      log(3, replaceAllInstances(advGetText("mutex_out_of_range"), "${MUTEX}", "games_readers"));
      do_exit = true;
      return;
    }
    if (!games_readers) {
      return;
    }
    else {
      pthread_mutex_unlock(&games_mutex);
      usleep(delay);
    }
  }
}

void AdvServer::startReadingHandles()
{
  pthread_mutex_lock(&handles_mutex);
  ++handles_readers;
  pthread_mutex_unlock(&handles_mutex);
}

void AdvServer::stopReadingHandles()
{
  pthread_mutex_lock(&handles_mutex);
  --handles_readers;
  pthread_mutex_unlock(&handles_mutex);
}

// handles_mutex is locked after returning
void AdvServer::writeHandles()
{
  while (true) {
    pthread_mutex_lock(&handles_mutex);
    if (handles_readers < 0) {
      log(3, replaceAllInstances(advGetText("mutex_out_of_range"), "${MUTEX}", "handles_readers"));
      do_exit = true;
      return;
    }
    if (!handles_readers) {
      return;
    }
    else {
      pthread_mutex_unlock(&handles_mutex);
      usleep(delay);
    }
  }
}

void AdvServer::startReadingIngames()
{
  pthread_mutex_lock(&ingames_mutex);
  ++ingames_readers;
  pthread_mutex_unlock(&ingames_mutex);
}

void AdvServer::stopReadingIngames()
{
  pthread_mutex_lock(&ingames_mutex);
  --ingames_readers;
  pthread_mutex_unlock(&ingames_mutex);
}

// ingames_mutex is locked after returning
void AdvServer::writeIngames()
{
  while (true) {
    pthread_mutex_lock(&ingames_mutex);
    if (ingames_readers < 0) {
      log(3, replaceAllInstances(advGetText("mutex_out_of_range"), "${MUTEX}", "ingames_readers"));
      do_exit = true;
      return;
    }
    if (!ingames_readers) {
      return;
    }
    else {
      pthread_mutex_unlock(&ingames_mutex);
      usleep(delay);
    }
  }
}

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * The AdvPlayerConnection class manages a client connection socket.
 * It announces every null-terminated string received through the
 * requestReceived signal and a disconnection event through the
 * connectionClosed signal.
 */

#include "../include/AdvPlayerConnection.h"

unsigned long AdvPlayerConnection::delay = 50000;
time_t AdvPlayerConnection::timeout = 3;

// The object constructor runs the connection thread immediately.
AdvPlayerConnection::AdvPlayerConnection(int fd, SSL_CTX* ssl_ctx)
{
  do_exit = false;
  this->fd = fd;
  this->ssl_ctx = ssl_ctx;
  uptime = time(0);
  events_mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_create(&thread, nullptr, &callThread, this);
}

// The connection thread cannot be a static method,
// so it is executed through this static method
void* AdvPlayerConnection::callThread(void *This)
{
  int oldstate;
  pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS, &oldstate);
  // Read "This" as a pointer to a AdvPlayerConnection
  AdvPlayerConnection *me = static_cast<AdvPlayerConnection *>(This);
  me->runThread(); // Run the real connection thread
  pthread_exit(nullptr);
}

void AdvPlayerConnection::runThread()
{
  char c;
  int ret;
  unsigned short ping_period = 0;
  std::string request;
  ssl = SSL_new(ssl_ctx);
  SSL_set_fd(ssl, fd);
  SSL_set_accept_state(ssl);
  while (true) {
    usleep(delay);
    if (time(0) - uptime > 10) {
      close(fd);
      connectionClosed.emit(this, fd, "SSL_ERROR_TIMEOUT");
      do_exit = true;
      break;
    }
    ERR_clear_error();
    ret = SSL_accept(ssl);
    if (ret == 1)
      break;
    else {
      ret = SSL_get_error(ssl, ret);
      if (ret == SSL_ERROR_WANT_READ || ret == SSL_ERROR_WANT_WRITE)
        continue;
      else {
        std::string str = "SSL_ERROR_UNKNOWN";
        switch (ret) {
          case SSL_ERROR_NONE: str = "SSL_ERROR_NONE"; break;
          case SSL_ERROR_ZERO_RETURN: str = "SSL_ERROR_ZERO_RETURN"; break;
          case SSL_ERROR_WANT_CONNECT: str = "SSL_ERROR_WANT_CONNECT"; break;
          case SSL_ERROR_WANT_ACCEPT: str = "SSL_ERROR_WANT_ACCEPT"; break;
          case SSL_ERROR_WANT_X509_LOOKUP: str = "SSL_ERROR_WANT_X509_LOOKUP"; break;
          case SSL_ERROR_WANT_ASYNC: str = "SSL_ERROR_WANT_ASYNC"; break;
          case SSL_ERROR_WANT_ASYNC_JOB: str = "SSL_ERROR_WANT_ASYNC_JOB"; break;
          case SSL_ERROR_WANT_CLIENT_HELLO_CB: str = "SSL_ERROR_WANT_CLIENT_HELLO_CB"; break;
          case SSL_ERROR_SYSCALL: str = "SSL_ERROR_SYSCALL"; break;
          case SSL_ERROR_SSL: str = "SSL_ERROR_SSL"; break;
        }
        close(fd);
        connectionClosed.emit(this, fd, str);
        do_exit = true;
        break;
      }
    }
  }
  while (!do_exit) {
    if (ping_period > 10) {
      ret = sendEvent(""); // Send a dummy message (ping) to the player
      if (ret == -1) { // Check that the ping was succesful or close the connection
        close(fd);
        connectionClosed.emit(this, fd, std::string(strerror(errno)));
        break;
      }
      if (!handle.empty())
        uptime = time(0);
      else
        if (time(0) - uptime > timeout) {
          close(fd);
          connectionClosed.emit(this, fd, "ERROR_IDLE_TIMEOUT");
          break;
        }
      ping_period = 0;
    }
    else ++ping_period;
    ret = SSL_read(ssl, &c, 1); // Check if there are incoming characters
    while (ret == 1) {
      if (c == '\0') {
        if (request.length() > 0) {
          requestReceived.emit(this, fd, request);
          request.clear();
        }
      }
      else request = request + c;
      ret = SSL_read(ssl, &c, 1);
    }
    usleep(delay);
  }
  SSL_shutdown(ssl);
  SSL_free(ssl);
}

ssize_t AdvPlayerConnection::sendEvent(std::string event)
{
  ssize_t ret;
  event += "\0";
  pthread_mutex_lock(&events_mutex);
  ret = SSL_write(ssl, event.c_str(), event.length() + 1);
  pthread_mutex_unlock(&events_mutex);
  return ret;
}

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvServer.h"
#include "AdvServerSlots.cpp"
#include "AdvServerRequests.cpp"
#include "AdvServerMutexes.cpp"

nlohmann::json AdvServer::strings;

// Initialize server default values
AdvServer::AdvServer()
{
  readLang();
  loadStrings();
  setlocale(LC_ALL, "");
  do_exit = false;
  delay = 50000;
  sync_period = 3600;
  socket_period = 30;
  socket_tries = 3;
  motd = nlohmann::json::parse("[]");
  admins = nlohmann::json::parse("[]");
  moderators = nlohmann::json::parse("[]");
  chat_backlog = nlohmann::json::parse("[]");
  max_players = 0;
  max_games = 0;
  chat_backlog_size = 50;
  creation_rate = 0;
  last_created = 0;
  enable_registration = true;
  enable_unregistered = true;
  AdvPlayerConnection::setDelay(delay);
  logger = nullptr;
  log_mutex = PTHREAD_MUTEX_INITIALIZER;
  connections_mutex = PTHREAD_MUTEX_INITIALIZER;
  handles_mutex = PTHREAD_MUTEX_INITIALIZER;
  ingames_mutex = PTHREAD_MUTEX_INITIALIZER;
  accounts_mutex = PTHREAD_MUTEX_INITIALIZER;
  games_mutex = PTHREAD_MUTEX_INITIALIZER;
  creation_mutex = PTHREAD_MUTEX_INITIALIZER;
  destruction_mutex = PTHREAD_MUTEX_INITIALIZER;
  config_mutex = PTHREAD_MUTEX_INITIALIZER;
  sync_mutex = PTHREAD_MUTEX_INITIALIZER;
  server_hooks_mutex = PTHREAD_MUTEX_INITIALIZER;
  connections_readers = games_readers = handles_readers = ingames_readers = 0;
  port = DEFAULT_PORT;
  dbname = "adv";
  dbhost = "localhost";
  dbport = 5432;
  dbuser = "adv";
  dbpass = "";
  rsync_excludes = " --exclude players.json --exclude config.json --exclude hooks --exclude ssl --exclude users --exclude logs ";
}

// Read argc and argv as command line arguments and perform the requested action
int AdvServer::exec(int argc, char **argv)
{
  loadStrings();
  std::string usage = std::string(SERVERPROGRAM) + " [ -huv ] { --start | --stop | --sync }";
  std::string action;
  for (int i = 1; i < argc; ++i) {
    if ( (strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0) ) {
      action = "help";
      break;
    }
    else if ( (strcmp(argv[i], "-u") == 0) || (strcmp(argv[i], "--usage") == 0) ) {
      action = "usage";
      break;
    }
    else if ( (strcmp(argv[i], "-v") == 0) || (strcmp(argv[i], "--version") == 0) ) {
      action = "version";
      break;
    }
    else if (strcmp(argv[i], "--start") == 0) {
      if (action == "") action = "start"; else action = "error_one_action";
    }
    else if (strcmp(argv[i], "--stop") == 0) {
      if (action == "") action = "stop"; else action = "error_one_action";
    }
    else if (strcmp(argv[i], "--sync") == 0) {
      if (action == "") action = "sync"; else action = "error_one_action";
    }
    else {
      std::cerr << AdvServer::advGetText("action_not_recognized") << " " << argv[i] << std::endl;
      std::cerr << usage << std::endl;
      return 1;
    }
  }
  if (action == "help") {
    std::cout << usage << std::endl;
    int line = 1;
    while (AdvServer::advGetText("command_line_help_" + std::to_string(line)) !=
           std::string("command_line_help_" + std::to_string(line))) {
      std::cout << AdvServer::advGetText("command_line_help_" + std::to_string(line)) << std::endl;
      ++line;
    }
    return 0;
  }
  if (action == "usage") {
    std::cout << std::string(AdvServer::advGetText("usage")) + " " << usage << std::endl;
    return 0;
  }
  if (action == "version") {
    std::string version = AdvServer::advGetText("version");
    version = replaceAllInstances(version, "${PROGRAM}", SERVERPROGRAM);
    version = replaceAllInstances(version, "${VERSION}", PROGRAMVERSION);
    std::cout << version << std::endl;
    std::cout << replaceAllInstances(AdvServer::advGetText("license"), "${LICENSE}", LICENSE) << std::endl;
    std::cout << replaceAllInstances(AdvServer::advGetText("written_by"), "${AUTHOR}", AUTHOR) << std::endl;
    return 0;
  }
  if (action == "") {
    std::cerr << AdvServer::advGetText("no_action_specified") << std::endl;
    std::cerr << usage << std::endl;
    return 1;
  }
  if (action == "error_one_action") {
    std::cerr << AdvServer::advGetText("only_one_action_must_be_specified") << std::endl;
    std::cerr << usage << std::endl;
    return 1;
  }
  // Setup static and working directories
  uid_t uid = getuid();
  if (uid == 0) { // Special directories for user root
    static_directory = "/var/lib/adv";
    working_directory = "/run/adv";
  }
  else {
    char* home_directory = getenv("HOME");
    if (home_directory != NULL) {
      static_directory = std::string(home_directory) + "/.adv";
      working_directory = "/run/user/" + std::to_string(uid) + "/adv";
    }
    else {
      std::cerr << AdvServer::advGetText("couldnt_find_home_directory") << std::endl;
      return 1;
    }
  }
  if (mkdirrecursively(0700, "/", static_directory) != 0) {
    std::cerr << AdvServer::replaceAllInstances(AdvServer::advGetText("couldnt_setup_static_directory"),
      "${DIR}", static_directory) << std::endl;
    return 1;
  }
  if ( access(static_directory.c_str(), R_OK) || access(static_directory.c_str(), W_OK) ||
    access(static_directory.c_str(), X_OK) ) {
    std::cerr << AdvServer::replaceAllInstances(AdvServer::advGetText("not_enough_permissions_on_static_directory"),
      "${DIR}", static_directory) << std::endl;
    return 1;
  }
  if (mkdirrecursively(0700, "/", working_directory) != 0) {
    std::cerr << AdvServer::replaceAllInstances(AdvServer::advGetText("couldnt_setup_working_directory"),
      "${DIR}", working_directory) << std::endl;
    return 1;
  }
  if ( access(working_directory.c_str(), R_OK) || access(working_directory.c_str(), W_OK) ||
    access(working_directory.c_str(), X_OK) ) {
    std::cerr << AdvServer::replaceAllInstances(AdvServer::advGetText("not_enough_permissions_on_working_directory"),
      "${DIR}", working_directory) << std::endl;
    return 1;
  }
  mkdirrecursively(0700, static_directory, "users");
  mkdirrecursively(0700, static_directory, "logs");
  mkdirrecursively(0700, working_directory, "games");
  mkdirrecursively(0700, working_directory, "trash");
  std::string pid_file = working_directory + "/pid";
  int pid_file_fd;
  if (action == "stop") { // Look for the server PID file and send a terminate signal to the process
    char pid_file_content[32];
    pid_t pid;
    if ((pid_file_fd = open(pid_file.c_str(), O_RDONLY)) == -1) {
      std::cerr << AdvServer::advGetText("couldnt_read_pid_file") << std::endl;
      return 1;
    }
    read(pid_file_fd, &pid_file_content[0], 32);
    close(pid_file_fd);
    pid = atoi(&pid_file_content[0]);
    kill(pid, SIGTERM);
    return 0;
  }
  if (action == "sync") { // Look for the server PID file and send a SIGUSR1 signal to the process
    char pid_file_content[32];
    pid_t pid;
    if ((pid_file_fd = open(pid_file.c_str(), O_RDONLY)) == -1) {
      std::cerr << AdvServer::advGetText("couldnt_read_pid_file") << std::endl;
      return 1;
    }
    read(pid_file_fd, &pid_file_content[0], 32);
    close(pid_file_fd);
    pid = atoi(&pid_file_content[0]);
    kill(pid, SIGUSR1);
    return 0;
  }
  if (action == "start") {
    if ((pid_file_fd = open(pid_file.c_str(), O_WRONLY | O_EXCL | O_CREAT, 0600)) == -1) {
      std::cerr << AdvServer::advGetText("server_is_already_running") << std::endl;
      return 1;
    }
    pid_t pid = fork();
    if (pid == 0) pid = getpid(); else return 0;
    std::string pid_file_content = std::to_string(pid);
    write(pid_file_fd, pid_file_content.c_str(), (size_t) pid_file_content.length());
    close(pid_file_fd);
    client_api["login"] = client_api["logout"] = client_api["register"] = client_api["password"] = "Account";
    client_api["version"] = client_api["games"] = client_api["mods"] = client_api["info"] = "Info";
    client_api["create"] = client_api["manage"] = client_api["destroy"] = client_api["transfer"] = "GameManagement";
    client_api["enter"] = client_api["leave"] = client_api["action"] = "Play";
    client_api["chat"] = client_api["roll"] = client_api["chat-backlog"] = "Chat";
    client_api["admin"]["set"] = client_api["admin"]["assign-role"] = client_api["admin"]["revoke-role"] = "Admin";
    client_api["admin"]["enable-registration"] = client_api["admin"]["disable-registration"] = "Admin";
    client_api["admin"]["enable-unregistered"] = client_api["admin"]["disable-unregistered"] = "Admin";
    client_api["admin"]["send"] = client_api["admin"]["broadcast"] = "Admin";
    client_api["admin"]["kick"] = client_api["admin"]["ban"] = "Admin";
    client_api["admin"]["players"] = client_api["admin"]["registered"] = client_api["admin"]["sync"] = "Admin";
    nlohmann::json mod_api;
    mod_api["server"] = "Server";
    mod_api["log"] = mod_api["send"] = mod_api["write"] = mod_api["broadcast"] = "Message";
    mod_api["run"] = mod_api["abort"] = mod_api["kick"] = mod_api["update"] = "GameWorkflow";
    mod_api["pause"] = mod_api["resume"] = mod_api["seconds-per-tick"] = mod_api["description"] = "GameProperties";
    mod_api["create"] = mod_api["destroy"] = mod_api["manage"] = "Branch";
    mod_api["teleport"] = mod_api["parent"] = mod_api["child"] = "Branch";
    AdvGame::loadModApi(mod_api);
    AdvGame::loadStaticDirectory(static_directory);
    return start();
  }
  return 1;
}

int AdvServer::start()
{
  initial_sync();
  last_sync = time(0);
  uptime = last_sync;
  srand(last_sync); // Initialize srand()
  chdir(working_directory.c_str());
  logger = new Logger(5, "adv-server.log", working_directory, static_directory + "/logs");
  log(5, AdvServer::advGetText("starting_adv_server"));
  updateCache();
  runHook("start", "{}");
  readConfig();
  readPlayersFile();
  readChatBacklog();
  loadGames();
  for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game) game->serverStarted();
  pthread_create(&thread, nullptr, &callConnectionsThread, this);
  std::list<game_birth_t> creations;
  std::list<std::list<AdvGame>::iterator> destructions;
  while (!do_exit) { // Main server loop
    if (time(0) - last_sync >= sync_period) sync("server_loop");
    pthread_mutex_lock(&creation_mutex);
    creations = scheduled_creations;
    scheduled_creations.clear();
    pthread_mutex_unlock(&creation_mutex);
    while (!creations.empty()) {
      if (creations.front().owner == "..")
        log(5, replaceAllInstances(AdvServer::advGetText("creating_new_child_game"), "${GAME}", creations.front().full_name));
      else
        log(5, replaceAllInstances(AdvServer::advGetText("creating_new_game"), "${GAME}", creations.front().full_name));
      writeGames();
      games.push_back(AdvGame(creations.front().full_name, gameDirectoryFromName(creations.front().full_name),
                              creations.front().owner, 60, creations.front().mods, creations.front().title,
                              dbname, dbhost, dbport, dbuser, dbpass, creations.front().libs));
      games.back().log.connect(this, &AdvServer::log);
      games.back().sendMessage.connect(this, &AdvServer::onSendMessage);
      games.back().kick.connect(this, &AdvServer::onKick);
      games.back().sendCall.connect(this, &AdvServer::onModApiCall);
      games.back().init(lang);
      if (creations.front().owner == "..") {
        size_t n = creations.front().full_name.rfind("_");
        std::string parent = creations.front().full_name.substr(0, n);
        for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game)
          if (game->getName() == parent) {
            game->pushBranch(creations.front().full_name.substr(n+1));
            break;
          }
        pthread_mutex_unlock(&games_mutex);
      }
      else {
        pthread_mutex_unlock(&games_mutex);
        nlohmann::json response;
        response["owner"]["username"] = response["admin"]["username"] = creations.front().owner;
        response["owner"]["name"] = response["admin"]["name"] = creations.front().full_name;
        runHook("create", response["owner"].dump());
        response["owner"].erase("username");
        response["owner"]["class"] = response["admin"]["event"] = "create";
        response["owner"]["response"] = response["admin"]["response"] = "success";
        response["owner"]["mods"] = creations.front().mods;
        response["owner"]["title"] = creations.front().title;
        response["admin"]["class"] = "admin";
        startReadingConnections();
        startReadingHandles();
        for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
          if ((*it)->handle == creations.front().owner) {
            (*it)->sendEvent(response["owner"].dump());
          }
          else if (isAdmin((*it)->handle)) {
            (*it)->sendEvent(response["admin"].dump());
          }
        stopReadingHandles();
        stopReadingConnections();
      }
      creations.pop_front();
      usleep(delay);
    }
    pthread_mutex_lock(&destruction_mutex);
    destructions = scheduled_destructions;
    scheduled_destructions.clear();
    pthread_mutex_unlock(&destruction_mutex);
    while (!destructions.empty()) {
      destroyGame(destructions.front());
      destructions.pop_front();
      usleep(delay);
    }
    startReadingGames();
    for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game) game->update();
    stopReadingGames();
    usleep(delay);
  }
  writeConnections();
  for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
    (*it)->exit();
  close(server_fd);
  pthread_mutex_unlock(&connections_mutex);
  log(7, AdvServer::advGetText("server_socket_closed"));
  runHook("stop", "{}");
  writeGames();
  for (std::list<AdvGame>::reverse_iterator game = games.rbegin(); game != games.rend(); ++game)
    game->serverStopped();
  while (!games.empty()) games.pop_front();
  pthread_mutex_unlock(&games_mutex);
  sync("server_stop");
  unlink(std::string(working_directory + "/pid").c_str());
  return 0;
}

void AdvServer::stop()
{
  log(5, AdvServer::advGetText("stopping_adv_server"));
  do_exit = true;
}

// The connections thread cannot be a static method,
// so it is executed through this static method
void* AdvServer::callConnectionsThread(void *This)
{
  int oldstate;
  pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS, &oldstate);
  // Read "This" as a pointer to a AdvServer
  AdvServer *me = static_cast<AdvServer *>(This);
  me->runConnectionsThread(); // Run the real connections thread
  pthread_exit(nullptr);
}

// Setup the server socket to open the server port.
// Then keep accepting new connections and give their control to an AdvPlayerConnection object.
void AdvServer::runConnectionsThread()
{
  sockaddr_in server, client;
  socklen_t clientl = sizeof(client);
  int client_fd;
  SSL_CTX* ssl_ctx;
  unsigned long tries = 0;
  bzero((char *) &server, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);
  AdvPlayerConnection* advPlayerConnection;
  AdvPlayerConnection* connection;
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) { // Create a TCP socket
    log(1, AdvServer::advGetText("couldnt_create_socket") + " (" + strerror(errno) + "). " + AdvServer::advGetText("exitting"));
    stop();
    return;
  }
  while (bind(server_fd, (struct sockaddr*)&server, sizeof(server)) == -1) { // Bind the socket to a TCP port
    log(1, AdvServer::replaceAllInstances(AdvServer::advGetText("couldnt_open_socket_on_port"),
      "${PORT}", std::to_string(port)) + " (" + strerror(errno) + ").");
    ++tries;
    if (tries >= socket_tries) {
      log(1, AdvServer::replaceAllInstances(AdvServer::advGetText("couldnt_open_socket_on_port"),
        "${PORT}", std::to_string(port)) + " (" + strerror(errno) + "). " + AdvServer::advGetText("exitting"));
      stop();
      return;
    }
    sleep(socket_period);
  }
  if (listen(server_fd, SOMAXCONN) == -1) { // Start listening for new connections
    log(1, AdvServer::replaceAllInstances(AdvServer::advGetText("couldnt_listen_on_port"),
      "${PORT}", std::to_string(port)) + " (" + strerror(errno) + "). " + AdvServer::advGetText("exitting"));
    stop();
    return;
  }
  SSL_load_error_strings();
  SSL_library_init();
  OpenSSL_add_all_algorithms();
  ssl_ctx = SSL_CTX_new(SSLv23_server_method());
  SSL_CTX_set_options(ssl_ctx, SSL_OP_SINGLE_DH_USE);
  SSL_CTX_set_verify(ssl_ctx, SSL_VERIFY_NONE, NULL);
  SSL_CTX_use_certificate_file(ssl_ctx, std::string(static_directory + "/ssl/server.crt").c_str() , SSL_FILETYPE_PEM);
  SSL_CTX_use_PrivateKey_file(ssl_ctx, std::string(static_directory + "/ssl/server.pem").c_str(), SSL_FILETYPE_PEM);
  if (!SSL_CTX_check_private_key(ssl_ctx)) {
    log(1, AdvServer::advGetText("private_key_doesnt_match_public_certificate"));
    ERR_free_strings();
    EVP_cleanup();
    stop();
    return;
  }
  fcntl(server_fd, F_SETFL, O_NONBLOCK); // Put the server socket in non-blocking mode
  log(5, AdvServer::replaceAllInstances(AdvServer::advGetText("adv_server_listening_on_port"), "${PORT}", std::to_string(port)));
  while (!do_exit) {
    client_fd = accept(server_fd, (struct sockaddr*)&client, &clientl); // Accept a new connection, if present
    if (client_fd > 0) {
      fcntl(client_fd, F_SETFL, O_NONBLOCK); // Put the client socket in non-blocking mode
      writeConnections();
      // Give control to an AdvPlayerConnection object and connect its requestReceived and connectionClosed
      // signals to this server's onRequestReceived() and onConnectionClosed() slots respectively.
      advPlayerConnection = new AdvPlayerConnection(client_fd, ssl_ctx);
      advPlayerConnection->requestReceived.connect(this, &AdvServer::onRequestReceived);
      advPlayerConnection->connectionClosed.connect(this, &AdvServer::onConnectionClosed);
      connection = advPlayerConnection;
      connections.push_back(connection); // Add connection to this server's connections list
      pthread_mutex_unlock(&connections_mutex);
      log(7, AdvServer::replaceAllInstances(AdvServer::advGetText("new_connection_spawned"), "${FD}", std::to_string(client_fd)));
    }
    else usleep(delay);
  }
  ERR_free_strings();
  EVP_cleanup();
}

void AdvServer::log(int log_level, std::string str)
{
  if (logger) {
    pthread_mutex_lock(&log_mutex);
    logger->log(log_level, "", str);
    pthread_mutex_unlock(&log_mutex);
  }
}

void AdvServer::updateCache()
{
  DIR *dir;
  struct dirent *entry;
  struct stat st;
  std::ifstream file;
  nlohmann::json json, mod;
  nlohmann::json mods = nlohmann::json::parse("[]");
  log(7, AdvServer::advGetText("updating_mods_and_hooks_cache"));
  cache.clear();
  cache["mods"] = nlohmann::json::parse("{}");
  cache["hooks"] = nlohmann::json::parse("{}");
  if ((dir = opendir(std::string(std::string(PREFIX) + "/share/adv/mods").c_str())) != NULL) {
    while ((entry = readdir(dir)) != NULL) { // Mods
      if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
      mods.push_back(entry->d_name);
    }
    closedir(dir);
  }
  for (int i = 0; i < mods.size(); ++i) {
    json.clear();
    cache["mods"][mods[i].get<std::string>()]["properties"] = nlohmann::json::parse("{}");
    cache["mods"][mods[i].get<std::string>()]["hooks"] = nlohmann::json::parse("{}");
    file.open(std::string(PREFIX) + "/share/adv/mods/" + mods[i].get<std::string>() + "/properties.json");
    if (file.good()) {
      try { file >> json; } catch (...) {}
      file.close();
    }
    else {
      continue;
    }
    mod.clear();
    mod["name"] = mods[i];
    if (checkString(&json, "type") && !json["type"].get<std::string>().empty()) mod["type"] = json["type"];
    if (checkString(&json, "name") && !json["name"].get<std::string>().empty()) mod["pretty-name"] = json["name"];
    if (checkString(&json, "version") && !json["version"].get<std::string>().empty()) mod["version"] = json["version"];
    if (checkString(&json, "url") && !json["url"].get<std::string>().empty()) mod["url"] = json["url"];
    if (checkArray(&json, "description")) mod["description"] = json["description"];
    if (checkArray(&json, "depends")) mod["depends"] = json["depends"];
    cache["mods"][mods[i].get<std::string>()]["properties"] = mod; // Mod properties
    if ((dir = opendir(std::string(std::string(PREFIX) + "/share/adv/mods/" + mods[i].get<std::string>() + "/hooks").c_str())) != NULL) {
      while ((entry = readdir(dir)) != NULL) { // Mod hooks
        if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
        cache["mods"][mods[i].get<std::string>()]["hooks"][entry->d_name] = true;
      }
      closedir(dir);
    }
  }
  if ((dir = opendir(std::string(static_directory + "/hooks").c_str())) != NULL) {
    while ((entry = readdir(dir)) != NULL) { // Server hooks
      if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
      if (stat(std::string(static_directory + "/hooks/" + entry->d_name).c_str(), &st) < 0) continue;
      if ((st.st_mode & S_IEXEC) == 0) continue;
      cache["hooks"][entry->d_name] = true;
    }
    closedir(dir);
  }
  AdvGame::updateModsCache(cache["mods"]);
}

void AdvServer::readLang()
{
  std::ifstream config_file;
  bool good = false;
  uid_t uid = getuid();
  std::string sdir;
  if (uid == 0) {
    sdir = "/var/lib/adv";
  }
  else {
    char* home_directory = getenv("HOME");
    if (home_directory != NULL) sdir = std::string(home_directory) + "/.adv";
  }
  std::string config_file_path = sdir + "/config.json";
  config_file.open(config_file_path.c_str());
  if (config_file.good())
    good = true;
  else {
    config_file_path = std::string(PREFIX) + "/share/adv/config.json";
    config_file.open(config_file_path.c_str());
    if (config_file.good()) {
      good = true;
    }
  }
  if (good) {
    std::string str;
    nlohmann::json config;
    try { config_file >> config; } catch (...) {}
    if (checkString(&config, "lang")) lang = config["lang"]; else lang = "en";
    config_file.close();
  }
}

void AdvServer::readConfig()
{
  std::ifstream config_file;
  bool good = false;
  std::string config_file_path = static_directory + "/config.json";
  config_file.open(config_file_path.c_str());
  if (config_file.good())
    good = true;
  else {
    config_file_path = std::string(PREFIX) + "/share/adv/config.json";
    config_file.open(config_file_path.c_str());
    if (config_file.good()) {
      good = true;
      std::ofstream config_file_copy;
      config_file_copy.open(std::string(static_directory + "/config.json").c_str());
      if (config_file_copy.good()) {
        config_file_copy << config_file.rdbuf();
        config_file_copy.close();
      }
    }
  }
  if (good) {
    std::string str;
    nlohmann::json config;
    try { config_file >> config; } catch (...) {}
    config_file.close();
    if (checkString(&config, "log-level") || checkInteger(&config, "log-level")) {
      int log_level;
      for (log_level = 0; log_level < 8; ++log_level)
        if (Logger::logLevel(log_level, true) == config["log-level"].get<std::string>()) {
          logger->setLogLevel(log_level);
          break;
        }
      if (log_level == 8) {
        try {
          log_level = (int) config["log-level"];
          logger->setLogLevel(log_level);
        }
        catch (...) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "log-level") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
      }
    }
    if (checkInteger(&config, "port")) {
      try {
        if ( config["port"] < 0 || config["port"] > 65535 ) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "port") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else port = (int) config["port"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "port") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "delay")) {
      try {
        if (config["delay"] < 1) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "delay") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else delay = (unsigned long) config["delay"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "delay") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
      AdvPlayerConnection::setDelay(delay);
    }
    if (checkInteger(&config, "sync-period")) {
      try {
        if (config["sync-period"] < 1) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "sync-period") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else sync_period = (unsigned long) config["sync-period"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "sync-period") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "socket-period")) {
      try {
        if (config["socket-period"] < 1) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "socket-period") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else socket_period = (unsigned long) config["socket-period"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "socket-period") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "socket-tries")) {
      try {
        if (config["socket-tries"] < 1) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "socket-tries") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else socket_tries = (unsigned long) config["socket-tries"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "socket-tries") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkString(&config, "lang")) lang = config["lang"];
    if (checkString(&config, "dbname")) dbname = config["dbname"];
    if (checkString(&config, "dbhost")) dbhost = config["dbhost"];
    if (checkInteger(&config, "dbport")) {
      try {
        if ( config["dbport"] < 0 || config["dbport"] > 65535 ) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "dbport") +
            " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else dbport = (int) config["dbport"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "dbport") +
          " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "max-players")) {
      try {
        if (config["max-players"] < 0) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "max-players") +
          " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else max_players = (unsigned long) config["max-players"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "max-players") +
        " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "max-games")) {
      try {
        if (config["max-players"] < 0) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "max-games") +
          " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else max_games = (unsigned long) config["max-games"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "max-games") +
        " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "chat-backlog-size")) {
      try {
        if (config["chat-backlog-size"] < 0) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "chat-backlog-size") +
          " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else chat_backlog_size = (unsigned long) config["chat-backlog-size"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "chat-backlog-size") +
        " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "creation-rate")) {
      try {
        if (config["creation-rate"] < 0) {
          str = replaceAllInstances(AdvServer::advGetText("value_out_of_range"), "${NAME}", "creation-rate") +
          " " + AdvServer::advGetText("option_not_changed");
          log(5, str);
        }
        else creation_rate = (unsigned long) config["creation-rate"];
      }
      catch (...) {
        str = replaceAllInstances(AdvServer::advGetText("invalid_value"), "${NAME}", "creation-rate") +
        " " + AdvServer::advGetText("option_not_changed");
        log(5, str);
      }
    }
    if (checkInteger(&config, "last-created")) {
      try {
        if (config["last-created"] > 0) last_created = (unsigned long) config["last-created"];
      } catch (...) {}
    }
    if (checkBoolean(&config, "enable-registration")) enable_registration = config["enable-registration"];
    if (checkBoolean(&config, "enable-unregistered")) enable_unregistered = config["enable-unregistered"];
    if (checkString(&config, "dbuser")) dbuser = config["dbuser"];
    if (checkString(&config, "dbpass")) dbpass = config["dbpass"];
    if (checkArray(&config, "admins")) admins = config["admins"]; else admins = nlohmann::json::parse("[]");
    if (checkArray(&config, "moderators")) moderators = config["moderators"]; else moderators = nlohmann::json::parse("[]");
    if (checkArray(&config, "motd")) motd = config["motd"]; else motd = nlohmann::json::parse("[]");
  }
}

void AdvServer::writeConfig()
{
  std::ofstream config_file;
  std::string config_file_path = static_directory + "/config.json";
  pthread_mutex_lock(&config_mutex);
  config_file.open(config_file_path.c_str());
  if (config_file.good()) {
    nlohmann::json config;
    config["lang"] = lang;
    config["log-level"] = Logger::logLevel(logger->getLogLevel(), true);
    config["port"] = port;
    config["delay"] = delay;
    config["sync-period"] = sync_period;
    config["socket-period"] = socket_period;
    config["socket-tries"] = socket_tries;
    config["max-players"] = max_players;
    config["max-games"] = max_games;
    config["chat-backlog-size"] = chat_backlog_size;
    config["creation-rate"] = creation_rate;
    config["last-created"] = last_created;
    config["enable-registration"] = enable_registration;
    config["enable-unregistered"] = enable_unregistered;
    config["dbname"] = dbname;
    config["dbhost"] = dbhost;
    config["dbport"] = dbport;
    config["dbuser"] = dbuser;
    config["dbpass"] = dbpass;
    config["admins"] = admins;
    config["moderators"] = moderators;
    config["motd"] = motd;
    config_file << std::setw(2) << config << std::endl;
    config_file.close();
  }
  pthread_mutex_unlock(&config_mutex);
}

void AdvServer::readChatBacklog()
{
  std::ifstream chat_backlog_file;
  std::string chat_backlog_file_path = static_directory + "/chat.json";
  chat_backlog_file.open(chat_backlog_file_path.c_str());
  if (chat_backlog_file.good()) {
    try {
      chat_backlog_file >> chat_backlog;
      while (chat_backlog.size() > chat_backlog_size) chat_backlog.erase(0);
    } catch (...) {}
  }
}

void AdvServer::writeChatBacklog()
{
  std::ofstream chat_backlog_file;
  std::string chat_backlog_file_path = working_directory + "/chat.json";
  chat_backlog_file.open(chat_backlog_file_path.c_str());
  if (chat_backlog_file.good()) {
    chat_backlog_file << std::setw(2) << chat_backlog << std::endl;
    chat_backlog_file.close();
  }
}

// Load text strings in the current language
void AdvServer::loadStrings()
{
  std::ifstream strings_file;
  nlohmann::json new_strings;
  strings_file.open(std::string(PREFIX) + "/share/adv/strings/server/en.json");
  if (strings_file.good()) {
    try {
      strings_file >> AdvServer::strings;
    }
    catch (...) {}
    strings_file.close();
  }
  strings_file.open(std::string(PREFIX) + "/share/adv/strings/server/" + lang + ".json");
  if (strings_file.good()) {
    try {
      strings_file >> new_strings;
      log(7, replaceAllInstances(advGetText("loading_language"), "${LANG}", lang));
      for (auto& x : nlohmann::json::iterator_wrapper(new_strings))
        AdvServer::strings[x.key()] = x.value();
    }
    catch (...) {}
    strings_file.close();
  }
}

// Sync the server running directory to the static working directory at startup using rsync
void AdvServer::initial_sync()
{
  log(6, AdvServer::advGetText("syncing_adv_static_directory"));
  std::string command = "rsync -a --exclude pid --delete --exclude trash " + rsync_excludes + static_directory + "/ " + working_directory + "/";
  system(command.c_str());
}

// Sync the server static directory to the running working directory using rsync
void AdvServer::sync(std::string origin)
{
  pthread_mutex_lock(&sync_mutex);
  if (!last_sync) origin = "admin";
  log(6, replaceAllInstances(advGetText("syncing_adv_working_directory"),
    "${REASON}", advGetText(std::string("sync_reason_") + origin)));
  std::string command = std::string("rsync -a ") + rsync_excludes + " --exclude pid --exclude trash --delete "
    + working_directory + "/ " + static_directory + "/";
  startReadingGames();
  for (std::list<AdvGame>::reverse_iterator game = games.rbegin(); game != games.rend(); ++game)
    game->lock();
  system(command.c_str());
  nlohmann::json j;
  j["class"] = "admin";
  j["event"] = "sync";
  j["response"] = "success";
  startReadingConnections();
  startReadingHandles();
  for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
    if (isAdmin((*it)->handle)) (*it)->sendEvent(j.dump());
  stopReadingConnections();
  stopReadingHandles();
  j.clear();
  j["origin"] = origin;
  j["arguments"] = sync_arguments;
  sync_arguments.clear();
  runHook("sync", j.dump());
  readConfig();
  writeChatBacklog();
  updateCache();
  for (std::list<AdvGame>::reverse_iterator game = games.rbegin(); game != games.rend(); ++game) {
    game->updateDatabaseConfig(dbname, dbhost, dbport, dbuser, dbpass);
    game->unlock();
  }
  stopReadingGames();
  j.clear();
  j["class"] = "admin";
  j["event"] = "just_synced";
  j["response"] = "success";
  startReadingConnections();
  startReadingHandles();
  for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
    if (isAdmin((*it)->handle)) (*it)->sendEvent(j.dump());
  stopReadingConnections();
  stopReadingHandles();
  last_sync = time(0);
  pthread_mutex_unlock(&sync_mutex);
}

bool AdvServer::isAdmin(std::string username)
{
  for (int i = 0; i < admins.size(); ++i)
    if (admins[i].get<std::string>() == username) return true;
  return false;
}

bool AdvServer::isModerator(std::string username)
{
  if (isAdmin(username)) return true;
  for (int i = 0; i < moderators.size(); ++i)
    if (moderators[i].get<std::string>() == username) return true;
  return false;
}

bool AdvServer::isRole(std::string role)
{
  if (role == "admin" || role == "moderator") return true;
  return false;
}

// Read the players file
void AdvServer::readPlayersFile()
{
  log(7, AdvServer::advGetText("reading_players_file"));
  std::ifstream players_file;
  players_file.open(std::string(static_directory + "/players.json").c_str());
  if (players_file.good()) {
    players_file >> players;
    players_file.close();
  }
}

// Write the players file to make it persistent
void AdvServer::writePlayersFile()
{
  log(7, AdvServer::advGetText("writing_players_file"));
  std::ofstream players_file;
  players_file.open(std::string(static_directory + "/players.json").c_str());
  if (players_file.good()) {
    players_file << std::setw(2) << players << std::endl;
    players_file.close();
  }
}

// Load all existing games
void AdvServer::loadGames()
{
  log(7, AdvServer::advGetText("loading_existing_games"));
  DIR *dir;
  struct dirent *entry;
  std::list<std::string> game_directories;
  game_directories.push_back(working_directory);
  while (!game_directories.empty()) {
    if ((dir = opendir(std::string(game_directories.front() + "/games").c_str())) != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
        log(7, replaceAllInstances(AdvServer::advGetText("loading_game"), "${NAME}", entry->d_name));
        games.push_back(AdvGame("", std::string(game_directories.front() + "/games/" + entry->d_name)));
        games.back().log.connect(this, &AdvServer::log);
        games.back().sendMessage.connect(this, &AdvServer::onSendMessage);
        games.back().kick.connect(this, &AdvServer::onKick);
        games.back().sendCall.connect(this, &AdvServer::onModApiCall);
        game_directories.push_back(std::string(game_directories.front() + "/games/" + entry->d_name));
      }
      closedir(dir);
      game_directories.pop_front();
    }
  }
}

void AdvServer::destroyGame(std::list<AdvGame>::iterator game)
{
  std::list<AdvGame>::reverse_iterator g;
  std::string game_directory, trash;
  nlohmann::json j;
  bool is_child_game;
  bool is_this_game = false;
  size_t n;
  writeGames();
  writeIngames();
  startReadingHandles();
  for (g = games.rbegin(); g != games.rend();) {
    is_child_game = (g->getName().substr(0, game->getName().length() + 1) == std::string(game->getName() + "_"));
    is_this_game = (g->getName() == game->getName());
    if (is_child_game || is_this_game) {
      log(5, replaceAllInstances(AdvServer::advGetText("destroying_game"), "${GAME}", g->getName()));
      startReadingConnections();
      for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
        if ((*it)->game == g->getName()) {
          j.clear();
          j["name"] = g->getName();
          j["username"] = (*it)->handle;
          runHook("leave", j.dump());
          n = g->getName().rfind("_");
          if (n != std::string::npos)
            (*it)->game = g->getName().substr(0, n);
          else {
            (*it)->game.clear();
            j.erase("username");
            j["class"] = "leave";
            j["response"] = "success";
            (*it)->sendEvent(j.dump());
          }
        }
      stopReadingConnections();
      g->destroy();
      game_directory = gameDirectoryFromName(g->getName());
      trash = working_directory + "/trash/" + g->getName() + "_" + std::to_string(time(0));
      g = decltype(g)(games.erase(std::next(g).base()));
      rename(game_directory.c_str(), trash.c_str());
      if (AdvServer::rmdirrecursively(trash))
        log(3, replaceAllInstances(AdvServer::advGetText("failed_removing_game_directory"), "${DIR}", trash));
    }
    else
      ++g;
    if (is_this_game) break;
  }
  stopReadingHandles();
  pthread_mutex_unlock(&ingames_mutex);
  pthread_mutex_unlock(&games_mutex);
}

// If game is empty or not provided, then leave from the parent game
void AdvServer::leaveFromGame(std::list<AdvPlayerConnection*>::iterator conn, std::string game)
{
  nlohmann::json response;
  bool done = false;
  startReadingHandles();
  startReadingGames();
  writeIngames();
  std::list<AdvGame>::reverse_iterator g = games.rbegin();
  while (!(*conn)->game.empty()) {
    size_t n;
    for (; g != games.rend(); ++g)
      if (g->getName() == (*conn)->game) break;
    if (g == games.rend()) break;
    response["name"] = (*conn)->game;
    runHook("leave", response.dump());
    if (!(*conn)->handle.empty()) g->playerLeft((*conn)->handle);
    if ((*conn)->game == game) done = true;
    if ((n = (*conn)->game.rfind("_")) != std::string::npos)
      (*conn)->game = (*conn)->game.substr(0, n);
    else {
      response.erase("username");
      response["class"] = "leave";
      response["response"] = "success";
      (*conn)->sendEvent(response.dump());
      (*conn)->game.clear();
    }
    if (done) break;
  }
  pthread_mutex_unlock(&ingames_mutex);
  stopReadingGames();
  stopReadingHandles();
}

void AdvServer::logoutFromServer(std::list<AdvPlayerConnection*>::iterator conn, bool banned, time_t wait)
{
  startReadingHandles();
  if ((*conn)->handle.empty()) {
    stopReadingHandles();
    return;
  }
  leaveFromGame(conn);
  nlohmann::json response, event;
  event["class"] = "admin";
  event["response"] = "success";
  event["event"] = "logout";
  event["username"] = (*conn)->handle;
  response["username"] = (*conn)->handle;
  startReadingConnections();
  for (std::list<AdvPlayerConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
    if (isModerator((*it)->handle) && it != conn) (*it)->sendEvent(event.dump());
  stopReadingConnections();
  stopReadingHandles();
  writeHandles();
  (*conn)->handle.clear();
  pthread_mutex_unlock(&handles_mutex);
  runHook("logout", response.dump());
  response["class"] = "logout";
  if (banned) {
    if (!wait)
      response["reason"] = "banned";
    else {
      response["reason"] = "kicked";
      response["wait"] = wait;
    }
  }
  else {
    response["reason"] = "logout";
  }
  (*conn)->sendEvent(response.dump());
}

void AdvServer::banPlayer(std::string handle, time_t wait)
{
  time_t seconds = 0;
  if (wait) seconds = time(0) + wait;
  std::list<AdvPlayerConnection*>::iterator it;
  unsigned int p;
  pthread_mutex_lock(&accounts_mutex);
  for (p = 0; p < players.size(); ++p) if (handle == players[p]["username"]) break;
  if (p < players.size()) players[p]["banned"] = seconds;
  writePlayersFile();
  startReadingConnections();
  startReadingHandles();
  for (it = connections.begin(); it != connections.end(); ++it)
    if ((*it)->handle == handle) break;
  stopReadingHandles();
  if (it != connections.end()) logoutFromServer(it, true, wait);
  stopReadingConnections();
  pthread_mutex_unlock(&accounts_mutex);
}

void AdvServer::runHook(std::string hook, std::string input)
{
  if (!hasParameter(&cache["hooks"], hook)) return;
  pthread_mutex_lock(&server_hooks_mutex);
  std::string cmd = static_directory + "/hooks/" + hook + " " + working_directory;
  log(7, replaceAllInstances(AdvServer::advGetText("running_server_hook"), "${HOOK}", hook));
  std::ofstream input_file;
  input_file.open(std::string(working_directory + "/input.json").c_str());
  if (input_file.good()) {
    input_file << std::setw(2) << input;
    input_file.close();
  }
  system(cmd.c_str());
  unlink(std::string(working_directory + "/input.json").c_str());
  pthread_mutex_unlock(&server_hooks_mutex);
}

bool AdvServer::checkGameMods(nlohmann::json mods, bool branch, bool are_libs)
{
  if (!are_libs && mods.empty()) return false;
  nlohmann::json json;
  for (int i = 0; i < mods.size(); ++i) {
    if (!hasParameter(&cache["mods"], mods[i].get<std::string>())) return false;
    json = cache["mods"][mods[i].get<std::string>()]["properties"];
    if (checkArray(&json, "libs"))
      for (int l = 0; l < json["libs"].size(); ++l)
        if (!hasParameter(&cache["mods"], json["libs"][l].get<std::string>())) return false;
    if (are_libs) continue;
    if ( !checkString(&json, "type") || (json["type"] != "branch" && json["type"] != "game") ||
      (json["type"] != "game" && !branch) ) return false;
  }
  return true;
}

nlohmann::json AdvServer::resolveModDependencies(nlohmann::json mod_deps)
{
  nlohmann::json json, checked_mods;
  nlohmann::json tmp_deps = nlohmann::json::parse("[]");
  // If previous iteration didn't change the dependencies array, then we're set
  while (tmp_deps.size() != mod_deps.size()) {
    tmp_deps = mod_deps;
    mod_deps = nlohmann::json::parse("[]");
    for (int i = 0; i < tmp_deps.size(); ++i) {
      // If this mod is marked, just add it and move forward
      if (hasParameter(&checked_mods, tmp_deps[i].get<std::string>())) {
        mod_deps.push_back(tmp_deps[i]);
        continue;
      }
      // If not, mark it and add its dependencies to its left
      checked_mods[tmp_deps[i].get<std::string>()] = true;
      if (hasParameter(&cache["mods"], tmp_deps[i].get<std::string>())) {
        json = cache["mods"][tmp_deps[i].get<std::string>()]["properties"];
      }
      else {
        mod_deps.push_back(tmp_deps[i]);
        continue;
      }
      if (!checkArray(&json, "depends")) {
        mod_deps.push_back(tmp_deps[i]);
        continue;
      }
      for (int j = 0; j < json["depends"].size(); ++j) mod_deps.push_back(json["depends"][j]);
      mod_deps.push_back(tmp_deps[i]);
    }
    // Remove duplicates
    for (int i = 0; i < mod_deps.size(); ++i)
      for (int j = i + 1; j < mod_deps.size(); ++j)
        if (mod_deps[i].get<std::string>() == mod_deps[j].get<std::string>()) {
          mod_deps.erase(mod_deps.begin() + j);
          --j;
        }
  }
  return tmp_deps;
}

nlohmann::json AdvServer::resolveModLibs(nlohmann::json mods, nlohmann::json branch_libs)
{
  nlohmann::json json, libs;
  for (int i = 0; i < mods.size(); ++i) {
    if (!hasParameter(&cache["mods"], mods[i].get<std::string>())) continue;
    json = cache["mods"][mods[i].get<std::string>()]["properties"];
    if (checkArray(&json, "libs"))
      for (int l = 0; l < json["libs"].size(); ++l)
        libs["libs"][json["libs"][l].get<std::string>()] = true;
  }
  for (int l = 0; l < branch_libs.size(); ++l)
    libs["libs"][branch_libs[l].get<std::string>()] = true;
  libs["array"] = nlohmann::json::parse("[]");
  for (auto& x: nlohmann::json::iterator_wrapper(libs["libs"]))
    libs["array"].push_back(x.key());
  return libs["array"];
}

std::string AdvServer::gameDirectoryFromName(std::string full_name)
{
  return working_directory + "/games/" + replaceAllInstances(full_name, "_", "/games/");
}

bool AdvServer::checkNameSanity(std::string name)
{
  if (name.empty() || name.length() > 30) return false; // Size restriction
  if (48 <= name[0] && name[0] <= 57) return false; // Cannot begin with a digit
  for (unsigned short i = 0; i < name.length(); ++i) { // Only valid characters are a-z and 0-9
    if (name[i] <= 47) return false;
    if (58 <= name[i] && name[i] <= 96) return false;
    if (name[i] >= 123) return false;
  }
  return true;
}

bool AdvServer::checkUserNameSanity(std::string name)
{
  if (name.empty() || name.length() > 30) return false; // Size restriction
  for (unsigned short i = 0; i < name.length(); ++i) { // Invalid characters
    if (name[i] <= 47) return false; // Spaces, control characters and basic symbols
    if (58 <= name[i] && name[i] <= 94) return false; // Basic symbols excluding _
    if (name[i] == 96) return false; // ` symbol
    if (123 <= name[i] && name[i] <= 127) return false; // Basic symbols
  }
  return true;
}

std::string AdvServer::advGetText(std::string label)
{
  if (!checkString(&AdvServer::strings, label))
    return label;
  else
    return AdvServer::strings[label].get<std::string>();
}

std::string AdvServer::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

int AdvServer::mkdirrecursively(mode_t mode, std::string root_path, std::string path)
{
  struct stat st;
  int new_i;
  std::string new_path;
  path = "/" + path;
  for (int i = 0; i < path.length(); ++i) {
    new_i = path.find_first_of('/', i);
    if (new_i == -1) new_i = path.length();
    new_path = root_path + "/" + std::string(path, 0, new_i);
    if (stat(new_path.c_str(), &st) != 0)
      if (mkdir(new_path.c_str(), mode) != 0 && errno != EEXIST) return errno;
    else
      if (!S_ISDIR(st.st_mode)) return ENOTDIR;
    i = new_i;
  }
  return 0;
}

int AdvServer::rmdirrecursively(std::string path)
{
  int ret = 0;
  FTS *ftsp = NULL;
  FTSENT *current;
  char *files[] = { (char *) path.c_str(), NULL };
  ftsp = fts_open(files, FTS_NOCHDIR | FTS_PHYSICAL | FTS_XDEV, NULL);
  if (!ftsp) return 1; // Failed opening directory
  while (current = fts_read(ftsp)) {
    switch (current->fts_info) {
      case FTS_NS: case FTS_DNR: case FTS_ERR: // fts_read error
        break;
      case FTS_DC: case FTS_DOT: case FTS_NSOK:
        // Not reached unless FTS_LOGICAL, FTS_SEEDOT, or FTS_NOSTAT were passed to fts_open()
        break;
      case FTS_D:
        // Do nothing. Need depth-first search, so directories are deleted in FTS_DP
        break;
      case FTS_DP: case FTS_F: case FTS_SL: case FTS_SLNONE: case FTS_DEFAULT:
        if (remove(current->fts_accpath) < 0) ret = 2; // Failed to remove file
        break;
    }
  }
  if (ftsp) fts_close(ftsp);
  return ret;
}

/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvServer::sendFailureResponse(std::list<AdvPlayerConnection*>::iterator it,
                                    nlohmann::json response, std::string reason, nlohmann::json content)
{
  response["response"] = "failure";
  response["reason"] = reason;
  response["content"] = content;
  (*it)->sendEvent(response.dump());
}

void AdvServer::handleAccountRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  nlohmann::json response;
  if (message["class"] == "login") { // Login request
    response["class"] = "login";
    startReadingHandles();
    if (!(*it)->handle.empty()) { // Already logged in
      stopReadingHandles();
      sendFailureResponse(it, response, "already_logged_in", message);
      return;
    }
    stopReadingHandles();
    // Check username
    if (!checkString(&message, "username") || message["username"].get<std::string>().empty()) {
      sendFailureResponse(it, response, "username_not_provided", message);
      return;
    }
    if (!checkUserNameSanity(message["username"].get<std::string>())) {
      sendFailureResponse(it, response, "invalid_username", message);
      return;
    }
    int p;
    pthread_mutex_lock(&accounts_mutex);
    for (p = 0; p < players.size(); ++p)
      if (message["username"] == players[p]["username"]) break;
    // Check if the account already exists
    if (p < players.size()) {
      if (!checkString(&message, "password")) {
        pthread_mutex_unlock(&accounts_mutex);
        sendFailureResponse(it, response, "already_registered_username", message);
        return;
      }
      // Verify password
      md5wrapper md5;
      std::string password = md5.getHashFromString(message["password"]);
      if (password != players[p]["password"]) {
        pthread_mutex_unlock(&accounts_mutex);
        sendFailureResponse(it, response, "incorrect_password", message);
        return;
      }
      if (checkNumber(&players[p], "banned")) {
        time_t now = time(0);
        if ((time_t) players[p]["banned"] == 0) {
          pthread_mutex_unlock(&accounts_mutex);
          sendFailureResponse(it, response, "banned", message);
          return;
        }
        else if (players[p]["banned"] > now) {
          pthread_mutex_unlock(&accounts_mutex);
          response["wait"] = (time_t) players[p]["banned"] - now;
          sendFailureResponse(it, response, "kicked", message);
          return;
        }
        else {
          players[p].erase("banned");
          writePlayersFile();
        }
      }
    }
    else {
      if (!enable_unregistered) {
        pthread_mutex_unlock(&accounts_mutex);
        sendFailureResponse(it, response, "unregistered_disabled", message);
        return;
      }
    }
    pthread_mutex_unlock(&accounts_mutex);
    std::list<AdvPlayerConnection*>::iterator conn;
    // Get number of connected players
    int n = 0;
    startReadingHandles();
    for (conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) ++n;
    for (conn = connections.begin(); conn != connections.end(); ++conn)
      if ((*conn)->handle == message["username"]) break;
    stopReadingHandles();
    if (conn != connections.end()) { // Already connected player. Logout from the old connection
      logoutFromServer(conn);
    }
    else {
      if (max_players > 0 && n >= max_players) {
        sendFailureResponse(it, response, "max_players_reached", message);
        return;
      }
    }
    // Send success response
    response["username"] = message["username"];
    response.erase("class");
    runHook("login", response.dump());
    response["class"] = "login";
    response["response"] = "success";
    response["connected"] = (unsigned long) n;
    response["version"] = PROGRAMVERSION;
    response["uptime"] = (unsigned long) (time(0) - uptime);
    response["motd"] = motd;
    (*it)->sendEvent(response.dump());
    writeHandles();
    (*it)->handle = message["username"];
    response.clear();
    response["class"] = "admin";
    response["response"] = "success";
    response["event"] = "login";
    response["username"] = message["username"];
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (isModerator((*conn)->handle) && (*conn)->handle != (*it)->handle) (*conn)->sendEvent(response.dump());
    pthread_mutex_unlock(&handles_mutex);
    writeIngames();
    (*it)->game.clear();
    pthread_mutex_unlock(&ingames_mutex);
    if (admins.empty()) {
      admins.push_back(message["username"]);
      writeConfig();
      response["event"] = "you_are_the_master";
      (*it)->sendEvent(response.dump());
    }
  }
  else if (message["class"] == "logout") { // Logout request
    logoutFromServer(it);
  }
  else if (message["class"] == "register") { // Registration request
    response["class"] = "register";
    startReadingHandles();
    if (!(*it)->handle.empty()) { // Already logged in
      stopReadingHandles();
      sendFailureResponse(it, response, "already_logged_in", message);
      return;
    }
    stopReadingHandles();
    if (!enable_registration) {
      sendFailureResponse(it, response, "registration_disabled", message);
      return;
    }
    // Check username
    if (!checkString(&message, "username") || message["username"].get<std::string>().empty()) {
      sendFailureResponse(it, response, "username_not_provided", message);
      return;
    }
    if (!checkUserNameSanity(message["username"].get<std::string>())) {
      sendFailureResponse(it, response, "invalid_username", message);
      return;
    }
    // Check password
    if (!checkString(&message, "password")) {
      sendFailureResponse(it, response, "password_not_provided", message);
      return;
    }
    int p;
    pthread_mutex_lock(&accounts_mutex);
    for (p = 0; p < players.size(); ++p)
      if (message["username"] == players[p]["username"]) break;
    // Check if the account already exists
    if (p < players.size()) {
      pthread_mutex_unlock(&accounts_mutex);
      sendFailureResponse(it, response, "already_registered_username", message);
      return;
    }
    // Store new player
    md5wrapper md5;
    std::string password = md5.getHashFromString(message["password"]);
    nlohmann::json player;
    player["username"] = message["username"];
    player["password"] = password;
    players.push_back(player);
    writePlayersFile();
    pthread_mutex_unlock(&accounts_mutex);
    std::list<AdvPlayerConnection*>::iterator conn;
    // Get number of connected players
    int n = 0;
    startReadingHandles();
    for (conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) ++n;
    for (conn = connections.begin(); conn != connections.end(); ++conn)
      if ((*conn)->handle == message["username"]) break;
    stopReadingHandles();
    if (conn != connections.end()) { // Already connected player. Logout from the old connection
      startReadingHandles();
      startReadingIngames();
      if (!(*conn)->game.empty()) {
        response["username"] = (*conn)->handle;
        response["name"] = (*conn)->game;
        runHook("leave", response.dump());
        response.erase("username");
        response["class"] = "leave";
        response["response"] = "success";
        (*conn)->sendEvent(response.dump());
        startReadingGames();
        for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game)
          if (game->getName() == (*conn)->game) {
            game->playerLeft((*conn)->handle);
            break;
          }
        stopReadingGames();
        response.clear();
      }
      stopReadingIngames();
      response["username"] = (*conn)->handle;
      stopReadingHandles();
      runHook("logout", response.dump());
      response["class"] = "logout";
      (*conn)->sendEvent(response.dump());
      response.clear();
      writeHandles();
      (*conn)->handle.clear();
      pthread_mutex_unlock(&handles_mutex);
      writeIngames();
      (*conn)->game.clear();
      pthread_mutex_unlock(&ingames_mutex);
    }
    else {
      if (max_players > 0 && n >= max_players) {
        sendFailureResponse(it, response, "max_players_reached", message);
        return;
      }
    }
    // Send success response
    response["class"] = "register";
    response["response"] = "success";
    response["username"] = message["username"];
    response["connected"] = (unsigned long) n;
    response["version"] = PROGRAMVERSION;
    response["uptime"] = (unsigned long) (time(0) - uptime);
    response["motd"] = motd;
    (*it)->sendEvent(response.dump());
    writeHandles();
    (*it)->handle = message["username"];
    response.clear();
    response["class"] = "admin";
    response["response"] = "success";
    response["event"] = "login";
    response["username"] = message["username"];
    for (conn = connections.begin(); conn != connections.end(); ++conn)
      if (isModerator((*conn)->handle) && (*conn)->handle != (*it)->handle) (*conn)->sendEvent(response.dump());
    pthread_mutex_unlock(&handles_mutex);
    writeIngames();
    (*it)->game.clear();
    pthread_mutex_unlock(&ingames_mutex);
    if (admins.empty()) {
      admins.push_back(message["username"]);
      writeConfig();
      response["event"] = "you_are_the_master";
      (*it)->sendEvent(response.dump());
    }
  }
  else if (message["class"] == "password") { // Password change request
    response["class"] = "password";
    startReadingHandles();
    if ((*it)->handle.empty()) { // Not logged in
      stopReadingHandles();
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check password
    if (!checkString(&message, "password")) {
      stopReadingHandles();
      sendFailureResponse(it, response, "password_not_provided", message);
      return;
    }
    int p;
    md5wrapper md5;
    std::string password = md5.getHashFromString(message["password"]);
    pthread_mutex_lock(&accounts_mutex);
    for (p = 0; p < players.size(); ++p)
      if ((*it)->handle == players[p]["username"]) break;
    // Check if the account already exists
    if (p < players.size())
      players[p]["password"] = password;
    else { // Store new player
      if (!enable_registration) {
        stopReadingHandles();
        pthread_mutex_unlock(&accounts_mutex);
        sendFailureResponse(it, response, "registration_disabled", message);
        return;
      }
      nlohmann::json player;
      player["username"] = (*it)->handle;
      player["password"] = password;
      players.push_back(player);
    }
    writePlayersFile();
    pthread_mutex_unlock(&accounts_mutex);
    // Send success response
    response["username"] = (*it)->handle;
    stopReadingHandles();
    response["response"] = "success";
    (*it)->sendEvent(response.dump());
  }
}

void AdvServer::handleInfoRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  nlohmann::json response;
  if (message["class"] == "version") { // Version request
    response["class"] = "version";
    response["version"] = PROGRAMVERSION;
    (*it)->sendEvent(response.dump());
  }
  else if (message["class"] == "games") { // List games request
    response["class"] = "games";
    response["games"] = nlohmann::json::parse("[]");
    nlohmann::json game_json;
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game) {
      if (game->getName().find("_") != std::string::npos) continue;
      game_json["name"] = game->getName();
      game_json["created"] = game->getCreated();
      game_json["mods"] = game->getMods();
      game_json["title"] = game->getTitle();
      game_json["description"] = game->getDescription();
      response["games"].push_back(game_json);
    }
    stopReadingGames();
    startReadingIngames();
    response["game"] = (*it)->game.substr(0, (*it)->game.find("_"));
    stopReadingIngames();
    (*it)->sendEvent(response.dump());
  }
  else if (message["class"] == "mods") { // List mods request
    response["class"] = "mods";
    if (checkString(&message, "type")) response["type"] = message["type"];
    response["mods"] = nlohmann::json::parse("[]");
    for (auto& x: nlohmann::json::iterator_wrapper(cache["mods"])) {
      if ( hasParameter(&response, "type") && (!hasParameter(&x.value()["properties"], "type") || x.value()["properties"]["type"] != response["type"]) )
        continue;
      response["mods"].push_back(x.value()["properties"]);
    }
    (*it)->sendEvent(response.dump());
  }
  else if (message["class"] == "info") {
    response["class"] = "info";
    unsigned long n = 0;
    startReadingHandles();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) ++n;
    stopReadingHandles();
    response["connected"] = (unsigned long) n;
    response["uptime"] = (unsigned long) (time(0) - uptime);
    response["motd"] = motd;
    (*it)->sendEvent(response.dump());
  }
}

void AdvServer::handleGameManagementRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  std::string handle;
  nlohmann::json response;
  if (message["class"] == "create") { // Create game request
    startReadingHandles();
    handle = (*it)->handle;
    stopReadingHandles();
    nlohmann::json json;
    response["class"] = "create";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    if (!checkString(&message, "name")) {
      sendFailureResponse(it, response, "game_name_not_provided", message);
      return;
    }
    if (!checkNameSanity(message["name"].get<std::string>())) {
      sendFailureResponse(it, response, "invalid_game_name", message);
      return;
    }
    // Check mods
    if (!checkArray(&message, "mods")) {
      sendFailureResponse(it, response, "no_mod_provided", message);
      return;
    }
    if (!checkGameMods(message["mods"])) {
      sendFailureResponse(it, response, "mods_must_be_games", message);
      return;
    }
    // Check that no game exists with the same name
    pthread_mutex_lock(&creation_mutex);
    for (std::list<game_birth_t>::iterator game = scheduled_creations.begin(); game != scheduled_creations.end(); ++game)
      if (game->full_name == message["name"].get<std::string>()) {
        pthread_mutex_unlock(&creation_mutex);
        sendFailureResponse(it, response, "game_name_already_exists", message);
        return;
      }
    startReadingGames();
    for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game)
      if (game->getName() == message["name"].get<std::string>()) {
        stopReadingGames();
        pthread_mutex_unlock(&creation_mutex);
        sendFailureResponse(it, response, "game_name_already_exists", message);
        return;
      }
    if (max_games) {
      unsigned int n = 0;
      for (std::list<AdvGame>::iterator game = games.begin(); game != games.end(); ++game)
        if (game->getName().find("_") == std::string::npos) ++n;
      if (n >= max_games) {
        stopReadingGames();
        pthread_mutex_unlock(&creation_mutex);
        sendFailureResponse(it, response, "max_games_limit", message);
        return;
      }
    }
    stopReadingGames();
    time_t now = time(0);
    if (creation_rate && (now - last_created < creation_rate)) {
      pthread_mutex_unlock(&creation_mutex);
      response["wait"] = (unsigned long) creation_rate + last_created - now;
      sendFailureResponse(it, response, "creation_rate_limit", message);
      return;
    }
    last_created = now;
    if (!checkString(&message, "title") || message["title"].get<std::string>().empty()) message["title"] = message["name"];
    message["mods"] = resolveModDependencies(message["mods"]);
    log(7, replaceAllInstances(AdvServer::advGetText("scheduling_game_creation"), "${GAME}", message["name"].get<std::string>()));
    scheduled_creations.push_back({message["name"].get<std::string>(), handle, message["mods"], message["title"].get<std::string>(), resolveModLibs(message["mods"])});
    pthread_mutex_unlock(&creation_mutex);
  }
  else if (message["class"] == "manage") { // Game management request
    startReadingHandles();
    handle = (*it)->handle;
    stopReadingHandles();
    response["class"] = "manage";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check mod name
    if (!checkString(&message, "mod")) {
      sendFailureResponse(it, response, "mod_name_not_provided", message);
      return;
    }
    startReadingIngames();
    // Check game name
    if ((*it)->game.empty()) { // Not in any game
      stopReadingIngames();
      sendFailureResponse(it, response, "not_in_any_game", message);
      return;
    }
    // Check that the game actually exists
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game)
      if (game->getName() == (*it)->game) break;
    stopReadingIngames();
    if (game == games.end()) {
      stopReadingGames();
      sendFailureResponse(it, response, "game_doesnt_exist", message);
      return;
    }
    // Check owner
    if (game->getOwner() != handle) {
      stopReadingGames();
      sendFailureResponse(it, response, "not_game_owner", message);
      return;
    }
    if (!checkArray(&message, "arguments")) message["arguments"] = nlohmann::json::parse("[]");
    game->manage(message["mod"].get<std::string>(), message["arguments"]);
    stopReadingGames();
  }
  else if (message["class"] == "destroy") { // Destroy game request
    startReadingHandles();
    handle = (*it)->handle;
    stopReadingHandles();
    response["class"] = "destroy";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    if (!checkString(&message, "name")) {
      sendFailureResponse(it, response, "game_name_not_provided", message);
      return;
    }
    // Check that the game actually exists
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game)
      if (game->getName() == message["name"].get<std::string>()) break;
    if (game == games.end()) {
      stopReadingGames();
      sendFailureResponse(it, response, "game_doesnt_exist", message);
      return;
    }
    if ((game->getOwner() != handle) && !isAdmin(handle)) {
      stopReadingGames();
      sendFailureResponse(it, response, "not_game_owner", message);
      return;
    }
    log(7, replaceAllInstances(AdvServer::advGetText("scheduling_game_destruction"), "${GAME}", message["name"].get<std::string>()));
    pthread_mutex_lock(&destruction_mutex);
    scheduled_destructions.push_back(game);
    pthread_mutex_unlock(&destruction_mutex);
    stopReadingGames();
    response["name"] = message["name"];
    response.erase("class");
    runHook("destroy", response.dump());
    response["class"] = "destroy";
    // Send success response
    response["response"] = "success";
    (*it)->sendEvent(response.dump());
  }
  else if (message["class"] == "transfer") { // Transfer game request
    startReadingHandles();
    handle = (*it)->handle;
    stopReadingHandles();
    response["class"] = "transfer";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    if (!checkString(&message, "name")) {
      sendFailureResponse(it, response, "game_name_not_provided", message);
      return;
    }
    // Check new owner
    if (!checkString(&message, "new-owner")) {
      sendFailureResponse(it, response, "new_owner_not_provided", message);
      return;
    }
    if (!checkUserNameSanity(message["new-owner"].get<std::string>())) {
      sendFailureResponse(it, response, "invalid_new_owner", message);
      return;
    }
    // Check that the game actually exists
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game)
      if (game->getName() == message["name"].get<std::string>()) break;
    if (game == games.end()) {
      stopReadingGames();
      sendFailureResponse(it, response, "game_doesnt_exist", message);
      return;
    }
    // Check owner
    if ((game->getOwner() != handle) && !isAdmin(handle)) {
      stopReadingGames();
      sendFailureResponse(it, response, "not_game_owner", message);
      return;
    }
    game->setOwner(message["new-owner"].get<std::string>());
    stopReadingGames();
    std::string log_message = replaceAllInstances(AdvServer::advGetText("transfering_game_to_player"),
      "${GAME}", message["name"].get<std::string>());
    log_message = replaceAllInstances(log_message, "${NEW_OWNER}", message["new-owner"].get<std::string>());
    log(5, log_message);
    // Send success response
    response["response"] = "success";
    response["name"] = message["name"];
    response["new-owner"] = message["new-owner"];
    (*it)->sendEvent(response.dump());
  }
}

void AdvServer::handlePlayRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  nlohmann::json response;
  startReadingHandles();
  std::string handle = (*it)->handle;
  stopReadingHandles();
  if (message["class"] == "enter") { // Enter game request
    response["class"] = "enter";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    if (!checkString(&message, "name")) {
      sendFailureResponse(it, response, "game_name_not_provided", message);
      return;
    }
    if (!checkNameSanity(message["name"].get<std::string>())) {
      sendFailureResponse(it, response, "invalid_game_name", message);
      return;
    }
    startReadingIngames();
    if (message["name"].get<std::string>() == (*it)->game.substr(0, (*it)->game.find("_"))) {
      stopReadingIngames();
      sendFailureResponse(it, response, "already_in_that_game", message);
      return;
    }
    if (!(*it)->game.empty()) { // Already in another game
      stopReadingIngames();
      sendFailureResponse(it, response, "already_in_another_game", message);
      return;
    }
    stopReadingIngames();
    // Check that the game actually exists
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game)
      if (game->getName() == message["name"].get<std::string>()) break;
    if (game == games.end()) { // Not existent game
      stopReadingGames();
      sendFailureResponse(it, response, "game_doesnt_exist", message);
      return;
    }
    writeIngames();
    (*it)->game = message["name"];
    pthread_mutex_unlock(&ingames_mutex);
    response["name"] = message["name"];
    response["username"] = handle;
    response.erase("class");
    runHook("enter", response.dump());
    response["class"] = "enter";
    response.erase("username");
    // Send success response
    response["response"] = "success";
    (*it)->sendEvent(response.dump());
    if (checkString(&message, "lang"))
      game->playerEntered(handle, message["lang"]);
    else
      game->playerEntered(handle, lang);
    stopReadingGames();
  }
  else if (message["class"] == "leave") { // Leave game request
    response["class"] = "leave";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    startReadingIngames();
    if ((*it)->game.empty()) { // Not in any game
      stopReadingIngames();
      sendFailureResponse(it, response, "not_in_any_game", message);
      return;
    }
    stopReadingIngames();
    leaveFromGame(it);
  }
  else if (message["class"] == "action") { // Game action request
    response["class"] = "action";
    if (handle.empty()) { // Not logged in
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    // Check game name
    startReadingIngames();
    if ((*it)->game.empty()) { // Not in any game
      stopReadingIngames();
      sendFailureResponse(it, response, "not_in_any_game", message);
      return;
    }
    if (!hasParameter(&message, "action")) {
      stopReadingIngames();
      sendFailureResponse(it, response, "no_action_provided", message);
      return;
    }
    std::list<AdvGame>::iterator game;
    startReadingGames();
    for (game = games.begin(); game != games.end(); ++game)
      if (game->getName() == (*it)->game) break;
    if ( (game != games.end()) && game->isPaused() ) {
      stopReadingIngames();
      response["pause-message"] = game->getPauseMessage();
      stopReadingGames();
      sendFailureResponse(it, response, "game_paused", message);
      return;
    }
    stopReadingIngames();
    nlohmann::json action;
    action["action"] = message["action"];
    if (checkArray(&message, "text")) action["text"] = message["text"];
    else
      action["text"] = nlohmann::json::parse("[]");
    if (game != games.end()) game->action(handle, action);
    stopReadingGames();
  }
}

void AdvServer::handleChatRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  nlohmann::json response;
  startReadingHandles();
  if (message["class"] == "chat") { // Chat request
    if ((*it)->handle.empty()) { // Not logged in
      stopReadingHandles();
      response["class"] = "chat";
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    response["username"] = (*it)->handle;
    response["content"] = message["content"];
    runHook("chat", response.dump());
    response["class"] = "chat";
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) (*conn)->sendEvent(response.dump());
    response["timestamp"] = (unsigned long) time(0);
    chat_backlog.push_back(response);
    while (chat_backlog.size() > chat_backlog_size) chat_backlog.erase(0);
  }
  else if (message["class"] == "roll") { // Request to roll dice
    response["class"] = "roll";
    if ((*it)->handle.empty()) { // Not logged in
      stopReadingHandles();
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    if (!checkNumber(&message, "ndice") || message["ndice"] < 1) message["ndice"] = 1;
    if (!hasParameter(&message, "nfaces") || message["nfaces"] < 2) message["nfaces"] = 6;
    response["username"] = (*it)->handle;
    response["ndice"] = message["ndice"];
    response["nfaces"] = message["nfaces"];
    response["values"] = nlohmann::json::parse("[]");
    for (unsigned int i = 0; i < (unsigned int) message["ndice"]; ++i)
      response["values"].push_back((unsigned int) (1 + rand() % (unsigned int) message["nfaces"]));
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) (*conn)->sendEvent(response.dump());
    response["timestamp"] = (unsigned long) time(0);
    chat_backlog.push_back(response);
    while (chat_backlog.size() > chat_backlog_size) chat_backlog.erase(0);
    response.erase("class");
    response.erase("timestamp");
    runHook("roll", response.dump());
  }
  else if (message["class"] == "chat-backlog") { // Chat backlog request
    if ((*it)->handle.empty()) { // Not logged in
      stopReadingHandles();
      response["class"] = "chat-backlog";
      sendFailureResponse(it, response, "not_logged_in", message);
      return;
    }
    response["class"] = "chat-backlog";
    response["content"] = chat_backlog;
    response["size"] = chat_backlog.size();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) (*conn)->sendEvent(response.dump());
  }
  stopReadingHandles();
}

void AdvServer::handleAdminRequest(std::list<AdvPlayerConnection*>::iterator it, nlohmann::json message)
{
  nlohmann::json failure, success;
  bool admin_event = true;
  failure["class"] = "admin";
  startReadingHandles();
  if ((*it)->handle.empty()) { // Not logged in
    stopReadingHandles();
    sendFailureResponse(it, failure, "not_logged_in", message);
    return;
  }
  if (checkString(&message, "command") &&
    ( (message["command"] == "send") || (message["command"] == "kick") || (message["command"] == "ban") ||
      (message["command"] == "registered") || (message["command"] == "players") ) ) {
    if (!isModerator((*it)->handle)) {
      stopReadingHandles();
      sendFailureResponse(it, failure, "not_a_server_moderator", message);
      return;
    }
  }
  else if (!isAdmin((*it)->handle)) {
    stopReadingHandles();
    sendFailureResponse(it, failure, "not_a_server_admin", message);
    return;
  }
  stopReadingHandles();
  if (!checkString(&message, "command") || message["command"].get<std::string>().empty()) {
    success["class"] = "admin";
    success["event"] = "yes_sir";
    success["response"] = "success";
    (*it)->sendEvent(success.dump());
    return;
  }
  if (!hasParameter(&client_api["admin"], message["command"].get<std::string>())) {
    sendFailureResponse(it, failure, "bad_admin_command", message);
    return;
  }
  if (message["command"] == "set") {
    if (!checkString(&message, "option")) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    success["option"] = message["option"];
    if (!checkString(&message, "value")) admin_event = false;
    if (message["option"] == "lang") {
      if (admin_event) {
        lang = message["value"].get<std::string>();
        writeConfig();
        readLang();
        loadStrings();
      }
      success["value"] = lang;
    }
    else if (message["option"] == "log-level") {
      if (admin_event) {
        int log_level;
        for (log_level = 0; log_level < 8; ++log_level)
          if (Logger::logLevel(log_level, true) == message["value"].get<std::string>()) {
            logger->setLogLevel(log_level);
            break;
          }
        if (log_level == 8) {
          try {
            log_level = std::stoi(message["value"].get<std::string>());
            logger->setLogLevel(log_level);
          }
          catch (...) {
            sendFailureResponse(it, failure, "bad_admin_command", message);
            return;
          }
        }
        writeConfig();
      }
      success["value"] = Logger::logLevel(logger->getLogLevel(), true);
    }
    else if (message["option"] == "delay") {
      if (admin_event) {
        long new_delay;
        try {
          new_delay = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_delay < 1) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        delay = new_delay;
        AdvPlayerConnection::setDelay(delay);
        writeConfig();
      }
      success["value"] = delay;
    }
    else if (message["option"] == "sync-period") {
      if (admin_event) {
        long new_sync_period;
        try {
          new_sync_period = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_sync_period < 1) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        sync_period = new_sync_period;
        writeConfig();
      }
      success["value"] = sync_period;
    }
    else if (message["option"] == "max-players") {
      if (admin_event) {
        long new_max_players;
        try {
          new_max_players = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_max_players < 0) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        max_players = new_max_players;
        writeConfig();
      }
      success["value"] = max_players;
    }
    else if (message["option"] == "max-games") {
      if (admin_event) {
        long new_max_games;
        try {
          new_max_games = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_max_games < 0) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        max_games = new_max_games;
        writeConfig();
      }
      success["value"] = max_games;
    }
    else if (message["option"] == "chat-backlog-size") {
      if (admin_event) {
        long new_chat_backlog_size;
        try {
          new_chat_backlog_size = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_chat_backlog_size < 0) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        chat_backlog_size = new_chat_backlog_size;
        writeConfig();
        while (chat_backlog.size() > chat_backlog_size) chat_backlog.erase(0);
      }
      success["value"] = chat_backlog_size;
    }
    else if (message["option"] == "creation-rate") {
      if (admin_event) {
        long new_creation_rate;
        try {
          new_creation_rate = (long) std::stoi(message["value"].get<std::string>());
        }
        catch (...) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        if (new_creation_rate < 0) {
          sendFailureResponse(it, failure, "bad_admin_command", message);
          return;
        }
        creation_rate = new_creation_rate;
        writeConfig();
      }
      success["value"] = creation_rate;
    }
    else if (message["option"] == "motd") {
      if (checkArray(&message, "value")) {
        motd = message["value"];
        writeConfig();
      }
      success["value"] = motd;
    }
    else {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
  }
  else if (message["command"] == "assign-role") {
    if (!checkString(&message, "role") || !isRole(message["role"].get<std::string>()) ||
      !checkString(&message, "username") || !checkUserNameSanity(message["username"])) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    success["role"] = message["role"];
    success["username"] = message["username"];
    unsigned int i;
    if (message["role"] == "admin") {
      pthread_mutex_lock(&config_mutex);
      for (i = 0; i < admins.size(); ++i)
        if (admins[i] == message["username"]) break;
      if (i == admins.size()) {
        admins.push_back(message["username"]);
        pthread_mutex_unlock(&config_mutex);
        writeConfig();
      }
      else
        pthread_mutex_unlock(&config_mutex);
    }
    else if (message["role"] == "moderator") {
      pthread_mutex_lock(&config_mutex);
      for (i = 0; i < moderators.size(); ++i)
        if (moderators[i] == message["username"]) break;
      if (i == moderators.size()) {
        moderators.push_back(message["username"]);
        pthread_mutex_unlock(&config_mutex);
        writeConfig();
      }
      else
        pthread_mutex_unlock(&config_mutex);
    }
    nlohmann::json j;
    j["class"] = "admin";
    j["response"] = "success";
    j["event"] = "assign-role";
    j["role"] = message["role"];
    startReadingHandles();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if ((*conn)->handle == message["username"].get<std::string>()) {
        (*conn)->sendEvent(j.dump());
        break;
      }
    stopReadingHandles();
  }
  else if (message["command"] == "revoke-role") {
    if (!checkString(&message, "role") || !isRole(message["role"].get<std::string>()) ||
      !checkString(&message, "username") || !checkUserNameSanity(message["username"])) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    success["role"] = message["role"];
    success["username"] = message["username"];
    unsigned int i;
    if (message["role"] == "admin") {
      pthread_mutex_lock(&config_mutex);
      for (i = 0; i < admins.size(); ++i)
        if (admins[i] == message["username"]) {
          admins.erase(i);
          pthread_mutex_unlock(&config_mutex);
          writeConfig();
          break;
        }
      if (i == admins.size()) pthread_mutex_unlock(&config_mutex);
    }
    else if (message["role"] == "moderator") {
      pthread_mutex_lock(&config_mutex);
      for (i = 0; i < moderators.size(); ++i)
        if (moderators[i] == message["username"]) {
          moderators.erase(i);
          pthread_mutex_unlock(&config_mutex);
          writeConfig();
          break;
        }
      if (i == moderators.size()) pthread_mutex_unlock(&config_mutex);
    }
  }
  else if (message["command"] == "enable-registration") {
    enable_registration = true;
    writeConfig();
  }
  else if (message["command"] == "disable-registration") {
    enable_registration = false;
    writeConfig();
  }
  else if (message["command"] == "enable-unregistered") {
    enable_unregistered = true;
    writeConfig();
  }
  else if (message["command"] == "disable-unregistered") {
    enable_unregistered = false;
    writeConfig();
  }
  else if (message["command"] == "send") {
    if (!checkString(&message, "username") || !checkString(&message, "message")) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    nlohmann::json j;
    j["class"] = "message";
    j["type"] = "admin";
    j["message"] = message["message"];
    startReadingHandles();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if ((*conn)->handle == message["username"].get<std::string>()) {
        (*conn)->sendEvent(j.dump());
        break;
      }
    stopReadingHandles();
    success["username"] = message["username"];
    success["message"] = message["message"];
  }
  else if (message["command"] == "broadcast") {
    if (!checkString(&message, "message")) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    nlohmann::json j;
    j["class"] = "message";
    j["type"] = "admin";
    j["message"] = message["message"];
    startReadingHandles();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) (*conn)->sendEvent(j.dump());
    stopReadingHandles();
    success["username"] = message["username"];
    success["message"] = message["message"];
  }
  else if (message["command"] == "kick") {
    if (!checkString(&message, "username") || !checkNumber(&message, "seconds")) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    if (isModerator(message["username"])) {
      sendFailureResponse(it, failure, "cannot_ban_a_moderator", message);
      return;
    }
    success["username"] = message["username"];
    success["seconds"] = message["seconds"];
    banPlayer(message["username"], message["seconds"]);
  }
  else if (message["command"] == "ban") {
    if (!checkString(&message, "username")) {
      sendFailureResponse(it, failure, "bad_admin_command", message);
      return;
    }
    if (isModerator(message["username"])) {
      sendFailureResponse(it, failure, "cannot_ban_a_moderator", message);
      return;
    }
    success["username"] = message["username"];
    banPlayer(message["username"], 0);
  }
  else if (message["command"] == "players") {
    admin_event = false;
    success["players"] = nlohmann::json::parse("[]");
    startReadingHandles();
    for (std::list<AdvPlayerConnection*>::iterator conn = connections.begin(); conn != connections.end(); ++conn)
      if (!(*conn)->handle.empty()) success["players"].push_back((*conn)->handle);
    stopReadingHandles();
  }
  else if (message["command"] == "registered") {
    admin_event = false;
    success["registered"] = nlohmann::json::parse("[]");
    nlohmann::json j;
    pthread_mutex_lock(&accounts_mutex);
    for (unsigned int p = 0; p < players.size(); ++p) {
      if (checkNumber(&players[p], "banned") && (time_t) players[p]["banned"] <= time(0))
        players[p].erase("banned");
      j = players[p];
      j.erase("password");
      j["roles"] = nlohmann::json::parse("[]");
      if (isAdmin(j["username"].get<std::string>())) j["roles"].push_back("admin");
      else if (isModerator(j["username"].get<std::string>())) j["roles"].push_back("moderator");
      success["registered"].push_back(j);
    }
    pthread_mutex_unlock(&accounts_mutex);
  }
  else if (message["command"] == "sync") {
    pthread_mutex_lock(&sync_mutex);
    last_sync = 0;
    if (checkString(&message, "arguments")) sync_arguments = message["arguments"].get<std::string>();
    pthread_mutex_unlock(&sync_mutex);
  }
  success["class"] = "admin";
  success["event"] = message["command"];
  success["response"] = "success";
  (*it)->sendEvent(success.dump());
  if (admin_event) log(5, replaceAllInstances(advGetText("admin_event"), "${EVENT}", success.dump()));
}

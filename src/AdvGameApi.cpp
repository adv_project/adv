/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvGame::runAPICommands(std::string commands)
{
  nlohmann::json j;
  std::string log_message;
  try {
    j = nlohmann::json::parse(commands);
  }
  catch (...) {
    log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("invalid_api_json_on_game"),
      "${GAME}", properties["name"].get<std::string>());
    log_message = AdvServer::replaceAllInstances(log_message, "${JSON}", commands);
    log.emit(7, log_message);
    return;
  }
  if (j.type() != nlohmann::json::value_t::array) {
    log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("api_json_not_an_array_on_game"),
      "${GAME}", properties["name"].get<std::string>());
    log_message = AdvServer::replaceAllInstances(log_message, "${JSON}", commands);
    log.emit(7, log_message);
    return;
  }
  for (int i = 0; j.size(); ++i) {
    if (j[i].is_null()) break;
    bool ok = true;
    if (!AdvServer::checkString(&j[i], "class") || j[i]["class"].get<std::string>().empty())
      ok = false;
    else if (!hasParameter(&mod_api, j[i]["class"].get<std::string>())) {
      log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("unknown_api_call_on_game"),
        "${GAME}", properties["name"].get<std::string>());
      log_message = AdvServer::replaceAllInstances(log_message, "${CALL}", j[i].dump());
      log.emit(7, log_message);
    }
    else if (mod_api[j[i]["class"].get<std::string>()] == "Message")
      ok = runMessageApiCommand(j[i]);
    else if (mod_api[j[i]["class"].get<std::string>()] == "GameWorkflow")
      ok = runGameWorkflowApiCommand(j[i]);
    else if (mod_api[j[i]["class"].get<std::string>()] == "GameProperties")
      ok = runGamePropertiesApiCommand(j[i]);
    else if (mod_api[j[i]["class"].get<std::string>()] == "Branch")
      sendCall.emit(this, j[i]);
    if (!ok) {
      log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("ignoring_api_call_on_game"),
        "${GAME}", properties["name"].get<std::string>());
      log_message = AdvServer::replaceAllInstances(log_message, "${CALL}", j[i].dump());
      log.emit(7, log_message);
    }
  }
}

bool AdvGame::runMessageApiCommand(nlohmann::json &call)
{
  if (call["class"] == "log") {
    if (!AdvServer::checkString(&call, "message")) return false;
    else {
      if (!AdvServer::checkNumber(&call, "log-level")) call["log-level"] = 5;
      log.emit((unsigned int) call["log-level"], call["message"]);
    }
  }
  else if (call["class"] == "send") {
    if (!AdvServer::checkString(&call, "player")) return false;
    else if (!hasParameter(&call, "message")) return false;
    else {
      if (!AdvServer::checkString(&call, "type")) call["type"] = "plain";
      if (!AdvServer::checkString(&call, "tag")) call["tag"] = "";
      nlohmann::json message;
      message["player"] = call["player"];
      message["message"] = call["message"];
      message["type"] = call["type"];
      message["tag"] = call["tag"];
      sendMessage.emit(message);
    }
  }
  else if (call["class"] == "write") {
    if (!AdvServer::checkString(&call, "player")) return false;
    else if (!hasParameter(&call, "action") || call["action"].type() != nlohmann::json::value_t::array) return false;
    else {
      nlohmann::json message;
      if (!hasParameter(&call, "text")) call["text"] = nlohmann::json::parse("[]");
      message["player"] = call["player"];
      message["action"] = call["action"];
      message["text"] = call["text"];
      sendMessage.emit(message);
    }
  }
  else if (call["class"] == "broadcast") {
    if (!hasParameter(&call, "message")) return false;
    else {
      if (!AdvServer::checkString(&call, "type")) call["type"] = "plain";
      if (!AdvServer::checkString(&call, "tag")) call["tag"] = "";
      nlohmann::json message;
      message["game"] = properties["name"];
      if (AdvServer::checkString(&call, "skip")) message["skip"] = call["skip"];
      message["message"] = call["message"];
      message["type"] = call["type"];
      message["tag"] = call["tag"];
      sendMessage.emit(message);
    }
  }
  return true;
}

bool AdvGame::runGameWorkflowApiCommand(nlohmann::json &call)
{
  std::string log_message;
  if (call["class"] == "run") {
    if (!hasParameter(&call, "path")) return false;
    else {
      struct stat st;
      if (stat(call["path"].get<std::string>().c_str(), &st) < 0) {
        log.emit(7, AdvServer::replaceAllInstances(AdvServer::advGetText("script_doesnt_exist"),
          "${PATH}", call["path"].get<std::string>()));
        return true;
      }
      if ((st.st_mode & S_IEXEC) == 0) {
        log.emit(7, AdvServer::replaceAllInstances(AdvServer::advGetText("no_execution_permission_on_script"),
          "${PATH}", call["path"].get<std::string>()));
        return true;
      }
      nlohmann::json input;
      if (!hasParameter(&call, "input")) call["input"] = nlohmann::json::parse("{}");
      input = call["input"];
      runAPICommands(runScript(call["path"], input.dump()));
    }
  }
  else if (call["class"] == "abort") {
    abort_hook = true;
  }
  else if (call["class"] == "kick") {
    if (!AdvServer::checkString(&call, "player")) return false;
    else {
      log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"), "${HOOK}", "leave");
      log_message = AdvServer::replaceAllInstances(log_message, "${GAME}", properties["name"].get<std::string>());
      log.emit(7, log_message);
      nlohmann::json input;
      input["player"] = call["player"];
      runHook("leave", input.dump());
      kick.emit(this, call["player"]);
    }
  }
  else if (call["class"] == "update") {
    log_message = AdvServer::replaceAllInstances(AdvServer::advGetText("running_hook_on_game"), "${HOOK}", "update");
    log_message = AdvServer::replaceAllInstances(log_message, "${GAME}", properties["name"].get<std::string>());
    log.emit(7, log_message);
    runHook("update", "{}");
    properties["last-updated"] = time(0);
    writeProperties();
  }
  return true;
}

bool AdvGame::runGamePropertiesApiCommand(nlohmann::json &call)
{
  if (call["class"] == "pause") {
    if (!AdvServer::checkArray(&call, "pause-message")) call["pause-message"] = nlohmann::json::parse("[]");
    pause(call["pause-message"]);
  }
  else if (call["class"] == "resume") {
    resume();
  }
  else if (call["class"] == "seconds-per-tick") {
    if (!AdvServer::checkNumber(&call, "seconds-per-tick") || call["seconds-per-tick"] < 0) {
      return false;
    }
    else {
      properties["seconds-per-tick"] = call["seconds-per-tick"];
      writeProperties();
    }
  }
  else if (call["class"] == "description") {
    if (!AdvServer::checkArray(&call, "description")) {
      properties["description"] = nlohmann::json::parse("[]");
      writeProperties();
    }
    else {
      properties["description"] = call["description"];
      writeProperties();
    }
  }
  return true;
}

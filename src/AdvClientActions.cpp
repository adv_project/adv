/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvClient::performAction(std::string action, std::string arguments)
{
  if (action == "list_servers") {
    std::string line;
    if (config["servers"].size() > 0) {
      line = Window::UNDERLINE + advGetText("configured_servers") + Window::NOTUNDERLINE;
      printText("server_info", line);
    }
    else
      printText("server_info", advGetText("no_configured_servers"));
    for (int server = 0; server < config["servers"].size(); ++server) {
      line = config["servers"][server]["name"].get<std::string>() + ": " +
        config["servers"][server]["hostname"].get<std::string>() + ":" +
        std::to_string((int) config["servers"][server]["port"]) + " " +
        config["servers"][server]["username"].get<std::string>();
      if (!hasParameter(&config["servers"][server], "password"))
        line += " (" + advGetText("non_registered") + ")";
      printText("server_info", line);
    }
    if (hasParameter(&current_server, "name") && current_server["name"] != "")
      printText("server_info", advGetText("current_server") + " " + current_server["name"].get<std::string>());
    if (hasParameter(&config, "default-server"))
      printText("help", std::string("default-server = ") + config["default-server"].get<std::string>());
  }
  else if (action == "new_server") {
    std::string name, hostname, username;
    int i, port;
    bool ok = true;
    nextArg(arguments, name);
    nextArg(arguments, hostname);
    nextArg(arguments, username);
    i = hostname.find(":");
    if (i != std::string::npos) {
      try {
        port = std::stoi(hostname.substr(i+1));
      }
      catch (...) {
        port = DEFAULT_PORT;
      }
      hostname = hostname.substr(0, i);
    }
    else
      port = DEFAULT_PORT;
    if (name.empty()) {
      printText("server_error", advGetText("name_for_server_must_be_provided"));
      ok = false;
    }
    if (ok) for (int server = 0; server < config["servers"].size(); ++server)
      if (config["servers"][server]["name"] == name) {
        printText("server_error", replaceAllInstances(advGetText("server_with_name_already_exists"),
          "${SERVER}", name));
        ok = false;
        break;
      }
    if (ok && hostname.empty()) {
      printText("server_error", advGetText("hostname_for_server_must_be_provided"));
      ok = false;
    }
    if (ok && username.empty()) {
      printText("server_error", advGetText("username_for_server_must_be_provided"));
      ok = false;
    }
    if (ok) {
      nlohmann::json server;
      server["name"] = name;
      server["hostname"] = hostname;
      server["port"] = port;
      server["username"] = username;
      if (!arguments.empty()) server["password"] = arguments;
      config["servers"].push_back(server);
      std::string line = replaceAllInstances(advGetText("added_server"), "${SERVER}", name);
      line += " " + hostname + ":" + std::to_string(port) + " " + username;
      if (arguments.empty()) line += " (" + advGetText("non_registered") + ")";
      printText("server_info", line);
    }
  }
  else if (action == "delete_server") {
    if (arguments.empty()) {
      printText("server_error", advGetText("name_for_server_must_be_provided"));
      return;
    }
    std::string name;
    bool found = false;
    while (!arguments.empty()) {
      nextArg(arguments, name);
      for (int server = 0; server < config["servers"].size(); ++server)
        if (config["servers"][server]["name"].get<std::string>() == name) {
          config["servers"].erase(server);
          found = true;
          printText("server_info", replaceAllInstances(advGetText("deleted_server"), "${SERVER}", name));
          break;
        }
      if (!found)
        printText("server_info", replaceAllInstances(advGetText("configured_server_not_found"), "${SERVER}", name));
    }
  }
  else if ( (action == "register") || (action == "login") ) {
    if (arguments.empty()) {
      printText("server_error", advGetText("name_for_server_must_be_provided"));
      return;
    }
    pthread_mutex_lock(&current_server_mutex);
    std::string arg;
    bool found = false;
    nextArg(arguments, arg);
    if (arguments.empty()) {
      for (int server = 0; server < config["servers"].size(); ++server)
        if (config["servers"][server]["name"].get<std::string>() == arg) {
          current_server = config["servers"][server];
          found = true;
          break;
        }
      if (!found) {
        printText("server_info", replaceAllInstances(advGetText("configured_server_not_found"),
          "${SERVER}", arg));
      }
    }
    else {
      current_server["name"] = arg;
      int i = arg.find(":");
      if (i != std::string::npos) {
        current_server["hostname"] = arg.substr(0, i);
        try {
          current_server["port"] = std::stoi(arg.substr(i+1));
        }
        catch (...) {
          current_server["port"] = DEFAULT_PORT;
        }
      }
      else {
        current_server["hostname"] = arg;
        current_server["port"] = DEFAULT_PORT;
      }
      nextArg(arguments, arg);
      current_server["username"] = arg;
      current_server["password"] = arguments;
    }
    if (action == "register") current_server["register"] = true;
    connected = false;
    new_login = true;
    pthread_mutex_unlock(&current_server_mutex);
  }
  else if (action == "help") {
    std::string arg;
    int form;
    if (arguments.empty()) {
      printText("help", "");
      printText("help", std::string("  ") + Window::UNDERLINE + advGetText("adv_client_help_title"));
      printText("help", advGetText("slash_help") + " " + advGetText("slash_help_topics"));
      printText("help", std::string("  ") + advGetText("show_all_help_topics"));
      printText("help", advGetText("slash_help") + " " + advGetText("slash_help_command"));
      printText("help", std::string("  ") + advGetText("show_detailed_help_for_command"));
      printText("help", advGetText("slash_help") + " " + advGetText("slash_help_commands"));
      printText("help", std::string("  ") + advGetText("show_brief_of_all_commands"));
      printText("help", advGetText("slash_help") + " " + advGetText("slash_help_all"));
      printText("help", std::string("  ") + advGetText("show_all_available_help_in_detail"));
      printText("help", std::string("  ") + Window::UNDERLINE + advGetText("help_topics"));
      for (int i = 0; i < commands.size(); ++i)
        if (commands[i]["class"].get<std::string>() == "topic") {
          printText("help", advGetText("slash_help") + " " + commands[i]["name"].get<std::string>());
          printText("help", std::string("  ") + commands[i]["title"].get<std::string>());
        }
      printText("help", "");
    }
    while (!arguments.empty()) {
      nextArg(arguments, arg);
      if (arg == advGetText("slash_help_commands")) {
        printText("help", "");
        printText("help", std::string("  ") + Window::UNDERLINE + advGetText("client_commands"));
        for (int i = 0; i < commands.size(); ++i)
          if (commands[i]["class"].get<std::string>() == "command") {
            form = commands[i]["forms"].size();
            if (form > 0) {
              printText("help", commands[i]["forms"][form-1].get<std::string>() + std::string(" ") +
                commands[i]["usage"].get<std::string>());
              printText("help", std::string("  ") + commands[i]["short"].get<std::string>());
            }
          }
        printText("help", "");
      }
      else if (arg == advGetText("slash_help_topics")) {
        for (int i = 0; i < commands.size(); ++i)
          if (commands[i]["class"].get<std::string>() == "topic")
            showDetailedHelp(commands[i]["name"].get<std::string>());
      }
      else if (arg == advGetText("slash_help_all")) {
        for (int i = 0; i < commands.size(); ++i)
          if (commands[i]["class"].get<std::string>() == "command") {
            form = commands[i]["forms"].size();
            if (form > 0) {
              arg = commands[i]["forms"][form-1].get<std::string>();
              showDetailedHelp(arg);
            }
          }
          else if (commands[i]["class"].get<std::string>() == "topic") {
            showDetailedHelp(commands[i]["name"].get<std::string>());
          }
      }
      else showDetailedHelp(arg);
    }
  }
  else if (action == "logout") {
    pthread_mutex_lock(&current_server_mutex);
    if (connected) {
      sendRequest("{\"class\":\"logout\"}");
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
    current_server.clear();
    pthread_mutex_unlock(&current_server_mutex);
  }
  else if (action == "passwd") {
    if (connected) {
      nlohmann::json message;
      message["class"] = "password";
      message["password"] = arguments;
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "create") {
    if (connected) {
      nlohmann::json message;
      std::string str;
      bool ok = true;
      message["class"] = "create";
      message["lang"] = config["lang"].get<std::string>();
      nextArg(arguments, str);
      if (str.empty()) {
        printText("server_error", advGetText("game_name_not_provided"));
        ok = false;
      }
      else {
        message["name"] = str;
      }
      if (ok) {
        nextArg(arguments, str);
        message["mods"] = nlohmann::json::parse("[]");
        message["title"] = arguments;
        arguments = replaceAllInstances(str, ",", " ");
        while (!arguments.empty()) {
          nextArg(arguments, str);
          message["mods"].push_back(str);
        }
        if (message["mods"].size() == 0)
          printText("server_error", advGetText("at_least_provide_one_mod"));
        else {
          sendRequest(message.dump());
        }
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "manage") {
    if (connected) {
      nlohmann::json message;
      std::string str;
      message["class"] = "manage";
      nextArg(arguments, str);
      if (str.empty())
        printText("server_error", advGetText("mod_name_not_provided"));
      else {
        message["mod"] = str;
        message["arguments"] = nlohmann::json::parse("[]");
        while (!arguments.empty()) {
          nextArg(arguments, str);
          message["arguments"].push_back(str);
        }
        sendRequest(message.dump());
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "transfer") {
    if (connected) {
      nlohmann::json message;
      std::string str;
      message["class"] = "transfer";
      nextArg(arguments, str);
      if (str.empty())
        printText("server_error", advGetText("game_name_not_provided"));
      else {
        message["name"] = str;
        nextArg(arguments, str);
        if (str.empty())
          printText("server_error", advGetText("new_owner_not_provided"));
        else {
          message["new-owner"] = str;
          sendRequest(message.dump());
        }
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "destroy") {
    if (connected) {
      nlohmann::json message;
      std::string str;
      message["class"] = "destroy";
      nextArg(arguments, str);
      if (str.empty())
        printText("server_error", advGetText("game_name_not_provided"));
      else {
        message["name"] = str;
        sendRequest(message.dump());
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "enter") {
    if (connected) {
      nlohmann::json message;
      std::string str;
      message["class"] = "enter";
      message["lang"] = config["lang"].get<std::string>();
      nextArg(arguments, str);
      if (!str.empty()) {
        message["name"] = str;
        sendRequest(message.dump());
      }
      else if (checkString(&config, "default-game") && !config["default-game"].get<std::string>().empty()) {
        message["name"] = config["default-game"].get<std::string>();
        sendRequest(message.dump());
      }
      else {
        printText("server_error", advGetText("game_name_not_provided"));
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "leave") {
    if (connected) {
      sendRequest("{\"class\":\"leave\"}");
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "list_games") {
    if (connected) {
      sendRequest("{\"class\":\"games\"}");
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "list_mods") {
    if (connected) {
      nlohmann::json message;
      message["class"] = "mods";
      if (!arguments.empty()) message["type"] = arguments;
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "version") {
    printText("server_info", replaceAllInstances(advGetText("running_client_version"),
      "${VERSION}", PROGRAMVERSION));
    if (connected) {
      sendRequest("{\"class\":\"version\"}");
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "info") {
    if (connected) {
      sendRequest("{\"class\":\"info\"}");
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "roll") {
    if (connected) {
      nlohmann::json message;
      std::string arg;
      message["class"] = "roll";
      int i;
      if (!arguments.empty()) try {
        nextArg(arguments, arg);
        i = std::stoi(arg);
        message["ndice"] = (unsigned int) i;
      }
      catch (...) {
        printText("error", advGetText("ndice_must_be_numeric"));
      }
      if (!arguments.empty()) try {
        nextArg(arguments, arg);
        i = std::stoi(arg);
        message["nfaces"] = (unsigned int) i;
      }
      catch (...) {
        printText("error", advGetText("nfaces_must_be_numeric"));
      }
      sendRequest(message.dump());
    }
    else {
      printText("server_info", advGetText("not_connected_to_any_server"));
    }
  }
  else if (action == "chat") {
    if (arguments.empty())
      play_mode = false;
    else if (connected) {
      nlohmann::json message;
      message["class"] = "chat";
      message["content"] = arguments;
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "chat-backlog") {
    if (connected) {
      nlohmann::json message;
      message["class"] = "chat-backlog";
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "play") {
    if (arguments.empty())
      play_mode = true;
    else if (connected) {
      std::string str;
      nlohmann::json message;
      message["class"] = "action";
      message["action"] = nlohmann::json::parse("[]");
      while (!arguments.empty()) {
        nextArg(arguments, str);
        message["action"].push_back(str);
      }
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "write") {
    if (connected) {
      std::string str;
      nlohmann::json message;
      message["class"] = "action";
      message["action"] = nlohmann::json::parse("[]");
      while (!arguments.empty()) {
        nextArg(arguments, str);
        message["action"].push_back(str);
      }
      message["text"] = AdvClient::getTextFromEditor();
      sendRequest(message.dump());
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "admin") {
    if (connected) {
      std::string str;
      nlohmann::json message;
      message["class"] = "admin";
      nextArg(arguments, str);
      message["command"] = str;
      if (str.empty()) {
        sendRequest(message.dump());
      }
      else {
        if (message["command"] == "set") {
          nextArg(arguments, str);
          message["option"] = str;
          nextArg(arguments, str);
          if (!str.empty()) {
            if (message["option"] == "sync-period" || message["option"] == "creation-rate")
              message["value"] = std::to_string(parseElapsedTime(str));
            else
              message["value"] = str;
          }
          else if (message["option"] == "motd") {
            asked_for_motd = true;
          }
        }
        else if (message["command"] == "assign-role" || message["command"] == "revoke-role") {
          nextArg(arguments, str);
          message["role"] = str;
          nextArg(arguments, str);
          message["username"] = str;
        }
        else if (message["command"] == "send") {
          nextArg(arguments, str);
          message["username"] = str;
          if (!arguments.empty()) message["message"] = arguments;
        }
        else if (message["command"] == "broadcast") {
          if (!arguments.empty()) message["message"] = arguments;
        }
        else if (message["command"] == "kick") {
          nextArg(arguments, str);
          message["username"] = str;
          if (!arguments.empty()) {
            nextArg(arguments, str);
            message["seconds"] = (long unsigned) parseElapsedTime(str);
          }
        }
        else if (message["command"] == "ban") {
          nextArg(arguments, str);
          message["username"] = str;
        }
        else if (message["command"] == "sync") {
          message["arguments"] = arguments;
        }
        sendRequest(message.dump());
      }
    }
    else printText("server_info", advGetText("not_connected_to_any_server"));
  }
  else if (action == "lang") {
    if (arguments.empty()) {
      printText("help", Window::UNDERLINE + advGetText("supported_languages") + Window::NOTUNDERLINE);
      for (auto& x: nlohmann::json::iterator_wrapper(langs))
        if (x.key() == config["lang"].get<std::string>())
          printText("help", Window::BOLD + x.key() + ": " + langs[x.key()][x.key()].get<std::string>() +
            " (" + advGetText("current_language") + ")");
        else
          printText("help", x.key() + ": " + langs[x.key()][x.key()].get<std::string>() +
            " (" + langs[x.key()][config["lang"].get<std::string>()].get<std::string>() + ")");
    }
    else {
      if (hasParameter(&langs, arguments)) {
        config["lang"] = arguments;
        loadStrings();
        loadCommands();
        printText("help", replaceAllInstances(advGetText("language_set_to"),
          "${LANG}", advGetText(std::string("lang_") + arguments)));
      }
      else {
        printText("error", replaceAllInstances(advGetText("not_a_supported_language"), "${LANG}", arguments));
      }
    }
  }
  else if (action == "option") {
    if (arguments.empty()) {
      printText("help", Window::UNDERLINE + advGetText("current_options") + Window::NOTUNDERLINE);
      if (hasParameter(&config, "default-server"))
        printText("help", std::string("default-server = ") + config["default-server"].get<std::string>());
      else
        printText("help", std::string("default-server = [ ") + advGetText("not_set") + " ]");
      if (hasParameter(&config, "default-game"))
        printText("help", std::string("default-game = ") + config["default-game"].get<std::string>());
      else
        printText("help", std::string("default-game = [ ") + advGetText("not_set") + " ]");
      printText("help", std::string("lang = ") + config["lang"].get<std::string>());
      printText("help", std::string("buffer-size = ") + std::to_string((int) config["buffer-size"]));
      printText("help", std::string("history-size = ") + std::to_string((int) config["history-size"]));
      printText("help", std::string("connection-period = ") + std::to_string((int) config["connection-period"]));
      printText("help", std::string("connection-tries = ") + std::to_string((int) config["connection-tries"]));
      printText("help", std::string("fast-scroll-percentage = ") + std::to_string((int) config["fast-scroll-percentage"]));
      printText("help", std::string("slow-scroll-lines = ") + std::to_string((int) config["slow-scroll-lines"]));
      printText("help", std::string("welcome-message = ") + config["welcome-message"].get<std::string>());
      printText("help", std::string("store-buffer = ") + config["store-buffer"].get<std::string>());
      printText("help", std::string("delay = ") + std::to_string((int) config["delay"]));
      printText("help", std::string("editor = ") + config["editor"].get<std::string>());
    }
    else {
      std::string name, value;
      int i;
      nextArg(arguments, name);
      nextArg(arguments, value);
      if (name == "default-server") {
        if (value.empty()) {
          config.erase("default-server");
          printText("help", std::string("default-server = [ ") + advGetText("not_set") + " ]");
        }
        else {
          config["default-server"] = value;
          printText("help", std::string("default-server = ") + value);
        }
      }
      else if (name == "default-game") {
        if (value.empty()) {
          config.erase("default-game");
          printText("help", std::string("default-game = [ ") + advGetText("not_set") + " ]");
        }
        else {
          config["default-game"] = value;
          printText("help", std::string("default-game = ") + value);
        }
      }
      else if (name == "lang") {
        if (value.empty()) {
          config["lang"] = "en";
          printText("help", std::string("lang = en"));
        }
        else {
          if (hasParameter(&langs, value)) {
            config["lang"] = value;
            printText("help", std::string("lang = ") + value + "(" + advGetText(std::string("lang_") + value) + ")");
          }
          else {
            printText("error", replaceAllInstances(advGetText("not_a_supported_language"), "${LANG}", value));
          }
        }
        loadStrings();
        loadCommands();
      }
      else if (name == "buffer-size") {
        if (value.empty()) {
          config["buffer-size"] = 1000;
          printText("help", std::string("buffer-size = ") + std::to_string((int) config["buffer-size"]));
        }
        else {
          try {
            i = std::stoi(value);
            if (i > 0) {
              config["buffer-size"] = i;
              printText("help", std::string("buffer-size = ") + std::to_string((int) config["buffer-size"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "history-size") {
        if (value.empty()) {
          config["history-size"] = 500;
          printText("help", std::string("history-size = ") + std::to_string((int) config["history-size"]));
        }
        else {
          try {
            i = std::stoi(value);
            if (i > 0) {
              config["history-size"] = i;
              printText("help", std::string("history-size = ") + std::to_string((int) config["history-size"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "connection-period") {
        if (value.empty()) {
          config["connection-period"] = 10;
          printText("help", std::string("connection-period = ") + std::to_string((int) config["connection-period"]));
        }
        else {
          try {
            i = std::stoi(value);
            if (i > 0) {
              config["connection-period"] = i;
              printText("help", std::string("connection-period = ") + std::to_string((int) config["connection-period"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "connection-tries") {
        if (value.empty()) {
          config["connection-tries"] = 10;
          printText("help", std::string("connection-tries = ") + std::to_string((int) config["connection-tries"]));
        }
        else {
          try {
            i = std::stoi(value);
            if (i > 0) {
              config["connection-tries"] = i;
              printText("help", std::string("connection-tries = ") + std::to_string((int) config["connection-tries"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "fast-scroll-percentage") {
        if (value.empty()) {
          config["fast-scroll-percentage"] = 90;
          printText("help", std::string("fast-scroll-percentage = ") +
            std::to_string((int) config["fast-scroll-percentage"]));
        }
        else {
          try {
            i = std::stoi(value);
            if ( (i > 0) && (i <= 100) ) {
              config["fast-scroll-percentage"] = i;
              printText("help", std::string("fast-scroll-percentage = ") +
                std::to_string((int) config["fast-scroll-percentage"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "slow-scroll-lines") {
        if (value.empty()) {
          config["slow-scroll-lines"] = 1;
          printText("help", std::string("slow-scroll-lines = ") +
            std::to_string((int) config["slow-scroll-lines"]));
        }
        else {
          try {
            i = std::stoi(value);
            if (i > 0) {
              config["slow-scroll-lines"] = i;
              printText("help", std::string("slow-scroll-lines = ") +
                std::to_string((int) config["slow-scroll-lines"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "welcome-message") {
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        if (value.empty()) value = "yes";
        if (value != "yes") value = "no";
        config["welcome-message"] = value;
        printText("help", std::string("welcome-message = ") + value);
      }
      else if (name == "store-buffer") {
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        if (value.empty()) value = "yes";
        if (value != "yes") value = "no";
        config["store-buffer"] = value;
        printText("help", std::string("store-buffer = ") + value);
      }
      else if (name == "delay") {
        if (value.empty()) {
          config["delay"] = 50000;
          printText("help", std::string("delay = ") + std::to_string((int) config["delay"]));
        }
        else {
          try {
            i = std::stoi(value);
            if ( (i > 0) && (i < 1000000) ) {
              config["delay"] = i;
              printText("help", std::string("delay = ") + std::to_string((int) config["delay"]));
            }
            else {
              std::string str = replaceAllInstances(advGetText("value_out_of_range"), "${NAME}", name);
              str += " " + advGetText("option_not_changed");
              printText("help", str);
            }
          }
          catch (...) {
            std::string str = replaceAllInstances(advGetText("invalid_value"), "${NAME}", name);
            str += " " + advGetText("option_not_changed");
            printText("help", str);
          }
        }
      }
      else if (name == "editor") {
        config["editor"] = value;
        printText("help", std::string("editor = ") + value);
      }
      else printText("help", replaceAllInstances(advGetText("not_a_recognized_option"), "${NAME}", name));
    }
  }
  else if (action == "alias") {
    if (arguments.empty()) {
      if (config["aliases"].size() == 0)
        printText("help", advGetText("no_configured_aliases"));
      else {
        printText("help", Window::UNDERLINE + advGetText("current_aliases") + Window::NOTUNDERLINE);
        for (int i = 0; i < config["aliases"].size(); ++i)
          printText("help", std::string(config["aliases"][i]["alias"].get<std::string>() +
            " = \"" + config["aliases"][i]["replacement"].get<std::string>() + "\""));
      }
    }
    else {
      std::string alias;
      nextArg(arguments, alias);
      if (alias.empty()) printText("help", advGetText("alias_not_provided"));
      else if (arguments.empty()) printText("help", advGetText("replacement_not_provided"));
      else {
        nlohmann::json new_alias;
        new_alias["alias"] = alias;
        new_alias["replacement"] = arguments;
        config["aliases"].push_back(new_alias);
        printText("help", std::string(alias + " = \"" + arguments + "\""));
      }
    }
  }
  else if (action == "unalias") {
    if (arguments.empty()) printText("help", advGetText("alias_not_provided"));
    else {
      std::string alias;
      while (!arguments.empty()) {
        nextArg(arguments, alias);
        if (!alias.empty()) for (int i = 0; i < config["aliases"].size(); ++i)
          if (alias == config["aliases"][i]["alias"].get<std::string>()) {
            config["aliases"].erase(i);
            printText("help", replaceAllInstances(advGetText("alias_removed"), "${ALIAS}", alias));
            break;
          }
      }
    }
  }
  else if (action == "bind") {
    if (arguments.empty()) {
      if (config["bindings"].size() == 0)
        printText("help", advGetText("no_configured_bindings"));
      else {
        printText("help", Window::UNDERLINE + advGetText("current_bindings") + Window::NOTUNDERLINE);
        for (int i = 0; i < config["bindings"].size(); ++i)
          printText("help", std::string(config["bindings"][i]["key"].get<std::string>() +
            " = \"" + config["bindings"][i]["command"].get<std::string>() + "\""));
      }
    }
    else {
      std::string key;
      nextArg(arguments, key);
      std::transform(key.begin(), key.end(), key.begin(), ::toupper);
      if (key.empty())
        printText("help", advGetText("key_not_provided"));
      else if (!isBoundable(key))
        printText("help", replaceAllInstances(advGetText("not_a_valid_key"), "${KEY}", key));
      else {
        bool found = false;
        if (arguments.empty()) {
          for (int i = 0; i < config["bindings"].size(); ++i)
            if (key == config["bindings"][i]["key"].get<std::string>()) {
              found = true;
              config["bindings"].erase(i);
              printText("help", replaceAllInstances(advGetText("binding_removed"), "${KEY}", key));
              break;
            }
          if (!found) printText("help", replaceAllInstances(advGetText("key_not_bound"), "${KEY}", key));
        }
        else {
          for (int i = 0; i < config["bindings"].size(); ++i)
            if (key == config["bindings"][i]["key"].get<std::string>()) {
              found = true;
              config["bindings"][i]["command"] = arguments;
              break;
            }
          if (!found) {
            nlohmann::json new_binding;
            new_binding["key"] = key;
            new_binding["command"] = arguments;
            config["bindings"].push_back(new_binding);
          }
          printText("help", std::string(key + " -> \"" + arguments + "\""));
        }
      }
    }
  }
  else if (action == "quit") {
    do_exit = true;
  }
  updateView();
}
